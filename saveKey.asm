    
skBuffer        = PlayerMaxHP
SK_BYTES        = 8
SAVEKEY_ADR    = #$0880
    
    include i2c.asm
    i2c_subs


SaveGame SUBROUTINE
    jsr     SetupSaveKey
    bcc     .saveFinished

    ldx     #SK_BYTES-1
.loop
    lda     skBuffer,x
    jsr     i2c_txbyte
    dex
    bpl     .loop

    jsr     i2c_stopwrite

.saveFinished
    rts

LoadGame SUBROUTINE
    jsr     SetupSaveKey
    bcc     .loadFinished
    jsr     i2c_stopwrite
    jsr     i2c_startread

    ldx     #SK_BYTES-1
.loop
    jsr     i2c_rxbyte
    cmp     #$ff
    bne     .skipEmpty
    lda     #0
.skipEmpty
    sta     skBuffer,x
    dex
    bpl     .loop

    jsr     i2c_stopread

    ;calc the checksum
    lda PlayerMaxHP
    beq .loadFinished
    eor Inventory
    clc
    adc NumArrows
    eor PlayerPower
    adc KeysOwned
    eor #%10010011
    eor Respawn
    eor PlayerArmor
    eor #%01001010
    sta LockStatus
.loadFinished
    rts


SetupSaveKey SUBROUTINE
    jsr     i2c_startwrite
    bne     .done
    clv
    lda     #>SAVEKEY_ADR
    jsr     i2c_txbyte
    lda     #<SAVEKEY_ADR
    jmp     i2c_txbyte
.done
    clc
    rts
