
    ORG $E000
    RORG $F000

CURRENT_BANK eqm BANK_2

    include allBanks.asm

    include kernel.asm
    include header.asm
    include graphics.asm
    include finalBossPart2.asm
    include roomColors.asm

;DoNothingAndReturnToBank1
    ;BankRts

    echo "------", [$FFFC - *]d, " bytes free in kernel bank"

	ORG $EFFC
	RORG $FFFC
	.word Start
	.word Start
