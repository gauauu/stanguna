;Thanks to Thomas Jentzch and his clown demo....
;http://www.qotile.net/minidig/demos/clown_demo.asm
;not sure I would have figured this out without that



HEIGHT = 22


Title
    jsr TitleInit
TitleLoop
    jsr     TitleVBlank
    jsr     TitleUpdate
    jsr     TitleDrawScreen
    jsr     TitleOverScan
    jmp     TitleLoop



TitleInit SUBROUTINE

    lda     #$00
    sta     IsStart
    sta     COLUPF
    sta     FrameCtr
    sta     REFP0
    sta     REFP1
    sta     CtrlReleased
    sta     ENABL

    ;make Start selected instead of Passwd
    lda     PlayerPower
    bne     .skipSetToStart
    lda     #$FF
    sta     IsStart
.skipSetToStart

    lda     #$DE
    sta     PlayerColorDef
    sta     COLUP0
    sta     COLUP1

    lda     #1
    sta     CTRLPF

    lda     #%011
    sta     NUSIZ0
    sta     NUSIZ1
    sta     VDELP0
    sta     VDELP1


    sta     WSYNC
    ;SLEEP31
    ;nop
    ;nop
    ;nop
    ;sta     RESP0
    ;sta     RESP1

    ;lda     #$10
    ;sta     HMP0

    ;lda     #$20
    ;sta     HMP1

    ;sta     WSYNC
    ;sta     HMOVE


    rts

TitleVBlank SUBROUTINE
    lda     #2
    sta     WSYNC
    sta     VSYNC
    sta     WSYNC
    sta     WSYNC
    lsr
    ldx     #44
    sta     WSYNC
    sta     VSYNC
    stx     TIM64T
    rts



TitleUpdate SUBROUTINE
    LoadPtrGraphics Title


    sta     WSYNC
    SLEEP31
    nop
    nop
    nop
    sta     RESP0
    sta     RESP1

    lda     #$10
    sta     HMP0

    lda     #$20
    sta     HMP1

    sta     WSYNC
    sta     HMOVE

  
    lda #$90
    sta PlayerY
    lda #$0
    sta COLUPF
    sta COLUBK
    sta PF0
    sta PF1
    sta PF2
    lda PlayerMaxHP
    eor #%11111111
    beq .skipBlueSky
    lda #$72
    sta COLUBK
.skipBlueSky

    inc FrameCtr

    lda FrameCtr
    and #$3
    bne .SkipColorChange

    lda PlayerColorDef
    clc
    adc #1
    sta PlayerColorDef
    sta COLUP0
    sta COLUP1

.SkipColorChange


    ;check fo reset button if we're on ending
    lda #SWCHB_RESET
    and SWCHB
    bne .SkipReset
    lda PlayerMaxHP
    eor #%11111111
    bne .SkipReset
    jmp Start
    


.SkipReset

    ;if button not pressed, skip button logic
    lda #%10000000
    bit INPT4
    bne .dontEndTitle

    ;if we're on ending screen, skip button logic
    lda PlayerMaxHP
    eor #%11111111
    beq .rts

    lda CtrlReleased
    beq .rts

    lda IsStart
    beq .passwordScreen
    BankJump StartNewGame,BANK_1
.passwordScreen
    BankJump PasswordEntry,BANK_B


.dontEndTitle
    ;check for controls to change selected menu item
    lda #JOY_DOWN
    bit SWCHA
    beq .switchItem
    lda #JOY_UP
    bit SWCHA
    beq .switchItem
    jmp .SkipControls
.switchItem

    lda CtrlReleased
    beq .rts
    lda IsStart
    eor #$FF
    sta IsStart
    lda #$0
    sta CtrlReleased
.rts
    rts

.SkipControls
    lda #1
    sta CtrlReleased
    rts


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Title screen kernel
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    echo "------", [TitleDrawScreen - *]d, " bytes free before TitleAlign"


    align 256

TitleDrawScreen SUBROUTINE
    ldx     #227
.waitTim:
    lda     INTIM
    bne     .waitTim
    sta     WSYNC
    sta     VBLANK              ; 3
    stx     TIM64T              ; 4

    ldx     #24
.wait:
    dex
    sta     WSYNC
    bne     .wait

    lda     #$C4
    sta     COLUPF
    
    lda     #%11000000
    sta     PF0
    sta     PF1
    sta     WSYNC

    ;SLEEP31
    ;nop
    ;nop
    ;nop
    ;sta     RESP0
    ;sta     RESP1

    
    ;lda     #$10
    ;sta     HMP0

    ;lda     #$20
    ;sta     HMP1

    ;sta     WSYNC
    ;sta     HMOVE


    sta     WSYNC
    sta     WSYNC
    sta     WSYNC

    lda     #%11100000
    sta     PF0
    sta     PF1

    ldx     #12
.wait2:
    dex
    sta     WSYNC
    bne     .wait2

    SLEEP51
    nop                         ;2
    ;lda     #%11111100          ; 2
    lda     #0
    sta     PF2                 ; 3
    ldy     #HEIGHT-1           ; 2
.loop:
    sty     tmpVar              ; 3

    ;update the color
    tya                         ;2
    adc     PlayerColorDef      ;3
    sta     COLUP0              ; 3 = 11

    lda     (ptr+$0),y          ; 5
    sta     GRP0                ; 3
    lda     (ptr+$2),y          ; 5
    sta     GRP1                ; 3
    lda     (ptr+$4),y          ; 5
    sta     GRP0                ; 3
    lda     (ptr+$6),y          ; 5
    sta     tmpVar2             ; 3 = 32

    lax     (ptr+$a),y          ; 5
    lda     (ptr+$8),y          ; 5
    ldy     tmpVar2             ; 3
    sty     GRP1                ; 3         @43
    sta     GRP0                ; 3
    stx     GRP1                ; 3
    sta     GRP0                ; 3 = 25

    ldy     tmpVar              ; 3
    dey                         ; 2

    ;burning 3 cycles in place of the bpl since the loop is half-unrolled
    sty     tmpVar              ;3 (in place of the bpl)


    sty     tmpVar              ; 3

    ;update the color
    tya                         ;2
    adc     PlayerColorDef      ;3
    sta     COLUP1              ; 3 = 11

    lda     (ptr+$0),y          ; 5
    sta     GRP0                ; 3
    lda     (ptr+$2),y          ; 5
    sta     GRP1                ; 3
    lda     (ptr+$4),y          ; 5
    sta     GRP0                ; 3
    lda     (ptr+$6),y          ; 5
    sta     tmpVar2             ; 3 = 32

    lax     (ptr+$a),y          ; 5
    lda     (ptr+$8),y          ; 5
    ldy     tmpVar2             ; 3
    sty     GRP1                ; 3  
    sta     GRP0                ; 3
    stx     GRP1                ; 3
    sta     GRP0                ; 3 = 25

    ldy     tmpVar              ; 3
    dey                         ; 2

    bpl     .loop               ; 2/3=  7/8

;---------------------------------------------------------------
;  After the title graphic, show the menu
;---------------------------------------------------------------
    iny
    sty     GRP0
    sty     GRP1
    sty     GRP0

    sta     WSYNC
    sta     WSYNC
    sta     WSYNC
    sta     WSYNC
    sta     WSYNC
    sta     WSYNC

    lda     #%11000000
    sta     PF0
    sta     PF1

    sta     WSYNC
    sta     WSYNC
    sta     WSYNC
    sta     WSYNC

    lda     #%10000000
    sta     PF0
    sta     PF1
    lda     #$16
    sta     COLUPF

    WaitSync #10

    lda     #$C8
    sta     COLUBK
    lda     #%11000000
    sta     PF2

    WaitSync #10

    lda     #0
    sta     PF0
    sta     PF1

    WaitSync #5

    lda     #%11100000
    sta     PF2

    WaitSync #10
    lda     #%11110000
    sta     PF2

    WaitSync #10
    lda     #%11111000
    sta     PF2

    WaitSync #10
    lda     #%11111100
    sta     PF2



    sty WSYNC
    sty WSYNC
    LoadPtrGraphics StartText
    lda #0
    sta GRP0
    sta GRP1
    sta GRP0
    sta GRP1

    lda PlayerMaxHP
    eor #%11111111
    bne .normalMenu

    jmp PlayerEnding


.normalMenu
    ;;;;;;;;;;;;;;;;
    ; Start option
    ;;;;;;;;;;;;;;;;
    ;pick the right color to display it
    lda IsStart
    beq .notIsStart
    lda FrameCtr
    and #%001000
    bne .startC2
    lda #$62
    jmp .done1
.startC2
    ;lda #$96
    lda #$62
    jmp .done1
.notIsStart
    lda #$06
.done1
    sta COLUP0
    sta COLUP1

    lda #6

    jsr Draw6Char

    sty WSYNC
    sty WSYNC
    sty WSYNC
    sty WSYNC

    ;;;;;;;;;;;;;;;;;;;;;
    ; Password option
    ;;;;;;;;;;;;;;;;;;;
    LoadPtrGraphics PasswordText
    lda #0
    sta GRP0
    sta GRP1
    sta GRP0
    sta GRP1

    ;pick the right color to display it
    lda IsStart
    beq .notIsStart2
    lda #$06
    jmp .done2
.notIsStart2
    lda #$62
.done2
    sta COLUP0
    sta COLUP1


    lda #7

    jsr Draw6Char

    lda #0
    sta GRP0
    sta GRP1
    sta GRP0
    sta GRP1

EndOfTitleMenuSection

    ldx     #2
.waitScreen:
    lda     INTIM
    bne     .waitScreen
    sta     WSYNC
    stx     VBLANK


    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TitleOverScan SUBROUTINE
    lda     #36
    sta     TIM64T

.waitTim:
    lda     INTIM
    bne     .waitTim
    rts



    

    echo "------", [Draw6Char - *]d, " bytes free before Draw6Char"
;---------------------------------------
;  General reusable 6-char kernel chunk
;---------------------------------------
    align 256
Draw6Char SUBROUTINE ;a should be number o flines
    sty WSYNC
    SLEEP51
    ;wasting more cycles:
    ldy     #00 ;3
    tay         ;2
    tay         ;2
    tay         ;2
    tay         ;2
.loop:
    sty     tmpVar              ; 3

    ;setting color...these are pretty unnecessary, can waste 8 cycles 
    ;doing something more productive if I need to....
    tya                         ;2
    adc     PlayerColorDef      ;3
    lda     PlayerColorDef      ; 3 = 11

    lda     (ptr+$0),y          ; 5
    sta     GRP0                ; 3
    lda     (ptr+$2),y          ; 5
    sta     GRP1                ; 3
    lda     (ptr+$4),y          ; 5
    sta     GRP0                ; 3
    lda     (ptr+$6),y          ; 5
    sta     tmpVar2             ; 3 = 32

    lax     (ptr+$a),y          ; 5
    lda     (ptr+$8),y          ; 5
    ldy     tmpVar2             ; 3
    sty     GRP1                ; 3         @43
    sta     GRP0                ; 3
    stx     GRP1                ; 3
    sta     GRP0                ; 3 = 25

    ldy     tmpVar              ; 3
    dey                         ; 2

    bpl     .loop
    iny
    sty     COLUP0
    sty     COLUP1
    sty     GRP0
    sty     GRP1

    rts






PlayerEnding SUBROUTINE

    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    ldx #6
.loop
    dex
    nop
    bne .loop
    sta RESP0

    sta WSYNC
    ldx #PLAYER_HEIGHT
    dex
    lda #0
    sta NUSIZ0

.playerLoop
    txa
    tay
    lda TitlePlayerFrame0,Y
    sta GRP0
    lda #0
    sta GRP1
    iny
    lda TitlePlayerColor,Y
    sta COLUP0
    dex
    sta WSYNC
    sta WSYNC
    bne .playerLoop
    lda #0
    sta GRP0
    sta GRP1
    sta COLUP0
    sta COLUP1
    lda #%011
    sta NUSIZ0

    jmp EndOfTitleMenuSection

TitlePlayerColor
        .byte #$08
        .byte #$08
        .byte #$08
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$1C
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$FE
        .byte #$FE
        .byte #$F8
;
        .byte #%00000000;
TitlePlayerFrame0
        .byte #%01110000
        .byte #%01100110
        .byte #%01100110
        .byte #%01110110
        .byte #%00111100
        .byte #%10111101
        .byte #%10111101
        .byte #%10111111
        .byte #%11111110
        .byte #%00111100
        .byte #%00101000
        .byte #%00111100
        .byte #%00000000
