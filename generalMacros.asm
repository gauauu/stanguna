        
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; MACro for general startup, clear everything
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MAC StartupZeroMem
        SUBROUTINE
            sei                         ;           Disable interrupts, if there are any.
            cld                         ;           Clear BCD math bit.
            ldx     #0
            txs
            pha                         ;           Set stack to beginning.
            txa
.clearLoop:
            pha
            dex
            bne     .clearLoop
        ENDM

    MAC Skip2Bytes
        .BYTE $2C
    ENDM

    MAC WaitSync
    SUBROUTINE
        ldx #{1}
.loop
        dex
        sta WSYNC
        bne .loop
    ENDM



    MAC LoadPtrGraphics
        lda #<{1}A
        sta ptr
        lda #>{1}B
        sta ptr+1

        lda #<{1}B
        sta ptr+2
        lda #>{1}B
        sta ptr+3

        lda #<{1}C
        sta ptr+4
        lda #>{1}C
        sta ptr+5

        lda #<{1}D
        sta ptr+6
        lda #>{1}D
        sta ptr+7

        lda #<{1}E
        sta ptr+8
        lda #>{1}E
        sta ptr+9

        lda #<{1}F
        sta ptr+10
        lda #>{1}F
        sta ptr+11
    ENDM
        

        
        ;-------------------------------------------------------------------------------
        ; SLEEP duration
        ; Original author: Thomas Jentzsch
        ; Inserts code which takes the specified number of cycles to execute.  This is
        ; useful for code where precise timing is required.
        ; ILLEGAL-OPCODE VERSION DOES NOT AFFECT FLAGS OR REGISTERS.
        ; LEGAL OPCODE VERSION MAY AFFECT FLAGS
        ; Uses illegal opcode (DASM 2.20.01 onwards).
        ; lifted from http://www.alienbill.com/2600/101/bin/macro.h
        MAC SLEEP            ;usage: SLEEP n (n>1)
.CYCLES     SET {1}

            IF .CYCLES < 2
                ECHO "MACRO ERROR: 'SLEEP': Duration must be > 1"
                ERR
            ENDIF

            IF .CYCLES & 1
                IFNCONST NO_ILLEGAL_OPCODES
                    nop 0
                ELSE
                    bit VSYNC
                ENDIF
.CYCLES             SET .CYCLES - 3
            ENDIF
        
            REPEAT .CYCLES / 2
                nop
            REPEND
        ENDM

        MAC SLEEP51
        SUBROUTINE
            ldx   #10         ;2
.wasteLoop
            dex               ;2
            bne   .wasteLoop  ;3  
        ENDM

        MAC SLEEP31
        SUBROUTINE
            ldx #6            ;2
.wasteLoop
            dex               ;2
            bne   .wasteLoop  ;3  
        ENDM


        MAC SetFlag
            lda MiscFlags
            ora {1}
            sta MiscFlags
        ENDM


        MAC ClearFlag
            lda MiscFlags
            and ~({1})
            sta MiscFlags
        ENDM

        MAC IfFlagSet
            lda MiscFlags
            and {1}
            bne {2}
        ENDM

        MAC IfFlagClear
            lda MiscFlags
            and {1}
            beq {2}
        ENDM

        MAC SetFlag2
            lda MiscFlags2
            ora {1}
            sta MiscFlags2
        ENDM


        MAC ClearFlag2
            lda MiscFlags2
            and ~{1}
            sta MiscFlags2
        ENDM

        MAC IfFlag2Set
            lda MiscFlags2
            and {1}
            bne {2}
        ENDM

        MAC IfFlag2Clear
            lda MiscFlags2
            and {1}
            beq {2}
        ENDM


        MAC DebugRepeatForever
        SUBROUTINE
.foo
        jmp .foo
        ENDM


        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; MACro for preparing sprite lookup tables 
        ; #{1} - Symbol name of graphical data location
        ; #{2} - Variable name for the lookup
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MAC PrepareSpriteLookupTable

            lda #>{1}
            sta {2}+1
            lda #<{1}-1
            sta {2}

        ENDM

        MAC CopyPointer
            lda #<{1}
            sta {2}
            lda #>{1}
            sta {2}+1
        ENDM
            
        ;Simple Randomizer thanks
        ; to Batari from
        ; http://atariage.com/forums/topic/159268-random-numbers/
        ; but hacked up to be self-seeding, 
        ; probably way less random now
        MAC Randomize
        SUBROUTINE
            lda Rand
            bne .SkipSeeding
            lda #$FF
.SkipSeeding
            lsr
            bcc .noeor
            eor #$B4
.noeor
            sta Rand
        ENDM




        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; Variation of Thomas Jentzsch's skipdraw for sprite rendering
        ; {1}  YPositionVariable
        ; #{2} SPRITE_HEIGHT constant
        ; {3}  SpriteAnimTable variable (lookup variable for sprite data)
        ; {4}
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MAC SkipDraw
        SUBROUTINE

            txa
            sec
            sbc {1}
            adc #{2}
            bcc .SkipDraw
            tay
            lda ({3}),Y
            sta {4}

.SkipDraw
        ENDM

        MAC BankJumpSaveA
            sta Temp
            lda #>{1}
            pha
            lda #<{1}-1
            pha
            lda Temp
            jmp JumpBank_{2}
        ENDM

        MAC BankJump 
            lda #>{1}
            pha
            lda #<{1}-1
            pha
            jmp JumpBank_{2}
        ENDM

        MAC BankJsr
            lda #>(.Return-1)
            pha
            lda #<(.Return-1)
            pha

            lda #<CURRENT_BANK
            pha

            lda #>({1}-1)
            pha
            lda #<({1}-1)
            pha

            jmp JumpBank_{2}
.Return
        ENDM

        MAC BankRts
            pla
            cmp #<BANK_1
            bne .not1
            jmp JumpBank_BANK_1
.not1
            cmp #<BANK_2
            bne .not2
            jmp JumpBank_BANK_2
.not2
            cmp #<BANK_A
            bne .nota
            jmp JumpBank_BANK_A
.nota
            jmp JumpBank_BANK_B
        ENDM
            

        MAC BitShiftAByX
        SUBROUTINE
            ;if x starts as 0, don't do anything
            inx
            dex
            beq .DoneShiftLoop
.ShiftLoop
            clc
            rol
            dex
            bne .ShiftLoop
.DoneShiftLoop
        ENDM

        MAC BitShiftBy
        SUBROUTINE
            ldx {1}
.ShiftLoop
            dex
            beq .DoneShiftLoop
            clc
            rol
            jmp .ShiftLoop
.DoneShiftLoop
        ENDM


        ;{1} - Target Address
        ;{2} - Graphic Base Address
        ;A   - Holds offset (must be > 16)
        MAC LoadGraphicByOffset
        SUBROUTINE
            ;multiply by 8
            clc
            rol 
            rol
            rol

            ;add A to the address at {2}
            ;and store it in target
            adc #<{2}
            sta {1}
            lda #>{2}
            adc #0
            sta {1}+1

        ENDM

        ;thanks to Urchlay
        ;http://atariage.com/forums/topic/71120-6502-killer-hacks/page-4#entry1255298
        MAC AbsoluteValue
        SUBROUTINE
          bpl .foundabs
          eor #$ff
          clc; not needed if the state of C is known up front
          adc #1; would be "adc #0", if C is known to be set
.foundabs
        ENDM

