
LdaEnemyDefYBankAToBank1
    lda (EnemyDef),Y
    BankJumpSaveA Bank1Rts,BANK_1


SetRandomDirection4Way SUBROUTINE
.Start4Way
        jsr GetRandomNumber
        and #%00100000
        beq .MoveHoriz
        jsr GetRandomNumber
        and #%00100000
        beq .GoUp
.GoDown
        lda Enemy0Dir,X
        and #DIR_MASK
        ora #DIR_Y_DOWN
        sta Enemy0Dir,X
        rts
.GoUp
        lda Enemy0Dir,X
        and #DIR_MASK
        ora #DIR_Y_UP
        sta Enemy0Dir,X
        ;lda #DIR_Y_UP
        ;sta Enemy0Dir,X
        rts
.MoveHoriz
        jsr GetRandomNumber
        and #%00100000
        beq .GoLeft
.GoRight
        lda Enemy0Dir,X
        and #DIR_MASK
        ora #DIR_X_RIGHT
        sta Enemy0Dir,X
        ;lda #DIR_X_RIGHT
        ;sta Enemy0Dir,X
        rts
.GoLeft
        lda Enemy0Dir,X
        and #DIR_MASK
        ora #DIR_X_LEFT
        sta Enemy0Dir,X
        ;lda #DIR_X_LEFT
        ;sta Enemy0Dir,X
.Done
        rts


SetEnemyPosition
    sty Enemy0Y,X
    sta Enemy0X,X
    EndEnemyFunc

SetRandomDirection
    lda FrameCtr
    adc Enemy0X,X
    adc Enemy0Y,X
    adc PlayerX
    adc PlayerSpriteAnimFrame
    sta Enemy0Dir,X
    rts



PursueMainChar SUBROUTINE
        ;clear the directional flags
        lda Enemy0Dir,X
        and #DIR_MASK
        sta Enemy0Dir,X

        sec
        lda Enemy0X,X
        sbc PlayerX
        beq .CheckY
        bmi .MoveRight
        lda Enemy0Dir,X
        ora #DIR_X_LEFT
        sta Enemy0Dir,X
        bne .CheckY ;optimized jmp
.MoveRight
        lda Enemy0Dir,X
        ora #DIR_X_RIGHT
        sta Enemy0Dir,X

.CheckY
        sec
        lda Enemy0Y,X
        sbc PlayerY
        beq .Done
        bmi .MoveUp
        lda Enemy0Dir,X
        ora #DIR_Y_DOWN
        sta Enemy0Dir,X
        bne .Done ;optimized jmp
.MoveUp
        lda Enemy0Dir,X
        ora #DIR_Y_UP
        sta Enemy0Dir,X
.Done
        rts

RunEnemyUpdateFunc 
    SUBROUTINE
    ;first ensure on screen
    ;if off edge of screen, move back
    lda Enemy0X,X
    cmp #ENEMY_RIGHT_THRESHOLD + 3
    bcc .noSideReposition
    lda #ENEMY_RIGHT_THRESHOLD
    sta Enemy0X,X


.noSideReposition

    ldy #6                      ;load the address from the enemy definition into the EnemyUpdateFunc
    lda (EnemyDef),Y
    sta TempPF1
    ldy #7
    lda (EnemyDef),Y
    sta TempPF2

    jmp (TempPF1)



MoveEnemyXByDirection
    SUBROUTINE
        lda Enemy0Dir,X
        and #~DIR_X_MASK
        beq .done
        and #DIR_X_RIGHT
        beq .moveXLeft
        inc Enemy0X,X   ; move right
        lda Enemy0X,X
        cmp #ENEMY_RIGHT_THRESHOLD
        ;bne .done
        bcc .done
        ;make 1 less than thresshold
        ldy #ENEMY_RIGHT_THRESHOLD
        dey
        sty Enemy0X,X

        lda Enemy0Dir,X
        ;if hit a wall, reverse
        and #DIR_X_MASK
        ora #DIR_X_LEFT
        sta Enemy0Dir,X
        jmp .done
.moveXLeft
        dec Enemy0X,X
        lda Enemy0X,X
        cmp #ENEMY_LEFT_THRESHOLD
        bne .done
        lda Enemy0Dir,X
        ;if hit a wall, reverse
        and #DIR_X_MASK
        ora #DIR_X_RIGHT
        sta Enemy0Dir,X
.done
        rts

MoveEnemyYByDirection
    SUBROUTINE
        lda Enemy0Dir,X
        and #~DIR_Y_MASK
        beq .done
        and #DIR_Y_UP
        beq .moveYDown

        inc Enemy0Y,X   ; move up
        lda Enemy0Y,X
        cmp #ENEMY_TOP_THRESHOLD
        bmi .done
        lda Enemy0Dir,X
        and #DIR_Y_MASK
        ora #DIR_Y_DOWN
        sta Enemy0Dir,X
        jmp .done
.moveYDown
        dec Enemy0Y,X
        lda Enemy0Y,X
        cmp #ENEMY_BOTTOM_THRESHOLD
        bpl .done
        lda Enemy0Dir,X
        and #DIR_Y_MASK
        ora #DIR_Y_UP
        sta Enemy0Dir,X
.done
        rts


    ;this is here because it MUST be called
    ;only from this file
PrepareEnemySpriteLookupTable
        ;ldy #{1}
        lda (EnemyDef),Y
        sta EnemySpriteTable
        dec EnemySpriteTable
        iny
        ;ldy #{1}+1
        lda (EnemyDef),Y
        sta EnemySpriteTable+1
        BankJump EnemyUpdateDone,1
        ;rts
    ;ENDM

;EnemyUpdateSpawnBullet SUBROUTINE
        ;lda EnemyMissileDir
        ;Initialize enemy bullet
        ;lda Enemy0Dir,X             ;match enemy dir
        ;don't spawn if no direction
        ;beq BulletSpawnRts
ActuallySpawnBullet
        ora #FLAG_ACTIVE              ;make active
        sta EnemyMissileDir
        lda Enemy0X,X
        adc #4
        sta EnemyMissileX
        lda Enemy0Y,X
        sbc #4
        sta EnemyMissileY
BulletSpawnRts
        rts

AdjustEnemyBulletXOnSpawn
        cmp EnemyMissileY
        bne BulletSpawnRts
        lda EnemyMissileX
        sty Temp
        sbc Temp
        sta EnemyMissileX
        rts

EnemyUpdateMaybeSpawnBulletWithDir SUBROUTINE
        tay
        lda EnemyMissileDir
        and #%10000000
        bne BulletSpawnRts

        tya
        and #(~DIR_Y_MASK | ~DIR_X_MASK)
        beq BulletSpawnRts

        jmp ActuallySpawnBullet
        ;Initialize enemy bullet
        ;ora #FLAG_ACTIVE              ;make active
        ;sta EnemyMissileDir
        ;lda Enemy0X,X
        ;sbc #7
        ;sta EnemyMissileX
        ;lda Enemy0Y,X
        ;sbc #4
        ;sta EnemyMissileY
;.noSpawn
        ;rts

RestoreEnemyAfterDark SUBROUTINE
    ldy #ENEMY_DEF_HEIGHT
    lda (EnemyDef),Y
    sta EnemyHeight
    ldy #ENEMY_DEF_COLOR
    lda (EnemyDef),Y
    sta EnemyColor
    ldy #ENEMY_DEF_SIZE
    lda (EnemyDef),Y
    sta EnemyScaleNuSiz
    lda EnemyAnimFrame
    clc
    rol
    adc #ENEMY_DEF_ANIM0
    tay
    lda (EnemyDef),Y
    sta EnemySpriteTable
    dec EnemySpriteTable
    iny
    lda (EnemyDef),Y
    sta EnemySpriteTable+1

    lda FrameCtr
    lsr
    bcc .AfterResetPos
       
    lda #0
    sta Enemy0X
    sta Enemy0Y
.AfterResetPos

    BankJump AfterDarkFix,BANK_1

  
