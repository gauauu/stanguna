all: anguna.bin

anguna.bin: *.asm *.h
		./dasm/dasm.Linux.x86 anguna.asm -f3 -v2 -p10 -T1 -sanguna.sym -llistfile -oanguna.bin 


run: anguna.bin
	stella anguna.bin

debug: anguna.bin
	echo `grep DEBUG *.asm`
	stella -break `grep DEBUG anguna.sym | awk '{print $$2}'` anguna.bin 

clean:
	rm -f anguna.bin


test: anguna.bin
	stella -tiadriven 1 anguna.bin

