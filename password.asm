    MAC PrepStat2
        lda {1}
        ;jsr PHex2Bcd
        ldx #{2}*2
        jsr LoadBCDDigits
    ENDM

    MAC PrepStat1
        lda {1}
        jsr PHex2Bcd
        clc
        rol
        rol
        rol
        adc #<PDigit0
        sta ptr1+{2}*2
        lda #>PDigit0
        adc #0
        sta ptr1+{2}*2+1
    ENDM
        

SubscreenShowPassword
    jsr ShowPassword
    BankJump AfterShowPassword,BANK_A

;PHex2Bcd ;(good 0-99)
;thanks to Omegamatrix: 
;http://atariage.com/forums/blog/563/entry-12057-hex-to-bcd-conversion-0-99-part-trois/
;22 bytes, 26 cycles
    ;tay              ;2  @2
    ;lsr              ;2  @4
    ;lsr              ;2  @6
    ;lsr              ;2  @8
    ;lsr              ;2  @10
    ;tax              ;2  @12
    ;tya              ;2  @14
    ;sed              ;2  @16
    ;clc              ;2  @18
    ;adc   #0         ;2  @20
    ;adc   PBcdTab,X   ;4  @24
    ;cld              ;2  @26
    ;rts
;
;
;PBcdTab:
    ;.byte $00,$06,$12,$18,$24,$30,$36
ResetAfterChecksum
    jsr CalcChecksum
    sta LockStatus
    BankJump PlayerDied,BANK_1

CalcChecksum
    lda PlayerMaxHP
    eor Inventory
    clc
    adc NumArrows
    eor PlayerPower
    adc KeysOwned
    eor #%10010011
    eor Respawn
    eor PlayerArmor
    eor #%01001010
    rts

SetCaret SUBROUTINE
    lda RoomDef
    bne .rts

    lda PasswordY
    stx Temp
    cmp Temp
    bne .rts

    ldx #MISSILE_BALL_ENABLE
    stx ENABL

    lda #BG_REFLECT
    ora #NUSIZ_MISSILE_4
    sta CTRLPF ; reflect background
    lda #15
    ;PositionObjectWithoutJSR 4
.rts
    rts


ShowPassword SUBROUTINE
    PrepStat2 PlayerMaxHP,0
    PrepStat2 Inventory,2
    PrepStat2 NumArrows,4
    lda #$0E
    sta COLUP0
    sta COLUP1
    lda #7
    jsr Draw6Char
    ldx #0
    jsr SetCaret
    PrepStat2 PlayerPower,0
    PrepStat2 PlayerArmor,2
    PrepStat2 KeysOwned,4

    lda #$0E
    sta COLUP0
    sta COLUP1
    lda #7
    ldx #0
    stx ENABL
    jsr Draw6Char
    ldx #1
    jsr SetCaret

    PrepStat2 Respawn,0
    PrepStat2 ProgressFlags,2
    lda RoomDef
    beq .afterCalc
    jsr CalcChecksum
    sta LockStatus
.afterCalc
    PrepStat2 LockStatus,4

    lda #$0E
    sta COLUP0
    sta COLUP1
    lda #8
    ldx #0
    stx ENABL
    jsr Draw6Char
    ldx #2
    jsr SetCaret
    sta WSYNC
    sta WSYNC
    sta WSYNC
    lda #0
    sta ENABL
    lda #1
    rts

LoadBCDDigits SUBROUTINE


    tay
    ;isolate first digit, multiply by 8
    and #$0f
    clc
    rol
    rol
    rol
    ;add the result # of bytes to the address of gfx 0
    ;and save it to the temp var for displaying digit 1
    clc
    adc #<PDigit0
    sta ptr1,X
    lda #>PDigit0
    adc #0
    inx
    sta ptr1,X

    ;now work on the 2nd digit
    tya
    and #%11110000
    clc
    ror

    ;add the result # of bytes to the address of gfx 0
    ;and save it to the temp var for displaying digit 2 
    clc
    adc #<PDigit0
    inx
    sta ptr1,X
    lda #>PDigit0
    adc #0
    inx
    sta ptr1,X
    rts


    
PasswordEntry
    jsr     PasswordInit
PasswordLoop
    jsr     TitleVBlank
    jsr     PasswordUpdate
    jsr     PasswordDrawScreen
    jsr     TitleOverScan
    jmp     PasswordLoop


PasswordInit
    lda     #0
    sta     TopDoorColor
    sta     COLUBK
    sta     CtrlReleased
    sta     REFP0
    sta     REFP1
    sta     ENABL
    sta     VDELBL
    sta     PasswordChar
    sta     PasswordX
    sta     PasswordY
    sta     HMP0
    sta     HMP1

    lda     #$FF
    sta     COLUP0
    sta     COLUP1
    sta     COLUPF
    
    rts

PasswordUpdate SUBROUTINE

    lda     #JOY_BUTTON_PRESSED
    bit     INPT4
    beq     .buttonPressed

    sta     CtrlReleased
    jmp     .afterButtonChecks

.buttonPressed
    lda     CtrlReleased
    beq     .ignoredBecauseHeld

    jsr     CalcChecksum
    sec
    cmp     LockStatus 
    beq     .success

    ; back to start screen, maybe play a noise
    BankJump Title,BANK_B


.success
    BankJump ContinueGame,BANK_1
    ; HERE IT SHOULD INIT AND PROCEED TO GAME

.ignoredBecauseHeld
.afterButtonChecks

    lda     PasswordScrollTimer
    beq     .checkControls
    dec     PasswordScrollTimer
    beq     .checkControls
    jmp     AfterPasswordControls

DEBUG
.checkControls

    ;clear experience if password changes
    lda     SWCHA
    eor     #%11111111
    and     #(JOY_UP|JOY_DOWN)
    beq     .skipClearXp

    lda     #0
    sta     Experience
.skipClearXp
    jmp     PasswordAfterTitleUpdate

PasswordBackToMain
    lda PasswordX
    clc
    rol
    rol
    rol
    adc #59
    ldx  #OBJ_BL
    nop
    nop
    nop
    PosObject2
    sta WSYNC
    sta HMOVE

    rts

PasswordDrawScreen SUBROUTINE
    ldx     #227
.waitTim:
    lda     INTIM
    bne     .waitTim
    sta     WSYNC
    sta     VBLANK              ; 3
    stx     TIM64T              ; 4


    ldx     #60
.loop
    sta     WSYNC
    dex
    bne     .loop

    jsr     ShowPassword

    ldx     #2
.waitScreen:
    lda     INTIM
    bne     .waitScreen
    sta     WSYNC
    stx     VBLANK

    rts
