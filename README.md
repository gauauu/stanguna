# Stanguna, aka Stella Anguna, aka Anguna 2600 #

This is an attempt at making an Atari 2600 version of my action-adventure game, [Anguna](http://www.bitethechili.com/anguna/).  For more information about this game, see [http://www.bitethechili.com/inprogress/](http://www.bitethechili.com/inprogress/) or [my development blog](http://anguna-dev.blogspot.com/).

## Assembling ##

To build on linux, just run make.  On mac or windows, rewrite the Makefile slightly to point at the right version of dasm. (Dasm, the assembler, is so small that I've included it in the repo to make sure I always have a working version.  [See the dasm project page](http://sourceforge.net/projects/dasm-dillon/) for source/license/etc of dasm).  To run the game, launch anguna.bin in [Stella](http://stella.sourceforge.net/) or whatever your preferred atari 2600 emulator is.

## License ##

I guess I should pick a license if I have this up here for the world. All this code (other than snippets marked with comments otherwise) is under the NCSA License. See license.txt.


todo:
- trees are a little ugly
- main boss colors bad
- item colors stand out more?

