;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Background-related constants
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BACKDROP_HALF_CLOSE_LINE    equ 2
BACKDROP_HALF_OPEN_LINE     equ 90
BACKDROP_FULL_CLOSE_LINE    equ 3
BACKDROP_FULL_OPEN_LINE     equ 180


;ROOM_COLOR_BROWN            equ $F2
ROOM_COLOR_BROWN            equ $20
ROOM_COLOR_TRUNK            equ $F2
ROOM_COLOR_TRUNK_B          equ $F0
ROOM_COLOR_GRAY             equ $06
ROOM_COLOR_DARK_GRAY        equ $02
ROOM_COLOR_GREEN            equ $C4
ROOM_COLOR_DARK_GREEN       equ $D0
ROOM_COLOR_DARK_GREEN_B     equ $D2
ROOM_COLOR_TAN              equ $2C

ROOM_COLOR_RIVER            equ $80
ROOM_COLOR_LAVA             equ $40

COLOR_THEME_D1              equ 0
COLOR_THEME_OUTDOOR_ROCKS_1 equ 1
COLOR_THEME_OUTDOOR_ROCKS_2 equ 2
COLOR_THEME_TREES_1         equ 3
COLOR_THEME_TREES_2         equ 4
COLOR_THEME_RIVER           equ 5
COLOR_THEME_DARK_CAVE       equ 6
COLOR_THEME_BIG_TREE        equ 7
COLOR_THEME_RIVER_START     equ 8
COLOR_THEME_CASTLE          equ 9
COLOR_THEME_DESERT          equ 10
COLOR_THEME_D2              equ 11
COLOR_THEME_D3              equ 12
COLOR_THEME_D4              equ 13
COLOR_THEME_D5              equ 14
COLOR_THEME_WASTELAND       equ 15
COLOR_THEME_LAVA            equ 16

BYTES_PER_ROOM              equ 6

ROOMDEF_INDEX               equ 0
ROOMDEF_COLOR               equ 1
ROOMDEF_DOOR_FLAGS          equ 2
ROOMDEF_ENEMY_DEF           equ 3
ROOMDEF_NUM_ENEMIES         equ 4
ROOMDEF_ITEM_AND_LOCK       equ 5

LOCK_COLOR_MASK             equ $0F
ITEM_MASK                   equ $F0

TOP_OPEN                    equ %00000000
TOP_CLOSED                  equ %11000000
TOP_KEY                     equ %01000000
TOP_ENEMY                   equ %10000000

BOTTOM_OPEN                 equ %00000000
BOTTOM_CLOSED               equ %00110000
BOTTOM_KEY                  equ %00010000
BOTTOM_ENEMY                equ %00100000

LEFT_OPEN                   equ %00000000
LEFT_CLOSED                 equ %00001100
LEFT_FAKE                   equ %00000100
LEFT_ENEMY                  equ %00001000

RIGHT_OPEN                  equ %00000000
RIGHT_CLOSED                equ %00000011
RIGHT_FAKE                  equ %00000001
RIGHT_ENEMY                 equ %00000010

ROOM_ITEM_NONE              equ ITEM_NONE<<4
ROOM_ITEM_KEY               equ ITEM_KEY<<4

ROOMS_SHIFT_HORIZ           equ 14
ROOMS_SHIFT_HORIZ_BYTES     equ ROOMS_SHIFT_HORIZ*BYTES_PER_ROOM

DOOR_TOP_Y_COLLIDE          equ MAX_Y - 5
DOOR_BOTTOM_Y_COLLIDE       equ 5

LINES_PER_SECTION equ 23
LINES_PER_ROOM equ LINES_PER_SECTION*3

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; destroys Y, puts value in A
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC LdaRoomDefY
    SUBROUTINE
        jsr LdaRoomDefYBank1Jump
    ENDM


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Checks to see if we need to move to a new room
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC CheckForNewRoom
    SUBROUTINE

        IfFlag2Set #MISC_FLAGS2_WARP_UP,goRoomUp

        lda PlayerY
        sbc #4
        bpl .DontGoDown

        ;warp between areas
        ;lda RoomDef
        ;cmp #<RoomDef20
        ;bne .NoWarp
        ;lda RoomDef+1
        ;cmp #>RoomDef20
        ;bne .NoWarp
        ;lda #<Overworld00
        ;sta RoomDef
        ;lda #>Overworld00
        ;sta RoomDef+1
        ;jmp .downAfterWarp
;.NoWarp
        ;Go Down
        clc
        lda RoomDef
        adc #BYTES_PER_ROOM
        sta RoomDef
        lda RoomDef+1
        adc #0
        sta RoomDef+1

.downAfterWarp

        lda #HALF_SCANLINES-1-6
        sta PlayerY
        sta PlayerLastY

        inc MapPosition
        bne .initWithRecenterX ;optimized jmp

        ;jsr InitRoom
        ;jmp .RecenterX

.DontGoDown
        lda PlayerY
        cmp #HALF_SCANLINES
        bne .DontGoUp

        ;Go Up
goRoomUp
        lda RoomDef
        sec
        sbc #BYTES_PER_ROOM
        sta RoomDef
        lda RoomDef+1
        sbc #0
        sta RoomDef+1

        lda #1+18
        sta PlayerY
        sta PlayerLastY
        ;lda #CENTER_X
        ;sta PlayerLastX

        ClearFlag2 #MISC_FLAGS2_WARP_UP

        dec MapPosition
        ;TODO ENDING
        lda MapPosition
        cmp #$0F
        bne .initWithRecenterX
        lda #$FF
        sta PlayerMaxHP
        jmp GotoTitle
        ;BankJump Title,BANK_B


        

.initWithRecenterX
        jsr InitRoom
        jmp .RecenterX

.DontGoUp

        lda PlayerX
        cmp #1
        bne .DontGoLeft

        ;Go Left
        lda RoomDef
        sec
        sbc #ROOMS_SHIFT_HORIZ_BYTES
        sta RoomDef
        lda RoomDef+1
        sbc #0
        sta RoomDef+1

        lda #MAX_PLAYER_X-1-5
        sta PlayerX
        sta PlayerLastX

        lda MapPosition
        sec
        sbc #$10
        sta MapPosition
        jmp .initRoomWithRecenterY


        ;jsr InitRoom
        ;jmp .RecenterY

.DontGoLeft
        cmp #MAX_PLAYER_X
        bne .DontChangeRooms

        ;Go Right
        clc
        lda RoomDef
        adc #ROOMS_SHIFT_HORIZ_BYTES
        sta RoomDef
        lda RoomDef+1
        adc #0
        sta RoomDef+1

        lda #3 + 3
        sta PlayerX
        sta PlayerLastX

        lda MapPosition
        clc
        adc #$10
        sta MapPosition

.initRoomWithRecenterY
        jsr InitRoom
        ;jmp .RecenterY

.RecenterY
        ;re-center slightly
        lda #CENTER_Y
        sta PlayerLastY
        lda PlayerY
        cmp #CENTER_Y+4
        bpl .YCenterUpward
        inc PlayerY
        Skip2Bytes
.YCenterUpward
        dec PlayerY
        jmp .DontChangeRooms

.RecenterX
        lda RoomFGColor
        cmp #ROOM_COLOR_RIVER
        beq .WaterUncenter
        lda #CENTER_X
        sta PlayerLastX
        ;bne .AfterRecenter

.WaterUncenter
        ;re-center slightly
        ;lda #CENTER_X
        ;sta PlayerLastX
        lda PlayerX
        sbc #CENTER_X
        bpl .XCenterRight
        inc PlayerX
        jmp .AfterRecenter
.XCenterRight
        dec PlayerX
.AfterRecenter

.DontChangeRooms
    ENDM




     
    




    MAC SetBackgroundFlags
    SUBROUTINE
        lda RoomBGColor
        sta COLUBK
        lda RoomFGColor
        sta COLUPF

        IfFlag2Clear #MISC_FLAGS2_DARK,SetNormalBGFlags
        lda FrameCtr
        lsr
        bcc SetNormalBGFlags
        lda #BG_REFLECT|NUSIZ_MISSILE_4|CTRLPF_PRIORITY_BG
        Skip2Bytes
SetNormalBGFlags
        lda #BG_REFLECT|NUSIZ_MISSILE_4
        sta CTRLPF ; reflect background

        lda #0
        sta ENABL

        IfFlagClear #MISC_FLAGS_ENABLE_BALL,DoneSetBackgroundFlags


        ldx #MISSILE_BALL_ENABLE
        stx ENABL

DoneSetBackgroundFlags
    ENDM

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Prepare the background for the kernel renderer
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC PrepareBackground
    SUBROUTINE

        lda #%11111111
        sta PF0
        sta PF1
        sta PF2

    ENDM

    MAC CloseBothDoors
        lda #%00010000
        sta SideDoorOrVal
    ENDM

    
    MAC CheckDoorSide
    SUBROUTINE
.CheckDoorLeft
        lda Temp
        and #{1}_CLOSED         ;if no flags (open), don't touch it
        beq .DoneCheckDoorLeft
        cmp #{1}_CLOSED         ;if closed, close it
        beq .CloseLeftDoor
        cmp #{1}_ENEMY          ;if enemy, jump to that part
        beq .CheckIfEnemiesAlive
        cmp #{1}_FAKE           ;if fake, fall through
        bne .OpenLeftDoor       ;else open the door

        ;close the door, but also mark as fake
        SetFlag #MISC_FLAGS_BALL_WALL_FAKE

.CloseLeftDoor
        PositionObjectAtAbsolute OBJ_BL,{2}
        SetFlag #MISC_FLAGS_ENABLE_BALL

        inc TempPF1

        ;jmp .DoneCheckDoorLeft
        bne .DoneCheckDoorLeft ;optimized jmp

.CheckIfEnemiesAlive
        ;Can't do this MACro as it needs absolute labels...
        ;IfFlagClear #MISC_FLAGS_ENEMIES_ALIVE,OpenLeftDoor
        ;so this is the reproduction of it....
        lda MiscFlags
        and #MISC_FLAGS_ENEMIES_ALIVE
        beq .OpenLeftDoor

        ;jmp .CloseLeftDoor
        bne .CloseLeftDoor ;optimized jmp


.OpenLeftDoor
        lda TempPF1
        bne .DoneCheckDoorLeft
        ClearFlag #MISC_FLAGS_ENABLE_BALL

.DoneCheckDoorLeft
    ENDM


    MAC CheckDoorVert
    SUBROUTINE
.xyz
        ClearFlag #MISC_FLAGS_{1}_CLOSED
        ldx RoomBGColor
        lda TempPF1
        and #{1}_CLOSED
        beq .DoneCheckDoorUp ;if no flags, done
        cmp #{1}_CLOSED
        beq .CloseTopDoor
        cmp #{1}_ENEMY
        beq .CheckTopDoorAgainstEnemies
        cmp #{1}_KEY
        beq .SetTopDoorKey
        ;jmp .DoneCheckDoorUp ; not sure how it would get here?

.SetTopDoorKey
        ldy #ROOMDEF_ITEM_AND_LOCK
        LdaRoomDefY
        and #LOCK_COLOR_MASK

        ;special case for switches
        cmp #7
        bne .notEight
        lda #%10000000
        bne .checkSwitch
.notEight
        cmp #6
        bne .notSeven
        lda #%01000000
.checkSwitch
        and KeysOwned
        bne .DoneCheckDoorUp

.notSeven

        ;;;;;

.LockDoor
        AccDoorToColor
        tax

        ;Setflag destroys A but not X
        SetFlag #MISC_FLAGS_{1}_CLOSED

        ;Put the color from X to the door color
        ;jmp .DoneCheckDoorUp
        bne .DoneCheckDoorUp ;optimized jmp

.CheckTopDoorAgainstEnemies
        lda MiscFlags
        and #MISC_FLAGS_ENEMIES_ALIVE
        beq .DoneCheckDoorUp
        bne .CloseTopDoor
.CloseTopDoor
        SetFlag #MISC_FLAGS_{1}_CLOSED
        ldx RoomFGColor      ;just close it
.DoneCheckDoorUp
        stx {2}DoorColor
    ENDM


    
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Background drawing routine in the kernel
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    MAC InitBackgroundIndexed
    SUBROUTINE
        ;lda #<StartRoom
        ;sta RoomDef
        ;lda #>StartRoom
        ;sta RoomDef+1

        lda #<RoomDef_0_0
        sta RoomDef
        lda #>RoomDef_0_0
        sta RoomDef+1

        lda Respawn
        sta MapPosition
        and #$0F
        tax
.yTop
        beq .yDone
        lda #BYTES_PER_ROOM
        clc
        adc RoomDef
        sta RoomDef
        lda #0
        adc RoomDef+1
        sta RoomDef+1
        dex
        jmp .yTop
.yDone

        lda Respawn
        and #$F0
        clc
        ror
        ror
        ror
        ror
        tax
.xTop
        beq .xDone
        lda #BYTES_PER_ROOM*14
        clc
        adc RoomDef
        sta RoomDef
        lda #0
        adc RoomDef+1
        sta RoomDef+1
        dex
        jmp .xTop
.xDone

        ;see if we need to nudge
        ;downward when respawning
        ;in castle (d4/d5) entrances
        lda Respawn
        cmp #$55
        beq .moveDown
        cmp #05
        bne .afterMoveDown
.moveDown
        ;nudge downward for castle respawns
        lda PlayerY
        sbc #32
        sta PlayerY
        sta PlayerLastY
.afterMoveDown
        lda Respawn
        eor #07
        bne .afterThreeAdjust
        lda PlayerX
        adc #10
        sta PlayerX
        sta PlayerLastX

.afterThreeAdjust
        lda #%00000001
        sta VDELBL

    ENDM

    MAC CheckAllDoors
        lda #0
        sta SideDoorOrVal
        ldy #ROOMDEF_DOOR_FLAGS
        LdaRoomDefY
        sta Temp
        lda #0
        sta TempPF1
        CheckDoorSide LEFT,5
        CheckDoorSide RIGHT,181
        dec TempPF1
        dec TempPF1
        bmi .notBothClosed
        CloseBothDoors
.notBothClosed
        lda Temp
        sta TempPF1
        CheckDoorVert TOP,Top
        CheckDoorVert BOTTOM,Bottom
    ENDM

    MAC PrepareBackgroundDoubleIndexed
    SUBROUTINE
        ldx #0
        stx PlayerCollisionFlags
        stx Temp
.sectionLoop
        jsr BurnFrameB
        lda Temp
        clc
        adc Temp
        tax
        

        lda #<RoomGraphics
        sta LineDefA,X
        lda #>RoomGraphics
        sta LineDefA+1,X


        ldy #0
        lda (RoomDef),Y
        adc (RoomDef),Y
        adc (RoomDef),Y

        adc Temp
        tay
        lda RoomLayoutSections,Y

        ;now do like we did in prepareBackgroundIndexed
        beq .DoneMultiplying
        tay
.MultiplyLoop
        clc
        lda LineDefA,X
        adc #LINES_PER_SECTION
        sta LineDefA,X
        lda LineDefA+1,X
        adc #0
        sta LineDefA+1,X
        dey
        bne .MultiplyLoop
.DoneMultiplying
        inc Temp
        lda Temp
        sec
        cmp #3
        bne .sectionLoop


    ENDM


    MAC PrepareBackgroundIndexed
    SUBROUTINE


        ;lda #<RoomGraphics
        ;sta LineDefA
        ;lda #>RoomGraphics
        ;sta LineDefA+1

        ;ldy #0
        ;LdaRoomDefY
        ;beq .DoneMultiplying
        ;tax
;.MultiplyLoop
        ;clc
        ;lda LineDefA
        ;adc #LINES_PER_ROOM
        ;sta LineDefA
        ;lda LineDefA+1
        ;adc #0
        ;sta LineDefA+1
        ;dex
        ;bne .MultiplyLoop
;.DoneMultiplying
        ;clc
        ;lda LineDefA
        ;adc #LINES_PER_SECTION
        ;sta LineDefB
        ;lda LineDefA+1
        ;adc #0
        ;sta LineDefB+1

        ;clc
        ;lda LineDefB
        ;adc #LINES_PER_SECTION
        ;sta LineDefC
        ;lda LineDefB+1
        ;adc #0
        ;sta LineDefC+1

        ;prepare the ball and right/left doors

        lda #0
        sta SideDoorOrVal

        ldy #ROOMDEF_DOOR_FLAGS
        LdaRoomDefY
        sta Temp
        lda MiscFlags
        and #(~(MISC_FLAGS_ENABLE_BALL|MISC_FLAGS_ITEM_SHOWING|MISC_FLAGS_ITEM_GOTTEN))
        sta MiscFlags
        lda Temp
        eor #LEFT_CLOSED|RIGHT_CLOSED
        and #LEFT_CLOSED|RIGHT_CLOSED
        bne .DontCloseBoth
        ; close both left and right
        ; using SideDoorOrVal
        lda #%00010000
        sta SideDoorOrVal

.DontCloseBoth
        jsr CheckAllDoorsSub
    ENDM




    MAC KernelDrawBackgroundIndexed
    SUBROUTINE
        txa             ;2
        lsr             ;2
        lsr             ;2
        tay             ;2  (8)

        lda (LineDefA),Y    ;6
        ora SideDoorOrVal
        sta PF0             ;3
        lda (LineDefB),Y    ;6
        sta PF1             ;3
        lda (LineDefC),Y    ;6
        sta PF2             ;3  (27 + 8 = 35)
        sty TempPF1
.BackgroundDone
    ENDM

