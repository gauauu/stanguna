;;;;;;;;;;;;;;;;;;;;;;;;;;
; Enemy-related constants
;;;;;;;;;;;;;;;;;;;;;;;;;;
ENEMY_RIGHT_THRESHOLD equ 140
ENEMY_LEFT_THRESHOLD equ 1
ENEMY_TOP_THRESHOLD equ DOOR_TOP_Y_COLLIDE
ENEMY_BOTTOM_THRESHOLD equ 17

ENEMY_SIZE_QUAD     equ 7
ENEMY_SIZE_DOUBLE   equ 5
ENEMY_SIZE_NORMAL   equ 0


ENEMY_DEF_ANIM0     equ 0
ENEMY_DEF_ANIM1     equ 2
ENEMY_DEF_HEIGHT    equ 4
ENEMY_DEF_COLOR     equ 5
ENEMY_DEF_UPDATE    equ 6
ENEMY_DEF_HP        equ 8
ENEMY_DEF_DMG       equ 9
ENEMY_DEF_SIZE      equ 10

ENEMY_HP_INVINCIBLE equ #127
ENEMY_HP_DYNAMITE   equ #126
ENEMY_DMG_NONE      equ #$FF
ENEMY_DMG_BARRIER   equ #$FE

ENEMY_TYPE_SLIME            equ 0
ENEMY_TYPE_GREENSLIME       equ 1
ENEMY_TYPE_ORANGESLIME      equ 2
ENEMY_TYPE_GREENTOADY       equ 3
ENEMY_TYPE_BAT              equ 4
ENEMY_TYPE_CROCKY           equ 5
ENEMY_TYPE_CAVEENTRANCE     equ 6
ENEMY_TYPE_SNAKE            equ 7
ENEMY_TYPE_BUG              equ 8
ENEMY_TYPE_REDBUG           equ 9
ENEMY_TYPE_FLYING_BAT       equ 10
ENEMY_TYPE_SPIDER           equ 11
ENEMY_TYPE_JUMPER           equ 12
ENEMY_TYPE_JUMPER_BOSS      equ 13
ENEMY_TYPE_TRAP             equ 14
ENEMY_TYPE_OVERLAY          equ 15
ENEMY_TYPE_GHOST            equ 16
ENEMY_TYPE_GHOST_BOSS       equ 17
ENEMY_TYPE_BOULDER          equ 18
ENEMY_TYPE_BOSS_3           equ 19
ENEMY_TYPE_BOSS_4           equ 20
ENEMY_TYPE_BOSS_5           equ 21
ENEMY_TYPE_SKELETON         equ 22
ENEMY_TYPE_REDTOADY         equ 23
ENEMY_TYPE_SWITCH           equ 24

    MAC LdaEnemyDefY
        jsr LdaEnemyDefYBank1Jump
    ENDM

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Initializes enemies.
    ; Assumes EnemyDef is already set up
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;    MAC InitEnemies
;    SUBROUTINE
;        
;
;        lda #0
;        ;STA Enemy0SpriteBuffer
;        sta REFP1
;        sta GRP1
;        sta HMP1
;        
;        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;        PrepareSpriteLookupTable SlimeFrame0,EnemySpriteTable
;        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
;        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;        ; custom test data for development
;        ; initializes 2 slimes positions
;        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;        lda #20
;        sta Enemy1X
;        sta Enemy1Y
;        sta Enemy2Y
;        lda #%00001010
;        sta Enemy0Dir
;        lda #%00000110
;        sta Enemy1Dir
;        sta Enemy2Dir
;
;        lda #1
;        sta Enemy0HP    ;set them up with some hit points
;        sta Enemy1HP
;        sta Enemy2HP
;
;        lda #85
;        sta Enemy0Y
;        sta Enemy2X
;
;        lda #110
;        sta Enemy0X
;
;
;
;        PositionObjectAtAcc 1
;
;        lda #ANIMATION_TIME
;        sta EnemyAnimCounter
;
;        lda #<EnemyDefSlime     ;load the enemy definition
;        sta EnemyDef
;        lda #>EnemyDefSlime
;        sta EnemyDef+1
;
;    ENDM
;
    MAC ClearEnemies
    SUBROUTINE
        lda #0
        ldx #EnemyCollisionFlags2 - Enemy0X
.ClearEnemiesLoop
        sta Enemy0X,X
        dex
        bne .ClearEnemiesLoop

    ENDM

    MAC SpawnRoomEnemies
    SUBROUTINE

.SpawnRoomEnemies
        lda #ANIMATION_TIME
        sta EnemyAnimCounter

        ;load the enemy definition from the room def
        ldy #ROOMDEF_ENEMY_DEF 
        LdaRoomDefY
        cmp #24
        bne .normal
        lda #<EnemyDefSwitch
        sta EnemyDef
        lda #>EnemyDefSwitch
        sta EnemyDef+1
        bne .afterLoadDef

.normal
        tax
        beq .multiplyBottom
        lda #0
        clc
.multiplyTop
        adc #11
        dex
        bne .multiplyTop
        
.multiplyBottom
        clc
        adc #<EnemyDefSlime
        sta EnemyDef
        lda #0
        adc #>EnemyDefSlime
        sta EnemyDef+1

        ;sta EnemyDef
        ;iny
        ;LdaRoomDefY
        ;sta EnemyDef+1


.afterLoadDef
        ;figure out how many enemies we have, put it in X
        ldy #ROOMDEF_NUM_ENEMIES
        LdaRoomDefY
        beq .DontSpawnEnemies
        tax
        sta TempPF2

        ldy #ENEMY_DEF_HEIGHT
        LdaEnemyDefY
        sta EnemyHeight

        ldy #ENEMY_DEF_COLOR
        ;lda (EnemyDef),Y
        LdaEnemyDefY
        sta EnemyColor

        ldy #ENEMY_DEF_SIZE
        LdaEnemyDefY
        sta EnemyScaleNuSiz

        ldy #ENEMY_DEF_HP
        ;lda (EnemyDef),Y
        LdaEnemyDefY
        tay ; put the enemy HP in Y
        dex
.SpawnEnemyLoop

        SpawnEnemy


        dex 
        bpl .SpawnEnemyLoop

        jsr SetEnemyPositionsFromSafeSpawns

        SetFlag #MISC_FLAGS_ENEMIES_ALIVE

.PrepareSpawnLookup
        ;Prepare the SpriteLookupTable (destroys Y!)
        ldy #0
        ;lda (EnemyDef),Y
        LdaEnemyDefY
        sec
        sbc #1
        sta EnemySpriteTable
        iny
        ;lda (EnemyDef),Y
        LdaEnemyDefY
        sta EnemySpriteTable+1
.DontSpawnEnemies
        lda #0
        sta Enemy0State0
        sta Enemy0State0+1
        sta Enemy0State0+2

    ENDM

    MAC RandomizeEnemyXPosition
        jsr GetRandomNumber
        and #%01111111
        adc #10
        sta Enemy0X,X
        sta Enemy0LastX,X
        ;STA Enemy0LastShownX,X
    ENDM

    MAC RandomizeEnemyYPosition
        jsr GetRandomNumber
        and #%00111111
        adc #14
        sta Enemy0Y,X
        sta Enemy0LastY,X
        ;STA Enemy0LastShownY,X
    ENDM

    MAC SpawnEnemy
    SUBROUTINE
        ;store the enemy HP
        tya
        sta Enemy0HP,X

        ;RandomizeEnemyXPosition
        ;RandomizeEnemyYPosition

    ENDM

 
    MAC CompareSwap
    SUBROUTINE
        lda Enemy0HP+{1}    ; if the first enemy has 0 HP, always swap
        beq .ForceSwap
        lda Enemy0HP+{2}    ; if the 2nd enemy has 0 HP, never swap
        beq .SkipSwap
        lda Enemy0Y+{1}
        cmp Enemy0Y+{2}
        bpl .SkipSwap
.ForceSwap
        ldx #{1}
        ldy #{2}
        jsr SwapEnemies
.SkipSwap
    ENDM

    



    MAC IfNoWallCollision
        lda EnemyCollisionFlags0,X
        and #COL_FLAG_WALL
        beq {1}
    ENDM

    MAC IfWallCollision
        lda EnemyCollisionFlags0,X
        and #COL_FLAG_WALL
        bne {1}
    ENDM


    MAC CheckIfAllEnemiesDead
    SUBROUTINE
        lda Enemy0HP
        ora Enemy1HP
        ora Enemy2HP
        bne .EnemiesAlive

        ClearFlag #MISC_FLAGS_ENEMIES_ALIVE
        jmp .EndCheckIfAllEnemiesDead
.EnemiesAlive
        SetFlag #MISC_FLAGS_ENEMIES_ALIVE
.EndCheckIfAllEnemiesDead
    ENDM


    MAC UpdateEnemies

        ;if we're in item mode, 
        ;jump to the specialized
        ;item update routine in items.asm

        IfFlagClear #MISC_FLAGS_ITEM_SHOWING,NormalUpdateEnemies
        jsr UpdateEnemies_Item
        jmp AfterUpdateEnemies


        ;otherwise, update the enemies as normal

NormalUpdateEnemies
        CheckIfAllEnemiesDead
        CheckIfSpawnItem
        IfFlagClear #MISC_FLAGS_ENEMIES_ALIVE,CheckUpdateDoors
        bne AfterUpdateDoors ;optimized jmp

CheckUpdateDoors
        IfFlag2Set #MISC_FLAGS2_HANDLED_ENEMIES_DEAD,AfterUpdateDoors
        jsr CheckAllDoorsSub
        SetFlag2 #MISC_FLAGS2_HANDLED_ENEMIES_DEAD

AfterUpdateDoors

        IfFlagClear #MISC_FLAGS_ITEM_SHOWING,NormalUpdateEnemies2
        bne AfterUpdateEnemies ;optimized jmp

NormalUpdateEnemies2
        

        ldx #0
NormalUpdateEnemiesLoop
        jsr UpdateSingleEnemy
        inx
        txa
        eor #3
        bne NormalUpdateEnemiesLoop
        ;IfWallCollision AfterCopy0
        ;IfEnemyHidden 0,AfterCopy0
        ;jsr CopyEnemyPosToLastPos
;AfterCopy0
        ;jsr UpdateSingleEnemy

        ;ldx #1
        ;IfWallCollision AfterCopy1
        ;IfEnemyHidden 1,AfterCopy1
        ;jsr CopyEnemyPosToLastPos
;AfterCopy1
        ;jsr UpdateSingleEnemy

        ;ldx #2
        ;IfWallCollision AfterCopy2
        ;IfEnemyHidden 2,AfterCopy2
        ;jsr CopyEnemyPosToLastPos
;AfterCopy2
        ;jsr UpdateSingleEnemy

        ;and clear collision flags
        lda #0
        sta EnemyCollisionFlags0
        sta EnemyCollisionFlags1
        sta EnemyCollisionFlags2



        ;;;;;;;;;;;;;;;;;;;;;;
        UpdateEnemyAnimations
        ;;;;;;;;;;;;;;;;;;;;;;
        jsr SortEnemies
AfterUpdateEnemies

    ENDM



    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Updates animations for enemy(s).
    ; Assumes that all enemies have a 2-sprite animation
    ; with the same timing (is this correct?)
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC UpdateEnemyAnimations
    SUBROUTINE

        dec EnemyAnimCounter
        bne EnemyUpdateDone
        lda #ANIMATION_TIME
        sta EnemyAnimCounter
        lda EnemyAnimFrame
        beq .DoEnemyFrame2

        lda #0
        ldy #0
        beq .storeAndGo

.DoEnemyFrame2

        lda #1
        ldy #2

.storeAndGo
        sta EnemyAnimFrame
        BankJump PrepareEnemySpriteLookupTable,A
EnemyUpdateDone

    ENDM




    MAC SwapEnemyValues
        lda {1},X
        ;sta Temp
        pha
        lda {1},Y
        sta {1},X
        ;lda Temp
        pla
        sta {1},Y
    ENDM


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; MACros just to simplify the long kernel selection routines below
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;Checks to see if 2 enemies are close to each other,
    ; Params:
    ; Enemy index 1
    ; Enemy index 2
    ; symbol to jump to
    MAC IfEnemiesClose
    SUBROUTINE 
        lda Enemy0Y+{1}
        sec
        sbc Enemy0Y+{2}
        cmp #14
        bpl .KeepGoing
        jmp {3}
.KeepGoing
    ENDM

    MAC IfEnemyHidden
    SUBROUTINE
.IfEnemyHiddenStart
        lda KernelMode
        IF {1}==0
        and #%11100000
        ENDIF
        IF {1}==1
        and #%01100000
        ENDIF
        IF {1}==2
        and #%00100000
        ENDIF
        beq {2}
    ENDM

    ;Sets the kernel mode
    ; Params:
    ; KernelMode
    MAC SetKernelMode
        lda {1}
        sta KernelMode
        lda #KERNEL_BOTTOM_DOOR_CUTOFF
        sta KernelSwitch1
    ENDM

    MAC SwapTheEnemies
        ldx {1}
        ldy {2}
        jsr SwapEnemies 
    ENDM

    ;params:
    ;   which enemy
    ;   which kernelSwitch
    MAC SetKernelSwitchAt
        lda Enemy0Y+{1}
        ldx #{2}
        jsr SetKernelSwitch
    ENDM

    MAC IfKernelModeAlready
        lda KernelMode   
        cmp {1}
        beq {2}
    ENDM

    MAC SaveLastDrawnPosition
        lda Enemy0LastX+{1}
        sta Enemy0LastShownX+{1}
        lda Enemy0LastY+{1}
        sta Enemy0LastShownY+{1}
    ENDM

    MAC MaybeFixDarkness
    SUBROUTINE
        IfFlag2Clear #MISC_FLAGS2_DARK,AfterDarkFix
 
        ;restore color,height,sprite table
        BankJump RestoreEnemyAfterDark,BANK_A
AfterDarkFix
    ENDM



    MAC PrepareEnemies
    SUBROUTINE
StartPrepareEnemies
        IfFlag2Clear #MISC_FLAGS2_DARK,AfterDarkPrep
        lda FrameCtr
        lsr
        bcc DarkCheckIfShowEnemy

        ; sort enemy(s) to bottom
        BankJsr SwapEnemy02,BANK_A

        ; put dark enemy in right x/y position
        lda PlayerX
        sbc #12
        bpl .skipZeroReset
        lda PlayerX
        sbc #100
        bmi .setToZero
        lda #ENEMY_RIGHT_THRESHOLD-15
        Skip2Bytes
.setToZero
        lda #0
.skipZeroReset
        sta Enemy0X
        lda PlayerY
        adc #7
        sta Enemy0Y

        ;;;X save off enemy color, height, sprite table
        ; update dark color, height, sprite table
        lda #$1E
        sta EnemyColor
        lda #23
        sta EnemyHeight
        lda #<LightSurround
        sta EnemySpriteTable
        lda #>LightSurround
        sta EnemySpriteTable+1
        lda #ENEMY_SIZE_QUAD
        sta EnemyScaleNuSiz


        ; set kernel to KERNEL_MODE_1
        lda #KERNEL_MODE_1
        sta KernelMode

        jmp .DoneWithPreparingEnemies

DarkCheckIfShowEnemy

        ;hide enemy if far away
        lda Enemy0Y
        sbc PlayerY
        bpl .foundabs
        eor #$ff
        clc; not needed if the state of C is known up front
        adc #1; would be "adc #0", if C is known to be set
.foundabs
        sbc #15
        bpl .hideEnemy

        lda Enemy0X
        sbc PlayerX
        bpl .foundabs2
        eor #$ff
        clc; not needed if the state of C is known up front
        adc #1; would be "adc #0", if C is known to be set
.foundabs2
        sbc #15
        bmi .dontHideEnemy

.hideEnemy
        lda #0
        sta EnemyColor



.dontHideEnemy

AfterDarkPrep
        lda #KERNEL_BOTTOM_DOOR_CUTOFF
        sta KernelSwitch1
        sta KernelSwitch2
        sta KernelSwitch3

                                ;at this point, we assume that
                                ;updateEnemies already took care
                                ; of swapping enemies based
                                ; on who's actually positioned first
                                    
        lda Enemy0HP
        bne AtLeastOneEnemy
        IfFlagSet #MISC_FLAGS_ITEM_SHOWING,AtLeastOneEnemy
        SetKernelMode #KERNEL_MODE_0
        jmp .DoneWithPreparingEnemies


AtLeastOneEnemy
        lda Enemy1HP
        bne .AtLeastTwoEnemies
        SetKernelMode #KERNEL_MODE_1
        SaveLastDrawnPosition 0
        jmp .DoneWithPreparingEnemies

.AtLeastTwoEnemies
        lda Enemy2HP
        beq .NotThreeEnemies
        jmp .ThreeEnemies
.NotThreeEnemies

        IfEnemiesClose 0,1,SetKernelMode_12
        jmp .SetKernelMode_1_2

SetKernelMode_12
        IfKernelModeAlready #KERNEL_MODE_12_A,SetKernelMode_12_B

.SetKernelMode_12_A
        SetKernelMode #KERNEL_MODE_12_A
        SaveLastDrawnPosition 0
        jmp .DoneWithPreparingEnemies

SetKernelMode_12_B
        ;SwapTheEnemies 0,1
        BankJsr SwapEnemy01,BANK_A

        SetKernelMode #KERNEL_MODE_12_B
        SaveLastDrawnPosition 0
        jmp .DoneWithPreparingEnemies

.SetKernelMode_1_2
        SetKernelMode #KERNEL_MODE_1_2
        SetKernelSwitchAt 1,0

        ;LDA Enemy0X             ; and go ahead and position
        ;PositionObjectAtAcc 1   ; enemy 0

        SaveLastDrawnPosition 0
        SaveLastDrawnPosition 1
        jmp .DoneWithPreparingEnemies

.ThreeEnemies
        IfEnemiesClose 0,1,ThreeEnemiesTopOverlap   

        IfEnemiesClose 1,2,SetKernelMode_1_23     

        SetKernelMode #KERNEL_MODE_1_2_3
        SetKernelSwitchAt 1,0
        SetKernelSwitchAt 2,1

        SaveLastDrawnPosition 0
        SaveLastDrawnPosition 1
        SaveLastDrawnPosition 2
        jmp .DoneWithPreparingEnemies

ThreeEnemiesTopOverlap
        IfEnemiesClose 1,2,SetKernelMode_123

        IfKernelModeAlready #KERNEL_MODE_12_3_A,SetKernelMode_12_3_B

        SetKernelMode #KERNEL_MODE_12_3_A
        ;SwapTheEnemies 1,2    ;move enemy 2 up so it gets rendered, and 1 gets hidden
        BankJsr SwapEnemy12,BANK_A

        SetKernelSwitchAt 1,0

        SaveLastDrawnPosition 0
        SaveLastDrawnPosition 1
        jmp .DoneWithPreparingEnemies

SetKernelMode_12_3_B
        SetKernelMode #KERNEL_MODE_12_3_B
        ;SwapTheEnemies 0,1      ;swap enemy 0 to the end, so 1 and 2 get rendered
        ;SwapTheEnemies 1,2
        BankJsr SwapEnemy0112,BANK_A

        SetKernelSwitchAt 1,0

        SaveLastDrawnPosition 0
        SaveLastDrawnPosition 1
        jmp .DoneWithPreparingEnemies

SetKernelMode_1_23
        IfKernelModeAlready #KERNEL_MODE_1_23_A,SetKernelMode_1_23_B
        SetKernelMode #KERNEL_MODE_1_23_A
        SetKernelSwitchAt 1,0

        SaveLastDrawnPosition 0
        SaveLastDrawnPosition 1
        jmp .DoneWithPreparingEnemies

SetKernelMode_1_23_B
        SetKernelMode #KERNEL_MODE_1_23_B
        ;SwapTheEnemies 1,2
        BankJsr SwapEnemy12,BANK_A
        SetKernelSwitchAt 1,0

        SaveLastDrawnPosition 0
        SaveLastDrawnPosition 1
        jmp .DoneWithPreparingEnemies

SetKernelMode_123
        SaveLastDrawnPosition 0
        ;if already in 123_A, switch to 123_B (and swap 1 and 2)
        ;if already in 123_B, switch to 123_C (and swap 1 and 3)
        ;otherwise, do 123_A
        IfKernelModeAlready #KERNEL_MODE_123_A,SetKernelMode_123_B
        IfKernelModeAlready #KERNEL_MODE_123_B,SetKernelMode_123_C

        SetKernelMode #KERNEL_MODE_123_A

        jmp .DoneWithPreparingEnemies

SetKernelMode_123_B
        SetKernelMode #KERNEL_MODE_123_B
        ;SwapTheEnemies 0,1
        BankJsr SwapEnemy01,BANK_A

        jmp .DoneWithPreparingEnemies

SetKernelMode_123_C
        SetKernelMode #KERNEL_MODE_123_C
        ;SwapTheEnemies 0,2
        BankJsr SwapEnemy02,BANK_A

.DoneWithPreparingEnemies

        lda Enemy0Dir
        sta REFP1

    ENDM





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Enemy def Update MACros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


    MAC KernelDrawEnemy
    SUBROUTINE
        txa
        sec
        sbc Enemy{1}Y
        adc EnemyHeight
        bcc .DrawBlank
        tay
        lda (EnemySpriteTable),Y
        jmp .DrawEnemyDone
.DrawBlank
        lda #0
.DrawEnemyDone
    ENDM


    






