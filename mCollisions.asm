
    MAC CheckAndProcessCollisions
    SUBROUTINE
        IfFlag2Clear #MISC_FLAGS2_DARK,SkipCollisionSkipper
        lda FrameCtr
        lsr
        bcc .toSkipEnemyCollisions
        jmp SkipEnemyCollisions
.toSkipEnemyCollisions

SkipCollisionSkipper
        CheckEnemyMissileCollisions 
        CheckEnemyWallCollisions
        CheckEnemyPlayerCollisions
SkipEnemyCollisions
        CheckPlayerWallCollisions
        ; do this after player movement instead
        ; so the sliding works better
        ;ClearPlayerCollisionFlags
    ENDM

    MAC CheckEnemyWallCollisions
    SUBROUTINE
        IfFlagClear #MISC_FLAGS_ITEM_SHOWING,GoAheadWithWallCollision
        ;jmp .EnemyWallCollisionsDone
        bne .EnemyWallCollisionsDone ;optimized jmp
GoAheadWithWallCollision
        lda EnemyColor
        eor #ENEMY_COLOR_FLYING
        bne .notFlying
        beq .EnemyWallCollisionsDone ;optimized jmp

.notFlying
        LDX #2
.CheckNextEnemy
        lda Enemy0HP,X
        beq .DoneCheckingThisEnemy
        ;if the enemy is down in the bottom wall where
        ;we don't properly log collisions and draw enemies,
        ; count that as a wall collision
        lda Enemy0Y,X
        sec
        sbc EnemyHeight
        sbc #4
        bpl .AfterPositionBasedCollision
        lda EnemyCollisionFlags0,X
        ora #COL_FLAG_WALL
        sta EnemyCollisionFlags0,X
    
.AfterPositionBasedCollision
        LDA EnemyCollisionFlags0,X
        AND #COL_FLAG_WALL
        BEQ .DoneCheckingThisEnemy

        LDA Enemy0LastShownX,X
        STA Enemy0X,X
        LDA Enemy0LastShownY,X
        STA Enemy0Y,X
.DoneCheckingThisEnemy
        DEX
        BPL .CheckNextEnemy
.EnemyWallCollisionsDone

    ENDM

    MAC CheckEnemyMissileCollisions 
    SUBROUTINE
        IfFlagSet #MISC_FLAGS_ITEM_SHOWING,SkipEnemyMissileCollisions

        ; you can't hurt the boss with arrows
        ;lda EnemyColor
        ;cmp #ENEMY_COLOR_MAINBOSS
        ;bne .notBoss
        ;lda #MISC_FLAGS2_ARROW_ACTIVE
        ;bit MiscFlags2
        ;bne SkipEnemyMissileCollisions

.notBoss
        LDX #2
.CheckNextEnemy
        LDA EnemyCollisionFlags0,X
        AND #COL_FLAG_HURT
        BEQ .DoneCheckingThisEnemy
        JSR HurtSelectedEnemy
.DoneCheckingThisEnemy
        ;LDA #0
        ;STA EnemyCollisionFlags0,X
        DEX
        BPL .CheckNextEnemy
SkipEnemyMissileCollisions

    ENDM

    MAC ClearPlayerCollisionFlags
        LDA #0
        STA PlayerCollisionFlags
    ENDM


    MAC CheckEnemyPlayerCollisions 
    SUBROUTINE

        lda EnemyColor
        eor #ENEMY_COLOR_FADED
        beq .EPCollisionsDone

        LDA PlayerFaded
        BNE .EPCollisionsDone
        LDA PlayerCollisionFlags
        AND #COL_FLAG_HURT
        BEQ .EPCollisionsDone

        IfFlagClear #MISC_FLAGS_ITEM_SHOWING,NonItemCollision
        BankJump BankB_PickUpItem,BANK_B
AfterPickupItem
        ;JSR PickUpItem
        JMP .EPCollisionsDone



NonItemCollision
        LDY #ENEMY_DEF_DMG
        LdaEnemyDefY
        cmp #ENEMY_DMG_NONE
        beq .EPCollisionsDone
        cmp #ENEMY_DMG_BARRIER
        beq .enemyBarrier


        ldy RoomTransitionTimer
        beq .collisionsAsUsual
        ;jmp .EPCollisionsDone
        bne .EPCollisionsDone ;optimized jmp

.collisionsAsUsual

        sec
        sbc PlayerArmor
        beq .setToOne      ;on zero, set to one
        bpl .afterSetToOne ; if positive, skip
.setToOne
        lda #1              ;(on negative, fall through)
.afterSetToOne
        sta Temp
        LDA PlayerHP
        SEC
        SBC Temp
        STA PlayerHP

        bmi .dead
        beq .dead
        ;jmp .DontDie
        bne .DontDie ;optimized jmp
.dead
        ;JMP PlayerDied
        lda #-180 ; 3 seconds pause when dead
        sta FrameCtr
        lda #0
        sta PlayerHP
.DontDie
        PlaySound #SOUND_PLAYER_HURT

        LDA #PLAYER_FADE_TIME
        STA PlayerFaded
        ;jmp .EPCollisionsDone
        bne .EPCollisionsDone ;optimized jmp
.enemyBarrier
        lda PlayerLastX
        sta PlayerX
        lda PlayerLastY
        sta PlayerY
.EPCollisionsDone
    ENDM



    MAC CheckPlayerWallCollisions
    SUBROUTINE
zzz
        ;if we hit the ball, boots don't help
        LDA PlayerCollisionFlags
        and #COL_FLAG_BALL
        bne .noBoots

        lda RoomDef
        eor #<RoomDef_6_6
        bne .notCurve1
        lda RoomDef+1
        eor #>RoomDef_6_6
        bne .notCurve1
        lda PlayerX
        sbc #127
        bmi .notCurve1
        ClearPlayerCollisionFlags
        jmp DoneCheckingPlayerWallCollision
.notCurve1

        lda RoomDef
        eor #<RoomDef_4_6
        bne .notCurve2
        lda RoomDef+1
        eor #>RoomDef_4_6
        bne .notCurve2
        lda PlayerX
        sbc #25
        bpl .notCurve2
        ClearPlayerCollisionFlags
        jmp DoneCheckingPlayerWallCollision
.notCurve2

        ;first check for special exceptions with boots
        lda Inventory
        and #(1 << ITEM_BOOTS)
        beq .noBoots
        lda RoomFGColor
        cmp #ROOM_COLOR_RIVER
        beq DoneCheckingPlayerWallCollision
        cmp #ROOM_COLOR_LAVA
        beq DoneCheckingPlayerWallCollision
        lda MapPosition
        cmp #$45
        beq .specialPos
        ;cmp #$55
        ;beq .specialPos
        cmp #$65
        beq .specialPos
        jmp .noBoots
.specialPos
        lda PlayerY
        sbc #88
        bpl .noBoots
        jmp DoneCheckingPlayerWallCollision

.noBoots
        ;First check wall collisions based on collision flags
        LDA PlayerCollisionFlags
        AND #COL_FLAG_WALL
        BEQ DoneCheckingPlayerWallCollision
        AND #COL_FLAG_BALL
        BEQ .AfterBallCheck
.BallCheck

        IfFlagSet #MISC_FLAGS_BALL_WALL_FAKE,DoneCheckingPlayerWallCollision
.AfterBallCheck
        ;IfPlayerLastPositionUnset MovePlayerTowardCenterX

        LDA PlayerLastX
        STA PlayerX
        LDA PlayerLastY
        STA PlayerY

        JMP DoneCheckingPlayerWallCollision

;MovePlayerTowardCenterX
        ;LDA PlayerX
        ;CMP #CENTER_X
        ;BPL .SubtractX
        ;ADC #6
        ;STA PlayerX
        ;JMP MovePlayerTowardCenterY
;.SubtractX        
        ;SBC #6
        ;STA PlayerX
;

;MovePlayerTowardCenterY
        ;LDA PlayerY
        ;CMP #CENTER_Y
        ;BPL .SubtractY
        ;ADC #6
        ;STA PlayerY
        ;JMP DoneCheckingPlayerWallCollision
;.SubtractY        
        ;SBC #6
        ;STA PlayerY


DoneCheckingPlayerWallCollision

        ;then special checks for top and bottom of the screen
        IfFlagClear #MISC_FLAGS_TOP_CLOSED,CheckBottomSpecialCollisions
        LDA PlayerY
        CMP #MAX_Y-1
        BMI CheckBottomSpecialCollisions
        ;if it's a closed door and not a lock, go straight to reset
aaa
        LDA TopDoorColor
        CMP RoomFGColor
        BEQ .ResetPosition
        ;otherwise check if player has the right key
        IfPlayerMissingKey
        BEQ .ResetPosition

bbb
        ;at this point, player has the right key. Open the door
        ClearFlag #MISC_FLAGS_TOP_CLOSED
        LDA RoomBGColor
        STA TopDoorColor
        LDY #ROOMDEF_ITEM_AND_LOCK
        LdaRoomDefY
        AND #LOCK_COLOR_MASK
        STA Temp
        UnlockDoor Temp
        PlaySound #SOUND_ENEMY_HURT


        JMP DoneCheckingTopBottomCollisions

.ResetPosition

        LDA PlayerLastX
        STA PlayerX
        LDA PlayerLastY
        STA PlayerY
        jmp DoneCheckingTopBottomCollisions
        
CheckBottomSpecialCollisions 


        IfFlagClear #MISC_FLAGS_BOTTOM_CLOSED,DoneCheckingTopBottomCollisions
        LDA PlayerY
        CMP #KERNEL_BOTTOM_DOOR_CUTOFF + PLAYER_HEIGHT + 2
        BPL DoneCheckingTopBottomCollisions
        ;if it's a closed door and not a lock, go straight to reset
        LDA BottomDoorColor
        CMP RoomFGColor
        BEQ .ResetPosition
        ;otherwise check if player has the right key
        IfPlayerMissingKey
        BEQ .ResetPosition

        ;at this point, player has the right key. Open the door
        ClearFlag #MISC_FLAGS_BOTTOM_CLOSED
        LDA RoomBGColor
        STA BottomDoorColor
        LDY #ROOMDEF_ITEM_AND_LOCK
        LdaRoomDefY
        AND #LOCK_COLOR_MASK
        STA Temp
        UnlockDoor Temp
        PlaySound #SOUND_ENEMY_HURT

        jmp DoneCheckingTopBottomCollisions

DoneCheckingTopBottomCollisions

    ENDM

    MAC StoreAllCollisions      ;52
        StorePlayerCollisions   ;27
        StoreEnemyCollisions {1};21
        STA CXCLR               ;4
    ENDM

    MAC StorePlayerCollisions ;27
    SUBROUTINE
        LDA CXM1P                   ;3
        ORA CXPPMM                  ;3
        AND #COL_FLAG_HURT          ;2

        ORA PlayerCollisionFlags    ;3
        STA PlayerCollisionFlags    ;3

        LDA CXP0FB                  ;3
        LSR                         ;2
        AND #COL_FLAG_WALL          ;2

        ORA PlayerCollisionFlags    ;3
        STA PlayerCollisionFlags    ;3

    ENDM

    MAC StoreEnemyCollisions ;21
    SUBROUTINE
        LDA CXM0P                   ;3
        AND #COL_FLAG_HURT          ;2
        STA EnemyCollisionFlags0+{1};3

        LDA CXP1FB                  ;3
        LSR                         ;2
        AND #COL_FLAG_WALL          ;2

        ORA EnemyCollisionFlags0+{1} ;3
        STA EnemyCollisionFlags0+{1} ;3
    ENDM
    




