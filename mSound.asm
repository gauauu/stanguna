SOUND_PLAYER_HURT equ #(1<<5)
SOUND_SWORD       equ #(2<<5)
SOUND_ENEMY_HURT  equ #(3<<5)
SOUND_GET_ITEM    equ #(4<<5)

SOUND_TIMER_MASK  equ #%00011111
SOUND_EFFECT_MASK equ #%11100000

SOUND_DEFAULT_TIME equ #%00001111
SOUND_HALF_TIME    equ #%00001000

    MAC PlaySound
        LDY {1}
        JSR PlaySoundInY
    ENDM


    MAC PlaySounds
    SUBROUTINE
        LDA Sound0+{1}          ;3
        AND #SOUND_TIMER_MASK   ;2
        BNE .YesPlaySound       ;2/3 ;check timer part of variable
        JMP .WaitThenTurnOffSound ;3
.YesPlaySound                   ;   8 so far
        TAY                     ;2
        DEY                     ;2
        STY Temp                ;3 ;decrement and store it in temp
        LDA Sound0+{1}          ;3 ;(we have to store as we can't or with a register)

                                ;  18 so far
                                
        AND #SOUND_EFFECT_MASK  ;2 ;now grab which sound
        TAX                     ;2 ;store it in X
        ORA Temp                ;3 ;OR the timer
        STA Sound0+{1}          ;3 ;and save it back

        LDY #$FF                ;2 ;set full volume
        STY AUDV0+{1}           ;3

                                ;  31 so far

        TXA                     ;2
        CMP #SOUND_PLAYER_HURT  ;2 
        BNE .TrySound2          ;2/3

        ; player hurt sound:
        ; channel 6, module from around f30 to f5
                                ;  37
        LDX #6                  ;2
        STX AUDC0+{1}           ;3
        LDA #10                 ;2
        SBC Temp                ;3
        STA AUDF0+{1}           ;3
        JMP .DonePlayingSound   ;3      (53)

.TrySound2                      ;38

        CMP #SOUND_SWORD        ;2
        BNE .TrySound3          ;2/3

        ;sword sound:
        ;c8/f4, but cut off after only 4 or so frames
        LDA Temp                ;3
        CMP #%00001100          ;2
        BNE .PlaySound2         ;2/3

        LDA #0                  ;2
        STA Sound0+{1}          ;3
        JMP .TurnOffSound       ;3  ;57 (68 with turnOffSound)

.PlaySound2                     ;   50
        LDX #8                  ;2
        STX AUDC0+{1}           ;3
        LDX #%00000100          ;2
        STX AUDF0+{1}           ;3
        JMP .DonePlayingSound   ;3      (63)


.TrySound3                      ;   43

        CMP #SOUND_ENEMY_HURT   ;2
        BNE .TrySound4          ;2/3

        ;enemy hurt sound:
        ;c14, f30 to f5
        LDX #14                 ;2
        STX AUDC0+{1}           ;3
        LDA #10                 ;2
        SBC Temp                ;3
        STA AUDF0+{1}           ;3
        JMP .DonePlayingSound   ;3      (63)


.TrySound4                      ; 48

        ;CMP #SOUND_GET_ITEM     ;2
        ;BNE .TrySound5          ;2/3

        LDX #6                  ;2
        STX AUDC0+{1}           ;3
        LDA Temp                ;3
        AND #8                  ;2
        ADC #1                  ;2
        STA AUDF0+{1}           ;3
        JMP .DonePlayingSound   ;3      (68)

.TrySound5
        nop
        
.WaitThenTurnOffSound ;10 so far
        ldx #5
.waitLoop
        dex
        bne .waitLoop

.TurnOffSound   
        LDA #0
        STA AUDV0+{1}
        STA AUDC0+{1}
        STA AUDF0+{1}

.DonePlayingSound
    ENDM

    MAC PlayNoteCycledForTesting
    SUBROUTINE
        LDA #%10000000
        BIT INPT4
        BNE .SkipNoteInc
        LDX Sound0
        DEX
        STX Sound0
        BNE .SkipNoteInc
        LDX #15
        STX Sound0
.SkipNoteInc
        LDA #$ff
        STA AUDV0
        LDA Sound0
        STA AUDC0
        LDA PlayerX
        STA AUDF0
    ENDM

    MAC ClearSounds
        LDA #0
        STA AUDV0
        STA AUDC0
        STA AUDF0
        STA AUDV1
        STA AUDC1
        STA AUDF1
    ENDM
