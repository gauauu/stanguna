; Positions an object horizontally
; Inputs: A = Desired position.
; X = Desired object to be positioned (0-5).
; scanlines: If control comes on or before cycle 73 then 1 scanline is consumed.
; If control comes after cycle 73 then 2 scanlines are consumed.
; Outputs: X = unchanged
; A = Fine Adjustment value.
; Y = the "remainder" of the division by 15 minus an additional 15.
; control is returned on cycle 6 of the next scanline.
; from R. Mundschau at http://www.biglist.com/lists/stella/archives/200403/msg00260.html

OBJ_P1 = 0
OBJ_P2 = 1
OBJ_M1 = 2
OBJ_M2 = 3
OBJ_BL = 4




    MAC PositionObjectAtAbsolute
        LDX #{1}
        LDA #{2}
        JSR PosObject
    ENDM

    MAC PositionObjectAtAcc
        LDX #{1}
        JSR PosObject
    ENDM


    MAC PositionObjectWithoutJSR
    SUBROUTINE
            LDX #{1}
            STA WSYNC                ; 00     Sync to start of scanline.
            SEC                      ; 02     Set the carry flag so no borrow will be applied during the division.
.divideby15 SBC #15                  ; 04     Waste the necessary amount of time dividing X-pos by 15!
            BCS .divideby15          ; 06/07  11/16/21/26/31/36/41/46/51/56/61/66
            TAY
            LDA fineAdjustTable,y    ; 13 -> Consume 5 cycles by guaranteeing we cross a page boundary
            STA HMP0,x
            STA RESP0,x              ; 21/ 26/31/36/41/46/51/56/61/66/71 - Set the rough position.
            LDA #0                   ; prepare A to be 0 for displaying enemy on next frame
            STA WSYNC

    ENDM


    ;Using Spiceware's positioning code sample from
    ;http://atariage.com/forums/topic/223653-maintaining-p0-horizontal-position-after-previous-resp0/?p=2958090
    ;Now need to update my check for new room code for new X/Y values
    MAC PosObject2
    SUBROUTINE
        sec             ; 2  
        sta WSYNC       ; X holds object, 0=P0, 1=P1, 2=M0, 3=M1, 4=Ball
.DivideLoop:
        sbc #15         ; 2  
        bcs .DivideLoop  ; 2  4
        eor #7          ; 2  6
        asl             ; 2  8
        asl             ; 2 10
        asl             ; 2 12
        asl             ; 2 14
        sta.wx HMP0,X   ; 5 19
        sta RESP0,X     ; 4 23 <- set object position
    ENDM

