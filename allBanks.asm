;This is code that needs to go at the beginning of each bank 
    
    include fineAdjustTable.asm
    include soundCommon.asm

Start
    StartupZeroMem

    BankJump BeginGame,1


JumpBank_1
JumpBank_BANK_1
    sta BANK_1
    rts

JumpBank_2
JumpBank_BANK_2
    sta BANK_2
    rts

JumpBank_A
JumpBank_BANK_A
    sta BANK_A
    rts

JumpBank_B
JumpBank_BANK_B
    sta BANK_B
    rts


GetRandomNumber
    Randomize
    adc FrameCtr
    adc PlayerX
    adc Enemy0State0
    rts
