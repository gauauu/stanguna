

ShowSubscreen
    jsr SaveGame
    jmp SubscreenLoopAfterVBlank

SubscreenLoop
    jsr     SubscreenVBlank
SubscreenLoopAfterVBlank
    ClearSounds
    lda     #0
    sta     ENABL

    ;update
    ;draw
    jsr     SubscreenUpdate
    jsr     SubscreenDrawScreen
    jsr     SubscreenOverScan
    jmp     SubscreenLoop
    

    BankJump MainKernelPrep,BANK_1





Hex2Bcd ;(good 0-99)
;thanks to Omegamatrix: 
;http://atariage.com/forums/blog/563/entry-12057-hex-to-bcd-conversion-0-99-part-trois/
;22 bytes, 26 cycles
    tay              ;2  @2
    lsr              ;2  @4
    lsr              ;2  @6
    lsr              ;2  @8
    lsr              ;2  @10
    tax              ;2  @12
    tya              ;2  @14
    sed              ;2  @16
    clc              ;2  @18
    adc   #0         ;2  @20
    adc   BcdTab,X   ;4  @24
    cld              ;2  @26
    rts


BcdTab:
    .byte $00,$06,$12,$18,$24,$30,$36


    MAC PrepBCDForDisplay
    
        tay
        ;isolate first digit, multiply by 8
        and #$0f
        clc
        rol
        rol
        rol
        ;add the result # of bytes to the address of gfx 0
        ;and save it to the temp var for displaying digit 1
        clc
        adc #<TDigit0
        sta {1}+2
        lda #>TDigit0
        adc #0
        sta {1}+3

        ;now work on the 2nd digit
        tya
        and #%11110000
        clc
        ror

        ;add the result # of bytes to the address of gfx 0
        ;and save it to the temp var for displaying digit 2 
        clc
        adc #<TDigit0
        sta {1}
        lda #>TDigit0
        adc #0
        sta {1}+1
    ENDM


    MAC SubscreenItemCheck
    SUBROUTINE
    
        tya
        and #(1 << {1})
        beq .notBow
        CopyPointer {2},{3}
.notBow

    ENDM


Prep6Char
    lda     #%011
    sta     NUSIZ0
    sta     NUSIZ1
    sta     VDELP0
    sta     VDELP1


    sta     WSYNC
    SLEEP31
    nop
    nop
    nop
    sta     RESP0
    sta     RESP1

    lda     #$10
    sta     HMP0

    lda     #$20
    sta     HMP1

    sta     WSYNC
    sta     HMOVE
    rts


    echo "------", [SubscreenAlign - *]d, " bytes free before SubscreenAlign"


;---------------------------------------
;  General reusable 6-char kernel chunk
;---------------------------------------
    align 256
SubscreenAlign
SDraw6Char SUBROUTINE ;a should be number o flines
    sty WSYNC
    SLEEP51
    ;wasting more cycles:
    ldy     #00 ;3
    tay         ;2
    tay         ;2
    tay         ;2
    tay         ;2
.loop:
    sty     tmpVar              ; 3

    ;setting color...these are pretty unnecessary, can waste 8 cycles 
    ;doing something more productive if I need to....
    tya                         ;2
    adc     PlayerColorDef      ;3
    lda     PlayerColorDef      ; 3 = 11

    lda     (ptr+$0),y          ; 5
    sta     GRP0                ; 3
    lda     (ptr+$2),y          ; 5
    sta     GRP1                ; 3
    lda     (ptr+$4),y          ; 5
    sta     GRP0                ; 3
    lda     (ptr+$6),y          ; 5
    sta     tmpVar2             ; 3 = 32

    lax     (ptr+$a),y          ; 5
    lda     (ptr+$8),y          ; 5
    ldy     tmpVar2             ; 3
    sty     GRP1                ; 3         @43
    sta     GRP0                ; 3
    stx     GRP1                ; 3
    sta     GRP0                ; 3 = 25

    ldy     tmpVar              ; 3
    dey                         ; 2

    bpl     .loop
    iny
    sty     COLUP0
    sty     COLUP1
    sty     GRP0
    sty     GRP1

    rts


SubscreenVBlank SUBROUTINE
    lda     #2
    sta     WSYNC
    sta     VSYNC
    sta     WSYNC
    sta     WSYNC
    lsr
    ldx     #43
    sta     WSYNC
    sta     VSYNC
    stx     TIM64T
    rts



SubscreenOverScan SUBROUTINE
    lda     #36
    sta     TIM64T

.waitTim:
    lda     INTIM
    bne     .waitTim
    rts
    

SubscreenUpdate SUBROUTINE
    ;position sprites
    sta     WSYNC
    lda     #0
    sta     GRP0
    sta     GRP1
    sta     GRP0
    sta     VDELP0
    sta     VDELP1
    sta     PF0
    sta     PF1
    sta     COLUBK
    sta     REFP0
    sta     REFP1
    sta     RESP0
    lda     #3
    sta     NUSIZ0
    sta     NUSIZ1
    nop
    nop
    sta     RESP1

    lda     #$0F
    sta     PF1

    lda     #$FF
    sta     PF2
    ;lda     #$18

    ;lda     #$E8
    lda     #$0
    sta     COLUPF


    ;set colors for keys
    lda #1
    ldx #7
    ldy #0
.keyLoop
    sta Temp
    bit KeysOwned
    beq .skipColor
    tya
    AccDoorToColor
    jmp .doneColor
.skipColor
    lda #$F6
.doneColor
    sta ptr,x
    lda Temp
    clc
    rol
    iny
    dex
    bne .keyLoop


    lda #SWCHB_COLOR
    and SWCHB
    bne .JumpBackToMain
    rts
.JumpBackToMain
    lda #0
    sta NUSIZ0
    BankJump MainKernelPrep,BANK_1




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; subscreen kernel
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;align 256

SubscreenDrawScreen SUBROUTINE
    ldx     #227
.waitTim:
    lda     INTIM
    bne     .waitTim
    sta     WSYNC
    sta     VBLANK              ; 3
    stx     TIM64T              ; 4


    ;WaitSync 8
    WaitSync 4
    nop
    nop
    lda     #0
    sta     COLUP0
    sta     COLUP1

    ; do this here, we don't have space to do it elsewhere?
    ; needs to be 8 clock cycles, and we want to
    ; set our BG to a normal setting in case of dark rooms
    lda #BG_REFLECT|NUSIZ_MISSILE_4
    sta CTRLPF
    sta CTRLPF

    sta     RESP0
    lda     #5
    sta     NUSIZ0
    sta     NUSIZ1
    lda     #$FF
    sta     GRP0
    sta     GRP1
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    sta     RESP1
    sta     WSYNC
    lda     #$20
    sta     COLUP0
    sta     COLUP1
    
    lda     #$F2
    sta     COLUPF
    sta     WSYNC
    sta     WSYNC

    lda     #$F4
    sta     COLUPF
    lda     #$22
    sta     COLUP0
    sta     COLUP1
    sta     WSYNC
    sta     WSYNC

    lda     #$F6
    sta     COLUPF
    lda     #$24
    sta     COLUP0
    sta     COLUP1
    sta     WSYNC
    sta     WSYNC

    lda     #$E8
    ;sta     COLUPF
    lda     #$22
    sta     COLUP0
    sta     COLUP1
    sta     WSYNC
    sta     WSYNC

    ;lda     #$EA
    lda     #$22
    sta     COLUP0
    sta     COLUP1
    sta     WSYNC
    sta     WSYNC
       
    lda     #3
    sta     NUSIZ0
    sta     NUSIZ1
    lda     #0
    sta     GRP0
    sta     GRP1

    sta     WSYNC
    lda     #0
    sta     GRP0
    sta     GRP1
    sta     GRP0
    sta     VDELP0
    sta     VDELP1
    nop
    nop
    ;sta     PF0
    ;sta     PF1
    sta     COLUBK
    sta     REFP0
    sta     REFP1
    sta     RESP0
    lda     #3
    sta     NUSIZ0
    sta     NUSIZ1
    nop
    nop
    sta     RESP1

    ;;;;;;;;;;;;;;;;;;;;
    ; Keys
    ;;;;;;;;;;;;;;;;;;;;

    lda #<SubKeyIcon 
    sta ptr
    lda #>SubKeyIcon
    sta ptr+1

    ldy #8
.loop
    sta WSYNC
    lda (ptr),y
    sta GRP0
    sta GRP1
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    lda ptr+2
    sta COLUP0
    lda ptr+3
    sta COLUP0
    lda ptr+4
    sta COLUP0
    lda ptr+5
    sta COLUP1
    lda ptr+6
    sta COLUP1
    lda ptr+7
    sta COLUP1

    dey
    bne .loop
    lda #0
    sta GRP0
    sta GRP1
    sta GRP0

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Reposition for 6 char display
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jsr Prep6Char
 


    ;;;;;;;;;;;;;;;;;;;;;;;;
    ; Health
    ;;;;;;;;;;;;;;;;;;;;;;;;

    ; prep graphics
    lda #$0E
    sta COLUP0
    sta COLUP1

    CopyPointer THpIcon,ptr1
    CopyPointer TDigitSlash,ptr4

    lda PlayerHP
    jsr Hex2Bcd

    PrepBcdForDisplay ptr2

    lda PlayerMaxHP
    jsr Hex2Bcd

    PrepBCDForDisplay ptr5

    sta WSYNC

    ;now draw it
    lda #6
    jsr SDraw6Char

    ;;;;;;;;;;;;;;;;;;;;
    ; Attack/defense
    ;;;;;;;;;;;;;;;;;;;;
    CopyPointer TPowerIcon,ptr1
    CopyPointer TArmorIcon,ptr4
    CopyPointer SubBlank,ptr3
    CopyPointer SubBlank,ptr6


    lda PlayerPower
    clc
    rol
    rol
    rol
    ;add the result # of bytes to the address of gfx 0
    ;and save it to the temp var for displaying digit 1
    clc
    adc #<TDigit0
    sta ptr2
    lda #>TDigit0
    adc #0
    sta ptr2+1

    lda PlayerArmor
    clc
    rol
    rol
    rol
    ;add the result # of bytes to the address of gfx 0
    ;and save it to the temp var for displaying digit 1
    clc
    adc #<TDigit0
    sta ptr5
    lda #>TDigit0
    adc #0
    sta ptr5+1

    lda #$0E
    sta COLUP0
    sta COLUP1

    lda #8
    jsr SDraw6Char

    ;;;;;;;;;;;;;;;;;;;;
    ; Bow/arrow
    ;;;;;;;;;;;;;;;;;;;;
    ;CopyPointer SubBlank,ptr1
    CopyPointer TArrowIcon,ptr1
    lda NumArrows
    PrepBCDForDisplay ptr2

    CopyPointer ExpText,ptr4
    lda Experience
    clc
    ror
    jsr Hex2Bcd
    PrepBCDForDisplay ptr5

    lda #$0E
    sta COLUP0
    sta COLUP1

    lda #8
    jsr SDraw6Char

    ;;;;;;;;;;;;;;;;;;;;
    ; Inventory
    ;;;;;;;;;;;;;;;;;;;;
    CopyPointer SubBlank,ptr1
    CopyPointer SubBlank,ptr2
    CopyPointer SubBlank,ptr3
    CopyPointer SubBlank,ptr4
    CopyPointer SubBlank,ptr5
    CopyPointer SubBlank,ptr6
    lda Inventory
    tay

    SubscreenItemCheck ITEM_BOW_ARROWS,TBowArrows,ptr1
    SubscreenItemCheck ITEM_DYNAMITE,TDynamite,ptr2
    SubscreenItemCheck ITEM_LANTERN,TLantern,ptr3
    SubscreenItemCheck ITEM_RING,TRing,ptr4
    SubscreenItemCheck ITEM_BOOTS,TBoots,ptr5

    lda #$0E
    sta COLUP0
    sta COLUP1
    lda #8
    jsr SDraw6Char






    ldx #3
.beforeMap
    sta WSYNC
    dex
    bne .beforeMap

    lda #0
    sta VDELP0
    sta VDELP1
    sta NUSIZ0
    sta NUSIZ1

    lda MapPosition
    and #$0F
    adc #<MapDotRange
    sta ptr
    lda #0
    adc #>MapDotRange
    sta ptr+1

    ldy #14

    ;timings to get lake/river in right place
    nop
    lda MapPosition,X
    sta RESP1
    sty COLUP0
    lda #$80
    sta COLUP1
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop

    lda MapPosition
    ;lda #$81
    and #$F0
    clc
    ror
    ror
    adc #60
    PositionObjectWithoutJSR 0
    sta HMOVE

;;;;;;;;;;;;;;;;
; Top dungeons
;;;;;;;;;;;;;;
    ldx #55
.innerLoop1
    lda #%11111001
    sta PF2
    sta WSYNC
    txa ;2
    clc ;2
    ror ;2
    clc ;2
    ror ;2
    tay ;2
    lda (ptr),Y ;5
    sta GRP0    ;3 (20)
    lda (ptr),Y ; burn the right # of clocks
    lda (ptr),Y ; burn the right # of clocks
    lda (ptr),Y ; burn the right # of clocks

    lda #32     ;2
    sta COLUBK  ;3
    ;nop
    ldy #%11000001
    ;lda Inventory ;burn cycles
    lda #$60
    sta COLUBK
    sty PF2
    ;lda #$80
    ;sta COLUP1
    nop
    nop
    dex
    lda #0
    sta COLUBK
    txa
    cmp #35
    bne .innerLoop1

;outdoor strip and lake
.mapTopLoop2
    sta WSYNC
    lda #%00000001
    sta PF2
    txa ;2
    clc ;2
    ror ;2
    clc ;2
    ror ;2
    tay ;2
    lda (ptr),Y ;5
    sta GRP0    ;3 (20)
    lda (ptr),Y ; burn the right # of clocks

    lda #ROOM_COLOR_GREEN
    sta COLUBK
    lda #$FF
    sta GRP1
    nop
    nop
    nop
    nop
    nop
    ;dex
    nop
    dex
    nop
    lda #0
    sta COLUBK
    txa
    cmp #27
    bne .mapTopLoop2


;top of d1
;outdoor strip and lake
.mapTopLoop3
    sta WSYNC
    ;lda #%11100001
    txa ;2
    clc ;2
    ror ;2
    clc ;2
    ror ;2
    tay ;2
    lda (ptr),Y ;5
    sta GRP0    ;3 (20)
    lda #%00110000
    sta GRP1
    sta GRP1
    nop
    nop
    ldy #ROOM_COLOR_GRAY
    lda #ROOM_COLOR_GREEN
    sty COLUBK
    ldy #ROOM_COLOR_BROWN
    nop
    sta COLUBK
    dex
    sty COLUBK
    lda #ROOM_COLOR_GREEN
    ldy #0
    sta COLUBK
    sty COLUBK
    txa
    cmp #19
    bne .mapTopLoop3


;middle of d1
.mapTopLoop4
    sta WSYNC
    ;lda #%11100001
    txa ;2
    clc ;2
    ror ;2
    clc ;2
    ror ;2
    tay ;2
    lda (ptr),Y ;5
    sta GRP0    ;3 (20)
    lda #%00110000
    sta GRP1
    sta GRP1
    nop
    nop
    ;ldy #ROOM_COLOR_BROWN
    ldy #ROOM_COLOR_TRUNK_B
    lda #ROOM_COLOR_GREEN
    sty COLUBK
    ldy #ROOM_COLOR_BROWN
    nop
    sta COLUBK
    dex
    sty COLUBK
    lda #ROOM_COLOR_GREEN
    ldy #0
    sta COLUBK
    sty COLUBK
    txa
    cmp #11
    bne .mapTopLoop4

    
;bottom
.mapTopLoop5
    sta WSYNC
    ;lda #%11100001
    txa ;2
    clc ;2
    ror ;2
    clc ;2
    ror ;2
    tay ;2
    lda (ptr),Y ;5
    sta GRP0    ;3 (20)
    lda #%00110000
    sta GRP1
    sta GRP1
    nop
    nop
    ;ldy #ROOM_COLOR_BROWN
    ldy #ROOM_COLOR_TRUNK_B
    lda #ROOM_COLOR_GREEN
    sty COLUBK
    ;ldy #ROOM_COLOR_BROWN
    nop
    nop
    sta COLUBK
    nop
    sta COLUBK
    lda #ROOM_COLOR_GREEN
    ldy #0
    sta COLUBK
    sty COLUBK
    dex
    bne .mapTopLoop5

    lda #0
    sta GRP0
    sta GRP1
    sta GRP0

    lda #$FF
    sta PF2

    jsr Prep6Char
    lda #$0E
    sta COLUP0
    sta COLUP1
    BankJump SubscreenShowPassword,BANK_B
AfterShowPassword
    lda #0
    sta GRP0
    sta GRP1 


    ldx #2
.waitScreen:
    lda     INTIM
    bne     .waitScreen
    sta     VSYNC
    sta     WSYNC
    stx     VBLANK
    sta     WSYNC
    rts


   

