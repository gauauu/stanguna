ExperienceTable
    .byte 8
    .byte 10
    .byte 12
    .byte 14
    .byte 16
    .byte 18
    .byte 22
    .byte 26
    .byte 30
    .byte 35
    .byte 40

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Using Y here because X is used in the calling
; scope. Otherwise I need to save it to a temp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


CheckForLevelUp SUBROUTINE
    jsr GetCurrentLevel
    tya
    ; if level 10, we never get level ups
    eor #10
    beq .resetExpAndReturn
    iny
    tya
    clc
    rol
    rol
    rol
    rol
    ; now A has exp required for levelup
    clc
    cmp Experience
    bcs ExpRts

    ; now level up (x already incremented above)
    lda ExperienceTable,y
    sta PlayerMaxHP
    sta PlayerHP

    lda #0
    sta Experience

    PlaySound #SOUND_GET_ITEM

.resetExpAndReturn
    lda #0
    sta Experience
ExpRts
    rts

GetCurrentLevel SUBROUTINE
    ldy #0
.loop
    lda PlayerMaxHP
    eor ExperienceTable,y
    beq ExpRts
    iny
    jmp .loop

