CHUNK_BLANK 			equ 1
CHUNK_START_A 			equ 2
CHUNK_START_B 			equ 3
CHUNK_START_C 			equ 4
CHUNK_DOORS_A 			equ 5
CHUNK_TOP_BOTTOM_B		equ 6
CHUNK_TOP_BOTTOM_C		equ 7
CHUNK_PILLARS_B			equ 8
CHUNK_PILLARS_C			equ 9
CHUNK_FUNNEL_A			equ 10
CHUNK_FUNNEL_B			equ 11
CHUNK_FUNNEL_C			equ 12
CHUNK_WINGS_B			equ 13
CHUNK_WINGS_C			equ 14
CHUNK_UFO_B		    	equ 15
CHUNK_UFO_C		    	equ 16
CHUNK_PLUS_A			equ 17
CHUNK_PLUS_B			equ 18
CHUNK_PLUS_C			equ 19
CHUNK_BIGV_B			equ 20
CHUNK_BIGV_C			equ 21
CHUNK_CLIFF_A			equ 22
CHUNK_CLIFF_B_WITH_TREE	equ 23
CHUNK_CLIFF_C_WITH_CAVE	equ 24
CHUNK_CLIFF_B			equ 25
CHUNK_CLIFF_C			equ 26
CHUNK_CLIFF_B_ROUNDED	equ 27
CHUNK_CLIFF_B_WITH_ROCK	equ 28
CHUNK_PINES_B			equ 29
CHUNK_TREES_B			equ 30
CHUNK_TREES_C			equ 31
CHUNK_RIVER_C			equ 32
CHUNK_LAKE_B			equ 33
CHUNK_LAKE_C			equ 34
