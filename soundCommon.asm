
PlaySoundInY    SUBROUTINE
        LDA #SOUND_TIMER_MASK
        BIT Sound0
        BNE .PlaySound1

        TYA 
        ORA #SOUND_DEFAULT_TIME
        STA Sound0
        RTS

        ;TODO: maybe turn this into a binary search
        ; or a jump table?

.PlaySound1
        TYA
        ORA #SOUND_DEFAULT_TIME
        STA Sound1
        RTS
