; TOP_OPEN, TOP_CLOSED, TOP_KEY, TOP_ENEMY
; BOTTOM_OPEN, BOTTOM_CLOSED, BOTTOM_KEY, BOTTOM_ENEMY
; LEFT_CLOSED, LEFT_OPEN, LEFT_ENEMY, LEFT_FAKE
; RIGHT_CLOSED, RIGHT_OPEN, RIGHT_ENEMY, RIGHT_FAKE
; 
; Open - 00
; Closed - 11
; Enemy - 10
; Other - 01

ROOM_STARTING               equ 0
ROOM_FOUR_DOORS             equ 1
ROOM_PILLARS                equ 2
ROOM_FUNNEL_TOP             equ 3
ROOM_WINGS                  equ 4
ROOM_UFO                    equ 5
ROOM_PLUS                   equ 6
ROOM_BIGV                   equ 7
ROOM_OUTDOOR_CLIFFTOP_CAVE  equ 8
ROOM_OUTDOOR_CLIFFTOP       equ 9
ROOM_OUTDOOR_CLIFF2         equ 10
ROOM_PINES                  equ 11
ROOM_TREES                  equ 12
ROOM_RIVER                  equ 13
ROOM_BLANK                  equ 14
ROOM_LAKE                   equ 15
ROOM_RIVER_BRIDGE           equ 16
ROOM_OUTDOOR_CLIFF3         equ 17
ROOM_OUTDOOR_BOULDER        equ 18
ROOM_RIVER_WIDE             equ 19
ROOM_BIG_TREE               equ 20
ROOM_BACK_FORTH             equ 21
ROOM_RIVER_CURVE            equ 22
ROOM_RIVER_MERGE            equ 23
ROOM_RIVER_START            equ 24
ROOM_CASTLE                 equ 25
ROOM_HOURGLASS              equ 26
ROOM_MAZE                   equ 27
ROOM_ROUND                  equ 28
ROOM_GOOFY                  equ 29
ROOM_CHAMBERS               equ 30
ROOM_OVAL                   equ 31

CHUNK_BLANK 			equ 0
CHUNK_START_A 			equ 1
CHUNK_START_B 			equ 2
CHUNK_START_C 			equ 3
CHUNK_DOORS_A 			equ 4
CHUNK_TOP_BOTTOM_B		equ 5
CHUNK_TOP_BOTTOM_C		equ 6
CHUNK_PILLARS_B			equ 7
CHUNK_PILLARS_C			equ 8
CHUNK_FUNNEL_A			equ 9
CHUNK_FUNNEL_B			equ 10
CHUNK_FUNNEL_C			equ 11
CHUNK_WINGS_B			equ 12
CHUNK_WINGS_C			equ 13
CHUNK_UFO_B		    	equ 14
CHUNK_UFO_C		    	equ 15
CHUNK_PLUS_A			equ 16
CHUNK_PLUS_B			equ 17
CHUNK_PLUS_C			equ 18
CHUNK_BIGV_B			equ 19
CHUNK_BIGV_C			equ 20
CHUNK_CLIFF_A			equ 21
CHUNK_CLIFF_B_WITH_TREE	equ 22
CHUNK_CLIFF_C_WITH_CAVE	equ 23
CHUNK_CLIFF_B			equ 24
CHUNK_CLIFF_C			equ 25
CHUNK_CLIFF_B_ROUNDED	equ 26
CHUNK_CLIFF_C_WITH_ROCK	equ 27
CHUNK_PINES_B			equ 28
CHUNK_TREES_B			equ 29
CHUNK_TREES_C			equ 30
CHUNK_RIVER_C			equ 31
CHUNK_LAKE_B			equ 32
CHUNK_LAKE_C			equ 33
CHUNK_RIVER_BRIDGE_C	equ 34
CHUNK_BOULDER_C	        equ 35
CHUNK_RIVER_WIDE_C      equ 36
CHUNK_BIG_TREE_C        equ 37
CHUNK_BACK_FORTH_B      equ 38
CHUNK_BACK_FORTH_C      equ 39
CHUNK_RIVER_CURVE_A     equ 40
CHUNK_RIVER_CURVE_B     equ 41
CHUNK_RIVER_CURVE_C     equ 42
CHUNK_RIVER_MERGE_B     equ 43
CHUNK_RIVER_MERGE_C     equ 44
CHUNK_RIVER_START_B     equ 45
CHUNK_RIVER_START_C     equ 46
CHUNK_CASTLE_B          equ 47
CHUNK_CASTLE_C          equ 48
CHUNK_HOURGLASS_A       equ 49
CHUNK_HOURGLASS_B       equ 50
CHUNK_HOURGLASS_C       equ 51
CHUNK_MAZE_A            equ 52
CHUNK_MAZE_B            equ 53
CHUNK_MAZE_C            equ 54
CHUNK_ROUND_A           equ 55
CHUNK_ROUND_B           equ 56
CHUNK_ROUND_C           equ 57



YFOURTH         equ 25
YTHIRD          equ 33

XFOURTH         equ 40
XTHIRD          equ 50

;progress item flags:
; 0 -- hp up in first green toady room (RoomDef23)
; 1 -- power up in 1st dungeon


SpawnsX 
    .byte XFOURTH-1
    .byte XFOURTH*3
    .byte XFOURTH*3
    .byte XFOURTH
    .byte XFOURTH*2
    .byte XTHIRD*2
    .byte XFOURTH*2

    .byte XFOURTH
    .byte XFOURTH*3
    .byte XFOURTH*3
    .byte XFOURTH
    .byte XFOURTH*2
    .byte XTHIRD*2
    .byte XFOURTH*2

    .byte XFOURTH
    .byte XFOURTH*3
    .byte XFOURTH*3
    .byte XFOURTH
    .byte XFOURTH*2
    .byte XTHIRD*2
    .byte XFOURTH*2
    ;.byte #30
    ;.byte #60
    ;.byte #90
    ;.byte #30
    ;.byte #60
    ;.byte #90
    ;.byte #60
    ;.byte #90

SpawnsY
    .byte YFOURTH*3+2
    .byte YFOURTH*3
    .byte YFOURTH
    .byte YFOURTH
    .byte YTHIRD*2
    .byte YFOURTH*2
    .byte YTHIRD
    .byte YFOURTH*2

    .byte YFOURTH*3
    .byte YFOURTH*3
    .byte YFOURTH
    .byte YFOURTH
    .byte YTHIRD*2
    .byte YFOURTH*2
    .byte YTHIRD
    .byte YFOURTH*2

    .byte YFOURTH*3
    .byte YFOURTH*3
    .byte YFOURTH
    .byte YFOURTH
    .byte YTHIRD*2
    .byte YFOURTH*2
    .byte YTHIRD
    .byte YFOURTH*2
    ;.byte #15
    ;.byte #35
    ;.byte #50
    ;.byte #70
    ;.byte #15
    ;.byte #35
    ;.byte #50
    ;.byte #70

SafeSpawns
    .byte #%11100001 ;TestRoom
    .byte #%11111111 ;4 doors
    .byte #%01110000 ;Pillars
    .byte #%01110100 ;FunnelTop
    .byte #%11110101 ;WingsOK
    .byte #%11100001 ;UFO
    .byte #%00011110 ;Plus
    .byte #%11001000 ;BigV
    .byte #%00101100 ;OutdoorCliffTopCave
    .byte #%00101110 ;OutdoorCliffTop
    .byte #%01100000 ;OutdoorCliffTop2
    .byte #%10011011 ;OutdoorPines
    .byte #%10000101 ;OutdoorTrees
    .byte #%11100001 ;OutdoorRiver
    .byte #%11111111 ;Blank
    .byte #%00010000 ;Lake
    .byte #%11100001 ;River-Bridge
    .byte #%00101110 ;OutdoorClifftop3
    .byte #%11100001 ;OutdoorBoulder
    .byte #%11100001 ;River-wide
    .byte #%11100001 ;Big Tree
    .byte #%00001000 ;BackForth
    .byte #%01100100 ;RiverCurve
    .byte #%01100000 ;RiverMerge
    .byte #%00000100 ;RiverStart
    .byte #%00000100 ;Castle
    .byte #%00010100 ;hourglass
    .byte #%11100001 ;maze
    .byte #%00010100 ;round

    .byte #%00101000 ;goofy
    .byte #%01111110 ;chambers
    .byte #%00011110 ;oval

Respawns
    .byte #$A7 ;starting room
    .byte #$8B ;outside
    .byte #$55 ;d4 entrance
    .byte #$A5 ;d2 entrance
    .byte #$07 ;d3 entrance
    .byte #$05 ;d5 entrance

ColorDefStart
ColorThemeD1
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #<ColorDungeon1
    .byte #>ColorDungeon1
ColorThemeOutdoorRocks1
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_TAN
    .byte #<ColorOutdoorRocks1
    .byte #>ColorOutdoorRocks1
ColorThemeOutdoorRocks2
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_TAN
    .byte #<ColorOutdoorRocks2
    .byte #>ColorOutdoorRocks2
ColorThemeTrees1
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #<ColorTrees1
    .byte #>ColorTrees1
ColorThemeTrees2
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #<ColorTrees2
    .byte #>ColorTrees2
ColorThemeRiver
    .byte #ROOM_COLOR_GREEN
    .byte #$80
    .byte #<ColorRiver
    .byte #>ColorRiver
ColorThemeDarkCave
    .byte #ROOM_COLOR_GRAY
    .byte #0
    .byte #<Zeros
    .byte #>Zeros
ColorThemeBigTree
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #<ColorBigTree
    .byte #>ColorBigTree
ColorThemeCliffRiverStart
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_TAN
    .byte #<ColorRiverStart
    .byte #>ColorRiverStart
ColorThemeCastle
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_GRAY
    .byte #<ColorDungeon1
    .byte #>ColorDungeon1
ColorThemeDesert
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_TAN
    .byte #<ColorOutdoorRocks2
    .byte #>ColorOutdoorRocks2
ColorThemeD2
    .byte #$60
    .byte #ROOM_COLOR_GRAY
    .byte #<ColorDungeon1
    .byte #>ColorDungeon1
ColorThemeD3
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_GRAY
    .byte #<ColorDungeon1
    .byte #>ColorDungeon1
ColorThemeD4
    .byte #ROOM_COLOR_BROWN+2
    .byte #ROOM_COLOR_GRAY
    .byte #<ColorDungeon1
    .byte #>ColorDungeon1
ColorThemeD5
    .byte #$60
    .byte #ROOM_COLOR_GRAY
    .byte #<ColorDungeon1
    .byte #>ColorDungeon1
ColorThemeWasteland
    .byte #ROOM_COLOR_GRAY
    .byte #$F4
    .byte #<ColorWasteland
    .byte #>ColorWasteland
ColorThemeLava
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_LAVA
    .byte #<ColorLavaStart
    .byte #>ColorLavaStart

RoomLayoutSections
;start
    .byte #CHUNK_START_A
    .byte #CHUNK_START_B
    .byte #CHUNK_START_C
;four_doors
    .byte #CHUNK_DOORS_A
    .byte #CHUNK_TOP_BOTTOM_B
    .byte #CHUNK_TOP_BOTTOM_C
;pillars
    .byte #CHUNK_DOORS_A
    .byte #CHUNK_PILLARS_B
    .byte #CHUNK_PILLARS_C
;funnel
    .byte #CHUNK_FUNNEL_A
    .byte #CHUNK_FUNNEL_B
    .byte #CHUNK_FUNNEL_C
;wings
    .byte #CHUNK_DOORS_A
    .byte #CHUNK_WINGS_B
    .byte #CHUNK_WINGS_C
;ufo
    .byte #CHUNK_DOORS_A
    .byte #CHUNK_UFO_B
    .byte #CHUNK_UFO_C
;plus
    .byte #CHUNK_PLUS_A
    .byte #CHUNK_PLUS_B
    .byte #CHUNK_PLUS_C
;bigV
    .byte #CHUNK_DOORS_A
    .byte #CHUNK_BIGV_B
    .byte #CHUNK_BIGV_C
;outdoor-clifftop-cave
    .byte #CHUNK_CLIFF_A
    .byte #CHUNK_CLIFF_B_WITH_TREE
    .byte #CHUNK_CLIFF_C_WITH_CAVE
;outdoor-clifftop
    .byte #CHUNK_CLIFF_A
    .byte #CHUNK_CLIFF_B
    .byte #CHUNK_CLIFF_C
;outdoor-cliff2
    .byte #CHUNK_CLIFF_A
    .byte #CHUNK_CLIFF_B_ROUNDED
    .byte #CHUNK_CLIFF_C_WITH_ROCK
;pines
    .byte #CHUNK_BLANK
    .byte #CHUNK_PINES_B
    .byte #CHUNK_BLANK
;trees
    .byte #CHUNK_BLANK
    .byte #CHUNK_TREES_B
    .byte #CHUNK_TREES_C
;river
    .byte #CHUNK_BLANK
    .byte #CHUNK_BLANK
    .byte #CHUNK_RIVER_C
;blank
    .byte #CHUNK_BLANK
    .byte #CHUNK_BLANK
    .byte #CHUNK_BLANK
;lake
    .byte #CHUNK_BLANK
    .byte #CHUNK_LAKE_B
    .byte #CHUNK_LAKE_C
;river-bridge
    .byte #CHUNK_BLANK
    .byte #CHUNK_BLANK
    .byte #CHUNK_RIVER_BRIDGE_C
;clifftop3
    .byte #CHUNK_CLIFF_A
    .byte #CHUNK_CLIFF_B_WITH_TREE
    .byte #CHUNK_CLIFF_C
;boulder
    .byte #CHUNK_BLANK
    .byte #CHUNK_BLANK
    .byte #CHUNK_BOULDER_C
;river-wide
    .byte #CHUNK_BLANK
    .byte #CHUNK_BLANK
    .byte #CHUNK_RIVER_WIDE_C
;big-tree
    .byte #CHUNK_BLANK
    .byte #CHUNK_BLANK
    .byte #CHUNK_BIG_TREE_C
;back-forth
    .byte #CHUNK_DOORS_A
    .byte #CHUNK_BACK_FORTH_B
    .byte #CHUNK_BACK_FORTH_C
;river-curve
    .byte #CHUNK_RIVER_CURVE_A
    .byte #CHUNK_RIVER_CURVE_B
    .byte #CHUNK_RIVER_CURVE_C
;river-merge
    .byte #CHUNK_RIVER_CURVE_A
    .byte #CHUNK_RIVER_MERGE_B
    .byte #CHUNK_RIVER_MERGE_C
;river-start
    .byte #CHUNK_CLIFF_A
    .byte #CHUNK_RIVER_START_B
    .byte #CHUNK_RIVER_START_C
;castle
    .byte #CHUNK_CLIFF_A
    .byte #CHUNK_CASTLE_B
    .byte #CHUNK_CASTLE_C
;hourglass
    .byte #CHUNK_HOURGLASS_A
    .byte #CHUNK_HOURGLASS_B
    .byte #CHUNK_HOURGLASS_C
;maze
    .byte #CHUNK_MAZE_A
    .byte #CHUNK_MAZE_B
    .byte #CHUNK_MAZE_C
;round
    .byte #CHUNK_ROUND_A
    .byte #CHUNK_ROUND_B
    .byte #CHUNK_ROUND_C
;goofy
    .byte #CHUNK_ROUND_A
    .byte #CHUNK_MAZE_B
    .byte #CHUNK_BACK_FORTH_C
;chambers
    .byte #CHUNK_FUNNEL_A
    .byte #CHUNK_TOP_BOTTOM_B
    .byte #CHUNK_PLUS_C
;oval
    .byte #CHUNK_ROUND_A
    .byte #CHUNK_ROUND_B
    .byte #CHUNK_TOP_BOTTOM_C



    MAC COMMENTED
RoomDef00
    .byte #ROOM_PLUS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|TOP_CLOSED
    .byte #<EnemyDefSlime
    .byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE
RoomDef01
    .byte #ROOM_FOUR_DOORS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|TOP_ENEMY
    .byte #<EnemyDefSlime
    .byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE
RoomDef02
    .byte #ROOM_BIGV
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #<EnemyDefGreenSlime
    .byte #>EnemyDefGreenSlime
    .byte #2
    .byte #ROOM_ITEM_NONE
RoomDef03
    .byte #ROOM_FOUR_DOORS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_ENEMY
    .byte #<EnemyDefCrocky
    .byte #>EnemyDefCrocky
    .byte #1
    .byte #ROOM_ITEM_NONE
RoomDef10
    .byte #ROOM_PLUS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #TOP_CLOSED|RIGHT_CLOSED
    .byte #<EnemyDefSlime
    .byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_POWER_UP << 4|1
RoomDef11
    .byte #ROOM_FOUR_DOORS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_ENEMY|TOP_CLOSED|RIGHT_CLOSED
    .byte #<EnemyDefOrangeSlime
    .byte #>EnemyDefOrangeSlime
    .byte #2
    .byte #ROOM_ITEM_NONE;
RoomDef12
    .byte #ROOM_FUNNEL_TOP
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #<EnemyDefSlime
    .byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE;|2
RoomDef13
    .byte #ROOM_FOUR_DOORS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|BOTTOM_CLOSED
    .byte #<EnemyDefBat
    .byte #>EnemyDefBat
    .byte #2
    .byte #ROOM_ITEM_NONE;
StartRoom
RoomDef20
    .byte #ROOM_STARTING
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #BOTTOM_ENEMY 
    .byte #<EnemyDefSlime
    .byte #>EnemyDefSlime
    .byte #2
    ;.byte #0
    .byte #ROOM_ITEM_NONE
    ;.byte ##ITEM_HP_UP<<4|0
    ;.byte #ITEM_BOW_ARROWS<<4

RoomDef21
    .byte #ROOM_FOUR_DOORS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|BOTTOM_ENEMY
    .byte #<EnemyDefSlime
    .byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE;|2
;RoomDef21
    ;.byte #ROOM_FOUR_DOORS
    ;.byte #ROOM_COLOR_BROWN
    ;.byte #ROOM_COLOR_GRAY
    ;.byte #LEFT_CLOSED|BOTTOM_ENEMY|RIGHT_CLOSED
    ;.byte #<EnemyDefCrocky
    ;.byte #>EnemyDefCrocky
    ;.byte #1
    ;.byte #ROOM_ITEM_NONE;|2

RoomDef22
    .byte #ROOM_UFO
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_KEY
    .byte #<EnemyDefOrangeSlime
    .byte #>EnemyDefOrangeSlime
    .byte #2
    .byte #ROOM_ITEM_NONE|2
RoomDef23
    .byte #ROOM_PILLARS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #BOTTOM_CLOSED
    .byte #<EnemyDefGreenToady
    .byte #>EnemyDefGreenToady
    .byte #1
    .byte #ITEM_HP_UP<<4|0 ;progress flag 0

RoomDef30
    .byte #ROOM_PLUS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #BOTTOM_CLOSED|TOP_CLOSED|LEFT_CLOSED
    .byte #<EnemyDefSlime
    .byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_BOW_ARROWS<<4

RoomDef31
    .byte #ROOM_FOUR_DOORS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #RIGHT_CLOSED|TOP_CLOSED|BOTTOM_ENEMY
    .byte #<EnemyDefOrangeSlime
    .byte #>EnemyDefOrangeSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

RoomDef32
    .byte #ROOM_PILLARS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #<EnemyDefGreenSlime
    .byte #>EnemyDefGreenSlime
    .byte #3
    .byte #ITEM_KEY<<4|2
RoomDef33
    .byte #ROOM_PLUS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #<EnemyDefBat
    .byte #>EnemyDefBat
    .byte #2
    .byte #ROOM_ITEM_NONE
RomomDef40
    .byte #ROOM_PILLARS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #TOP_CLOSED|LEFT_ENEMY|RIGHT_CLOSED
    .byte #<EnemyDefGreenToady
    .byte #>EnemyDefGreenToady
    .byte #1
    .byte #ROOM_ITEM_NONE
RomomDef41
    .byte #ROOM_FUNNEL_TOP
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #<EnemyDefBat
    .byte #>EnemyDefBat
    .byte #3
    .byte #ROOM_ITEM_NONE
RomomDef42
    .byte #ROOM_BIGV
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED|RIGHT_CLOSED|TOP_ENEMY
    .byte #<EnemyDefBat
    .byte #>EnemyDefBat
    .byte #3
    .byte #ROOM_ITEM_NONE
RomomDef43
    .byte #ROOM_WINGS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #<EnemyDefGreenSlime
    .byte #>EnemyDefGreenSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

Overworld00
    .byte #ROOM_FOUR_DOORS
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_BROWN
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #<EnemyDefGreenSlime
    .byte #>EnemyDefGreenSlime
    .byte #0
    .byte #ROOM_ITEM_NONE


ExampleRoomHasItem
    .byte #ROOM_PILLARS
    .byte #ROOM_COLOR_BROWN
    .byte #ROOM_COLOR_GRAY
    .byte #LEFT_CLOSED
    .byte #<EnemyDefGreenToady
    .byte #>EnemyDefGreenToady
    .byte #1
    .byte #ITEM_HP_UP<<4

ExampleRoomHasKey
    .byte #ROOM_FUNNEL_TOP
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_BROWN
    .byte #RIGHT_CLOSED
    .byte #<EnemyDefGreenSlime
    .byte #>EnemyDefGreenSlime
    .byte #1
    .byte #ITEM_KEY<<4|2

ExampleRoomHasLock
    .byte #ROOM_WINGS
    .byte #ROOM_COLOR_GREEN
    .byte #ROOM_COLOR_TAN
    .byte #RIGHT_CLOSED|TOP_KEY
    .byte #<EnemyDefOrangeSlime
    .byte #>EnemyDefOrangeSlime
    .byte #2
    .byte #ROOM_ITEM_NONE|2


    ENDM
