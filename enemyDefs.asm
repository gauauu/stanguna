
    MAC EndEnemyFunc
        jmp JumpBank_1
    ENDM

ENEMY_COLOR_REDBUG   equ $32
ENEMY_COLOR_FLYING   equ $0E
ENEMY_COLOR_FADED    equ $7D
ENEMY_COLOR_BOSSBAT  equ $1E
ENEMY_COLOR_MAINBOSS equ $2C


EnemyDefSlime
    .byte #<SlimeFrame0             ;animations
    .byte #>SlimeFrame0             
    .byte #<SlimeFrame1             ;animations
    .byte #>SlimeFrame1             
    .byte #8                        ;sprite height
    .byte #$86                      ;color
    .byte #<EnemySlimeUpdate         ;update code
    .byte #>EnemySlimeUpdate
    .byte #1                        ;hp
    .byte #1                        ;damage
    .byte #ENEMY_SIZE_NORMAL


EnemyDefGreenSlime
    .byte #<SlimeFrame0             ;animations
    .byte #>SlimeFrame0             
    .byte #<SlimeFrame1             ;animations
    .byte #>SlimeFrame1             
    .byte #8                        ;sprite height
    .byte #$D8                      ;color
    .byte #<EnemyGreenSlimeUpdate   ;update code
    .byte #>EnemyGreenSlimeUpdate
    .byte #1                        ;hp
    .byte #1                        ;damage
    .byte #ENEMY_SIZE_NORMAL

EnemyDefOrangeSlime
    .byte #<SlimeFrame0             ;animations
    .byte #>SlimeFrame0             
    .byte #<SlimeFrame1             ;animations
    .byte #>SlimeFrame1             
    .byte #8                        ;sprite height
    .byte #$1E                      ;color
    .byte #<EnemySlimeUpdate   ;update code
    .byte #>EnemySlimeUpdate
    .byte #3                        ;hp
    .byte #1                        ;damage
    .byte #ENEMY_SIZE_NORMAL

EnemyDefGreenToady
    .byte #<ToadyFrame0             ;animations
    .byte #>ToadyFrame0             
    .byte #<ToadyFrame1             ;animations
    .byte #>ToadyFrame1             
    .byte #12                       ;sprite height
    .byte #$C6                      ;color
    .byte #<EnemyToadyUpdate        ;update code
    .byte #>EnemyToadyUpdate
    .byte #7                        ;hp
    .byte #1                        ;damage
    .byte #ENEMY_SIZE_NORMAL

EnemyDefBat
    .byte #<BatFrame0             ;animations
    .byte #>BatFrame0             
    .byte #<BatFrame1             ;animations
    .byte #>BatFrame1             
    .byte #8                        ;sprite height
    .byte #$AA
    .byte #<EnemyBatUpdate   ;update code
    .byte #>EnemyBatUpdate
    .byte #1                        ;hp
    .byte #1                        ;damage
    .byte #ENEMY_SIZE_NORMAL
EnemyDefCrocky
    .byte #<CrockyFrame
    .byte #>CrockyFrame
    .byte #<CrockyFrame
    .byte #>CrockyFrame
    .byte #25                        ;sprite height
    .byte #$C6
    .byte #<EnemyCrockyUpdate   ;update code
    .byte #>EnemyCrockyUpdate
    .byte #10                        ;hp
    .byte #1                        ;damage
    .byte #ENEMY_SIZE_DOUBLE|NUSIZ_MISSILE_8

EnemyDefCaveEntrance
    .byte #<CaveEntranceGfx             ;animations
    .byte #>CaveEntranceGfx             
    .byte #<CaveEntranceGfx             ;animations
    .byte #>CaveEntranceGfx             
    .byte #20                        ;sprite height
    .byte #$00
    .byte #<EnemyCaveUpdate   ;update code
    .byte #>EnemyCaveUpdate
    .byte #ENEMY_HP_INVINCIBLE                        ;hp
    .byte #ENEMY_DMG_NONE                            ;damage
    .byte #ENEMY_SIZE_DOUBLE

EnemyDefSnake
    .byte #<SnakeFrame0
    .byte #>SnakeFrame0           
    .byte #<SnakeFrame1             ;animations
    .byte #>SnakeFrame1             
    .byte #8                        ;sprite height
    .byte #$40
    .byte #<EnemyToadyUpdate   ;update code
    .byte #>EnemyToadyUpdate
    .byte #2                        ;hp
    .byte #4                            ;damage
    .byte #ENEMY_SIZE_NORMAL
    
EnemyDefBug
    .byte #<BugFrame0
    .byte #>BugFrame0           
    .byte #<BugFrame1             ;animations
    .byte #>BugFrame1             
    .byte #10                       ;sprite height
    .byte #$1E
    .byte #<EnemyBugUpdate   ;update code
    .byte #>EnemyBugUpdate
    .byte #4                        ;hp
    .byte #2                            ;damage
    .byte #ENEMY_SIZE_NORMAL

EnemyDefRedBug
    .byte #<BugFrame0
    .byte #>BugFrame0           
    .byte #<BugFrame1             ;animations
    .byte #>BugFrame1             
    .byte #10                       ;sprite height
    .byte #ENEMY_COLOR_REDBUG
    .byte #<EnemyBugUpdate   ;update code
    .byte #>EnemyBugUpdate
    .byte #8                        ;hp
    .byte #2                            ;damage
    .byte #ENEMY_SIZE_DOUBLE|NUSIZ_MISSILE_4

EnemyDefFlyingBat
    .byte #<BatFrame0             ;animations
    .byte #>BatFrame0             
    .byte #<BatFrame1             ;animations
    .byte #>BatFrame1             
    .byte #8                        ;sprite height
    .byte #ENEMY_COLOR_FLYING
    .byte #<EnemyBatUpdate   ;update code
    .byte #>EnemyBatUpdate
    .byte #4                        ;hp
    .byte #3                        ;damage
    .byte #ENEMY_SIZE_NORMAL

EnemyDefSpider
    .byte #<SpiderFrame0             ;animations
    .byte #>SpiderFrame0             
    .byte #<SpiderFrame1             ;animations
    .byte #>SpiderFrame1             
    .byte #8                        ;sprite height
    .byte #ENEMY_COLOR_REDBUG
    .byte #<EnemySlimeUpdate   ;update code
    .byte #>EnemySlimeUpdate
    .byte #6                        ;hp
    .byte #3                        ;damage
    .byte #ENEMY_SIZE_NORMAL

EnemyDefJumper
    .byte #<JumperFrame0             ;animations
    .byte #>JumperFrame0             
    .byte #<JumperFrame1             ;animations
    .byte #>JumperFrame1             
    .byte #8                        ;sprite height
    .byte #ENEMY_COLOR_FLYING
    .byte #<EnemyJumperUpdate   ;update code
    .byte #>EnemyJumperUpdate
    .byte #3                        ;hp
    .byte #2                        ;damage
    .byte #ENEMY_SIZE_NORMAL

EnemyDefJumperBoss
    .byte #<JumperFrame0             ;animations
    .byte #>JumperFrame0             
    .byte #<JumperFrame1             ;animations
    .byte #>JumperFrame1             
    .byte #8                        ;sprite height
    .byte #$1E
    .byte #<EnemyJumperUpdate   ;update code
    .byte #>EnemyJumperUpdate
    .byte #25                        ;hp
    .byte #10                        ;damage
    .byte #ENEMY_SIZE_DOUBLE

EnemyDefTrap
    .byte #<TrapFrame0             ;animations
    .byte #>TrapFrame0             
    .byte #<TrapFrame0             ;animations
    .byte #>TrapFrame0             
    .byte #8                        ;sprite height
    .byte #ENEMY_COLOR_FLYING
    .byte #<EnemyTrapUpdate   ;update code
    .byte #>EnemyTrapUpdate
    .byte #ENEMY_HP_DYNAMITE
    .byte #8                        ;damage
    .byte #ENEMY_SIZE_NORMAL

EnemyDefOverlay
    .byte #<CaveEntranceGfx
    .byte #>CaveEntranceGfx
    .byte #<CaveEntranceGfx
    .byte #>CaveEntranceGfx
    .byte #12                        ;sprite height
    .byte #ROOM_COLOR_GREEN
    .byte #<EnemyOverlayUpdate   ;update code
    .byte #>EnemyOverlayUpdate
    .byte #ENEMY_HP_INVINCIBLE
    .byte #ENEMY_DMG_NONE
    .byte #ENEMY_SIZE_QUAD

EnemyDefGhost
    .byte #<GhostFrame0
    .byte #>GhostFrame0
    .byte #<GhostFrame0
    .byte #>GhostFrame0
    .byte #8                        ;sprite height
    .byte #ROOM_COLOR_GREEN
    .byte #<EnemyGhostUpdate
    .byte #>EnemyGhostUpdate
    .byte #5
    .byte #4
    .byte #ENEMY_SIZE_NORMAL

EnemyDefGhostBoss
    .byte #<GhostFrame0
    .byte #>GhostFrame0
    .byte #<GhostFrame0
    .byte #>GhostFrame0
    .byte #8                        ;sprite height
    .byte #ENEMY_COLOR_FLYING
    .byte #<EnemyGhostUpdate
    .byte #>EnemyGhostUpdate
    .byte #18
    .byte #6
    .byte #ENEMY_SIZE_DOUBLE

EnemyDefBoulder
    .byte #<BoulderFrame0             ;animations
    .byte #>BoulderFrame0             
    .byte #<BoulderFrame0             ;animations
    .byte #>BoulderFrame0             
    .byte #14                        ;sprite height
    .byte #ENEMY_COLOR_FLYING
    .byte #<EnemyBoulderUpdate   ;update code
    .byte #>EnemyBoulderUpdate
    .byte #ENEMY_HP_DYNAMITE
    .byte #ENEMY_DMG_BARRIER                        ;damage
    .byte #ENEMY_SIZE_DOUBLE
EnemyDefBoss3
    .byte #<BatFrame0             ;animations
    .byte #>BatFrame0             
    .byte #<BatFrame1             ;animations
    .byte #>BatFrame1             
    .byte #8                        ;sprite height
    .byte #ENEMY_COLOR_BOSSBAT
    .byte #<EnemyBatUpdate   ;update code
    .byte #>EnemyBatUpdate
    .byte #50
    ;.byte #5
    .byte #5
    .byte #ENEMY_SIZE_DOUBLE|NUSIZ_MISSILE_8

EnemyDefBoss4
    .byte #<Boss3Frame0             ;animations
    .byte #>Boss3Frame0             
    .byte #<Boss3Frame0             ;animations
    .byte #>Boss3Frame0             
    .byte #14                        ;sprite height
    .byte #ENEMY_COLOR_FLYING
    .byte #<EnemyBoss3Update   ;update code
    .byte #>EnemyBoss3Update
    .byte #85
    .byte #4
    .byte #ENEMY_SIZE_DOUBLE|NUSIZ_MISSILE_8
EnemyDefBoss5
    .byte #<MainBossFrame0             ;animations
    .byte #>MainBossFrame0             
    .byte #<MainBossFrame1             ;animations
    .byte #>MainBossFrame1             
    .byte #19                        ;sprite height
    .byte #ENEMY_COLOR_FLYING
    .byte #<FinalBossUpdate   ;update code
    .byte #>FinalBossUpdate
    .byte #100
    .byte #10                        ;damage
    .byte #ENEMY_SIZE_QUAD|NUSIZ_MISSILE_8
EnemyDefSkeleton
    .byte #<SkeletonFrame0
    .byte #>SkeletonFrame0           
    .byte #<SkeletonFrame1             ;animations
    .byte #>SkeletonFrame1             
    .byte #13                       ;sprite height
    .byte #$1E
    .byte #<EnemyBugUpdate   ;update code
    .byte #>EnemyBugUpdate
    .byte #8                        ;hp
    .byte #7                            ;damage
    .byte #ENEMY_SIZE_NORMAL
EnemyDefRedToady
    .byte #<ToadyFrame0             ;animations
    .byte #>ToadyFrame0             
    .byte #<ToadyFrame1             ;animations
    .byte #>ToadyFrame1             
    .byte #12                       ;sprite height
    .byte #$1E                      ;color
    .byte #<EnemyToadyUpdate        ;update code
    .byte #>EnemyToadyUpdate
    .byte #20                        ;hp
    .byte #10                        ;damage
    .byte #ENEMY_SIZE_NORMAL
EnemyDefSwitch
    .byte #<SwitchFrame0             ;animations
    .byte #>SwitchFrame0             
    .byte #<SwitchFrame1             ;animations
    .byte #>SwitchFrame1             
    .byte #9                       ;sprite height
    .byte #$1E                      ;color
    .byte #<EnemySwitchUpdate        ;update code
    .byte #>EnemySwitchUpdate
    .byte #50                        ;hp
    .byte #ENEMY_DMG_NONE ;damage
    .byte #ENEMY_SIZE_NORMAL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TOADY_MODE_NORMAL   = %00100000
TOADY_MODE_PAUSE    = %01000000
TOADY_MODE_CHASE    = %10000000

TOADY_MODE_MASK     = %11100000

TOADY_TIMER_MASK    = %00011111

TOADY_PAUSE_TIME    = %00011111
TOADY_CHASE_TIME    = %00011111



EnemyOverlayUpdate SUBROUTINE
        ldy #57
        ;sta Enemy0Y,X
        lda RoomDef
        eor #<RoomDef_6_6
        beq .rightRoom
        ;lda #4
        lda #167
        Skip2Bytes
.rightRoom
        lda #127
        ;sta Enemy0X,X
        jmp SetEnemyPosition




EnemyToadyUpdate SUBROUTINE

    lda Enemy0State0,X
    bne .SkipInitToady
    lda #TOADY_MODE_NORMAL
    sta Enemy0State0,X
    jsr SetRandomDirection4Way
.SkipInitToady
    ;if we're in chase mode
    and #TOADY_MODE_CHASE
    beq .NotChaseMode
    ; see if chase timer done
    lda Enemy0State0,X
    and #TOADY_TIMER_MASK
    bne .NotChaseTimerDone
    ;jmp .NotChaseTimerDone
    ; if so, back to normal mode
    lda #TOADY_MODE_NORMAL
    sta Enemy0State0,X
    jmp GeneralEnemyDone
.NotChaseTimerDone
    IfNoWallCollision ToadyChaseMove
    lda #TOADY_MODE_NORMAL
    sta Enemy0State0,X
    jsr SetRandomDirection4Way
    jmp GeneralEnemyDone
    ; move
ToadyChaseMove
    dec Enemy0State0,X
    jmp GeneralEnemyMove
    ;jsr MoveEnemyXByDirection
    ;jsr MoveEnemyYByDirection
    ; decrement chase timer
    ;jmp GeneralEnemyDone

.NotChaseMode

    ; if in pause mode
    lda Enemy0State0,X
    and #TOADY_MODE_PAUSE
    beq .NotPauseMode
    ; see if timer done
    lda Enemy0State0,X
    and #TOADY_TIMER_MASK
    bne .NotPauseTimerDone

    ;   if so, change to chase mode, set timer
    lda #TOADY_MODE_CHASE
    ora #TOADY_CHASE_TIME
    sta Enemy0State0,X
    jmp GeneralEnemyDone

.NotPauseTimerDone
    ; decrement timer
    dec Enemy0State0,X
    jmp GeneralEnemyDone

.NotPauseMode
    ; otherwise, see if lined up with character
    ; if so, set to pause mode, set timer
    lda Enemy0X,X
    sbc PlayerX
    adc #5
    bmi .NotLinedUpX
    sbc #10
    bpl .NotLinedUpX
    ;chase main char only on Y axis
    jsr PursueMainChar
    lda Enemy0Dir,X
    and #DIR_X_MASK
    sta Enemy0Dir,X
    lda #TOADY_PAUSE_TIME
    ora #TOADY_MODE_PAUSE
    sta Enemy0State0,X
    jmp GeneralEnemyDone

.NotLinedUpX

    lda Enemy0Y,X
    sbc PlayerY
    adc #5
    bmi .NotLinedUpY
    sbc #10
    bpl .NotLinedUpY
    ;chase main char only on X axis
    jsr PursueMainChar
    lda Enemy0Dir,X
    and #DIR_Y_MASK
    sta Enemy0Dir,X
    lda #TOADY_PAUSE_TIME
    ora #TOADY_MODE_PAUSE
    sta Enemy0State0,X
    jmp GeneralEnemyDone

.NotLinedUpY
    ; if wall collision, change direction
    IfNoWallCollision ToadyMove
    jsr SetRandomDirection4Way
    ; move (every other frame)
ToadyMove
    lda FrameCtr
    and #1
    beq GeneralEnemyDone
    jmp GeneralEnemyMove
    ;jsr MoveEnemyXByDirection
    ;jsr MoveEnemyYByDirection


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

EnemyGreenSlimeUpdate SUBROUTINE
        IfNoWallCollision CheckIfGreenSlimeSwitchDirection
        jmp .SwitchDirection
CheckIfGreenSlimeSwitchDirection
        lda Enemy0State0,X
        beq .MaybeSwitchDirection
        dec Enemy0State0,X
        bne .Move
.MaybeSwitchDirection
        jsr GetRandomNumber
        and #$0F
        cmp #$0F
        bne .Move
.SwitchDirection
        jsr SetRandomDirection4Way
        lda #$20
        sta Enemy0State0,X
.Move
GeneralEnemyMove
        jsr MoveEnemyXByDirection
        jsr MoveEnemyYByDirection
GeneralEnemyDone
        EndEnemyFunc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

EnemySlimeUpdate SUBROUTINE

        IfNoWallCollision SlimeSkipSwitchMode
        ;if we hit a wall, switch modes
        sec
        lda #1
        sbc Enemy0State0,X
        sta Enemy0State0,X
        beq ChangeSlimeDirection
        jmp .UpdateSlimeInChaseMode

SlimeSkipSwitchMode

        ;check mode and branch
        lda Enemy0State0,X
        and #1
        bne .UpdateSlimeInChaseMode

.UpdateSlimeInWanderMode

        ;if no direction, change direction
        lda Enemy0Dir,X
        beq ChangeSlimeDirection

        ;wander mode
        lda FrameCtr
        adc Enemy0X,X
        beq ChangeSlimeDirection
        jmp MoveSlime

ChangeSlimeDirection
        jsr SetRandomDirection
        jmp MoveSlime

.UpdateSlimeInChaseMode
        jsr PursueMainChar

MoveSlime
        ;see if we should shoot
        lda EnemyColor
        cmp #$1E
        bne .skipShoot
        jsr GetRandomNumber
        and #%00111111
        bne .skipShoot
        ;
        lda Enemy0Dir,X
        jsr EnemyUpdateMaybeSpawnBulletWithDir

.skipShoot
        lda FrameCtr
        and #%00000011
        bne GeneralEnemyDone
        jmp GeneralEnemyMove


        

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
EnemyBugUpdate SUBROUTINE
        IfNoWallCollision BugContinueOn
        jmp .nextPhase
BugContinueOn
        lda Enemy0State0,X
        beq .nextPhase
        dec Enemy0State0,X
        jmp .move
.nextPhase
        ;if red bug, maybe shoot
        lda EnemyColor
        sec
        cmp #ENEMY_COLOR_REDBUG
        bne .afterShoot
        jsr GetRandomNumber
        jsr EnemyUpdateMaybeSpawnBulletWithDir

.afterShoot
        lda #40
        sta Enemy0State0,X
        lda Enemy0Dir,X
        and #~DIR_MASK
        beq .pickDirection
        lda Enemy0Dir,X
        and #DIR_MASK
        sta Enemy0Dir,X
        ;jmp .done
        jmp GeneralEnemyDone
.pickDirection
        jsr SetRandomDirection4Way
.move
        jmp GeneralEnemyMove
        ;jsr MoveEnemyXByDirection
        ;jsr MoveEnemyYByDirection




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

EnemyBatUpdate SUBROUTINE
    lda Enemy0State0,X
    bne .SkipInitBat

    lda #1
    sta Enemy0State0,X

    jsr GetRandomNumber
    and #1
    beq .BatLeft
    lda #DIR_X_RIGHT
    Skip2Bytes
.BatLeft
    lda #DIR_X_LEFT
    ora Enemy0Dir,X
    sta Enemy0Dir,X


.VertChooser
    jsr GetRandomNumber
    and #1
    beq .BatUp
    lda #DIR_Y_DOWN
    Skip2Bytes
.BatUp
    lda #DIR_Y_UP
    ora Enemy0Dir,X
    sta Enemy0Dir,X

        
.SkipInitBat    
    lda FrameCtr
    and #%00000001
    bne .OddFrame

.EvenFrame
    IfNoWallCollision MoveBatEvenFrame
    ;swap vertical direction
    lda Enemy0Dir,X
    eor #~DIR_Y_MASK
    sta Enemy0Dir,X

MoveBatEvenFrame
    jsr MoveEnemyXByDirection
    jmp .DoneMovingBat

.OddFrame
    IfNoWallCollision MoveBatOddFrame
    ; swap horizontal direction
    lda Enemy0Dir,X
    eor #~DIR_X_MASK
    sta Enemy0Dir,X

MoveBatOddFrame
    jsr MoveEnemyYByDirection
.DoneMovingBat
    lda EnemyColor
    cmp #ENEMY_COLOR_BOSSBAT
    bne .batDone
    jsr ShootAtPlayer
.batDone
    EndEnemyFunc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;EnemyOrangeSlimeUpdate SUBROUTINE

        ;IfNoWallCollision CheckIfOrangeSlimeSwitchDirection
        ;jmp .SwitchDirection
;CheckIfOrangeSlimeSwitchDirection
        ;lda Enemy0State0,X
        ;beq .MaybeSwitchDirection
        ;dec Enemy0State0,X
        ;bne .PossiblyShoot
;.MaybeSwitchDirection
        ;jsr GetRandomNumber
        ;and #$0F
        ;cmp #$0F
        ;bne .PossiblyShoot
;.SwitchDirection
        ;jsr SetRandomDirection4Way
        ;lda #$20
        ;sta Enemy0State0,X
        ;jmp .Move
;.PossiblyShoot
        ;lda Enemy0State0,X
        ;beq .Move
        ;lda EnemyMissileDir
        ;and #%10000000
        ;bne .Move
        ;jsr GetRandomNumber
        ;and #%00111111
        ;bne .Move
        ;
        ;jsr EnemyUpdateSpawnBullet
;
;
;.Move
        ;lda FrameCtr
        ;and #%00000011
        ;bne .DoneMoving
;
        ;jmp GeneralEnemyMove
;.DoneMoving
;OrangeSlimeEnd

        ;EndEnemyFunc
        
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

EnemyCrockyUpdate SUBROUTINE
        jsr PursueMainChar
        lda #30
        sta Enemy0Y,X
        lda FrameCtr
        and #%00000001
        beq .CheckShoot
        jsr MoveEnemyXByDirection
.CheckShoot
        lda #DIR_Y_UP
        jsr EnemyUpdateMaybeSpawnBulletWithDir
        ldy #-1
        jsr AdjustEnemyBulletXOnSpawn

CrockyDone
        EndEnemyFunc


    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        


        
EnemyTrapUpdate SUBROUTINE
        txa
        beq .second
        Skip2Bytes
.second
        lda #-50
        adc #80
        sta Enemy0Y,X
        lda Enemy0Dir,X
        bne .move
        lda #DIR_X_RIGHT
        sta Enemy0Dir,X
.move
        jsr MoveEnemyXByDirection
        jsr MoveEnemyXByDirection
        jmp GeneralEnemyMove
        ;jsr MoveEnemyXByDirection
        ;EndEnemyFunc

EnemyGhostUpdate SUBROUTINE
        lda Enemy0State0,X
        beq .warp
        cmp #50
        bpl .done
        jmp .move
.warp
        lda #ENEMY_COLOR_FADED
        sta EnemyColor
        lda #85
        sta Enemy0State0,X
        jsr GetRandomNumber
        and #%01111111
        adc #10
        sta Enemy0X,X
        jsr GetRandomNumber
        and #%00111111
        adc #15
        sta Enemy0Y,X
        jsr SetRandomDirection4Way
        jmp CrockyDone
.move
        jsr MoveEnemyXByDirection
        jsr MoveEnemyYByDirection
        lda #ENEMY_COLOR_FLYING
        sta EnemyColor

        dec Enemy0State0,X

        lda EnemyScaleNuSiz
        eor #ENEMY_SIZE_NORMAL
        beq CrockyDone
        jmp GeneralEnemyMove
.done 
        dec Enemy0State0
GhostDone
        EndEnemyFunc
        ;jsr MoveEnemyXByDirection
        ;jsr MoveEnemyYByDirection




        ;EndEnemyFunc

         
 

FinalBossUpdateBlock SUBROUTINE

.RightFrame
    lda Enemy0X
    adc #31
    sta Enemy0X
    lda Enemy0Dir
    and #~DIR_X_LEFT
    ora #DIR_X_RIGHT
    sta Enemy0Dir



.bossMoveDone
    jmp GhostDone

FinalBossUpdate 

    lda #10
    sta EnemyAnimCounter
    clc
    lda FrameCtr
    and #1
    ;bne .LeftFrame
    beq .RightFrame

.LeftFrame

    ;reposition
    lda Enemy0X
    sbc #30
    sta Enemy0X

    lda Enemy0Dir
    and #~DIR_X_RIGHT
    ora #DIR_X_LEFT
    sta Enemy0Dir

    lda #ENEMY_COLOR_MAINBOSS
    sta EnemyColor

    lda Enemy2State0
    beq .noDelay
    jmp MainBossDelay

.noDelay
    BankJump FinalBossPart2,BANK_2

    ;EndEnemyFunc

UpDownInvincible
    lda #ENEMY_COLOR_REDBUG
    sta EnemyColor
    lda Enemy0Dir
    ora #FLAG_ALREADY_HIT
    sta Enemy0Dir
    bne .bossMoveDone ;optimized jmp
    ;jmp GhostDone


        
EnemyJumperUpdate SUBROUTINE
        lda Enemy0State0,x
        bne .move
        ;next stage
        
        ;first reset timer
        jsr GetRandomNumber
        and #%00111111
        sta Enemy0State0,x

        ;then figure out how to transition
        lda Enemy0Dir,X
        and #DIR_Y_UP
        beq .wasntGoingUp
        ; was going up, change to going down
        lda #Enemy0Dir,X
        and #~DIR_Y_UP
        ora #DIR_Y_DOWN
        sta Enemy0Dir,X
        bne .move ;optimized jmp
        ;jmp .move
        
.wasntGoingUp
        lda Enemy0Dir,X
        and #DIR_Y_DOWN
        beq .wasntGoingDown

        ; was going down, stop movement
        ;lda Enemy0State0,x
        ;lda EnemyColor
        ;cmp #ENEMY_COLOR_MAINBOSS
        ;bne .normal
        ;lda #%00001000
        ;Skip2Bytes

.normal
        lda #%00111000
        sta Enemy0State0,x
        lda #0
        sta Enemy0Dir,X
        ;jmp .move
        beq .move

.wasntGoingDown
        lda Enemy0State0,x
        ror
        lda #DIR_Y_UP
        bcc .left
        ora #DIR_X_RIGHT
        Skip2Bytes
.left
        ora #DIR_X_RIGHT
        sta Enemy0Dir,x
.storeMovement
.move
        dec Enemy0State0,x
        lda EnemyColor
        cmp #ENEMY_COLOR_MAINBOSS
        bne .done
        ;jsr ShootAtPlayer
        ;jsr FinalBossUpdate
.done
        jmp GeneralEnemyMove

        ;jsr MoveEnemyXByDirection
        ;jsr MoveEnemyYByDirection

        ;EndEnemyFunc


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; for additional enemy functions,
; see bank_a.asm, where it says
; ENEMY_DEFS.
; Squeezing extra enemies into
; space from .align 256's
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
