
RoomDef_0_0
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D5
    .byte #TOP_CLOSED|LEFT_CLOSED|RIGHT_ENEMY|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_BOSS_5
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_1
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D5
    .byte #LEFT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_2
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D5
    .byte #TOP_CLOSED|LEFT_CLOSED|RIGHT_ENEMY
    .byte #ENEMY_TYPE_BOSS_3
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_3
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D5
    .byte #LEFT_CLOSED|BOTTOM_CLOSED|TOP_KEY
    .byte #ENEMY_TYPE_SWITCH
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE|3

    
RoomDef_0_4
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D5
    .byte #LEFT_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_5
    .byte #ROOM_CASTLE
    .byte #COLOR_THEME_CASTLE
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_CAVEENTRANCE
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_6
    .byte #ROOM_OUTDOOR_BOULDER
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #LEFT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_7
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_WASTELAND
    .byte #TOP_CLOSED|LEFT_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_8
    .byte #ROOM_HOURGLASS
    .byte #COLOR_THEME_WASTELAND
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_9
    .byte #ROOM_CHAMBERS
    .byte #COLOR_THEME_D3
    .byte #RIGHT_FAKE|LEFT_CLOSED|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_GREENTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_10
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D3
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_CLOSED|TOP_ENEMY
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ITEM_POWER_UP<<4|0

    
RoomDef_0_11
    .byte #ROOM_WINGS
    .byte #COLOR_THEME_D3
    .byte #LEFT_CLOSED|TOP_CLOSED|BOTTOM_KEY
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|7

    
RoomDef_0_12
    .byte #ROOM_MAZE
    .byte #COLOR_THEME_D3
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_GREENSLIME
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_0_13
    .byte #ROOM_FUNNEL_TOP
    .byte #COLOR_THEME_D3
    .byte #LEFT_CLOSED|BOTTOM_CLOSED|RIGHT_ENEMY
    .byte #ENEMY_TYPE_GREENTOADY
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_0
    .byte #ROOM_FUNNEL_TOP
    .byte #COLOR_THEME_D5
    .byte #RIGHT_CLOSED|LEFT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_1
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D5
    .byte #TOP_CLOSED|RIGHT_CLOSED|LEFT_ENEMY
    .byte #ENEMY_TYPE_BOSS_4
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_2
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D5
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED|TOP_KEY
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE|5

    
RoomDef_1_3
    .byte #ROOM_UFO
    .byte #COLOR_THEME_D5
    .byte #TOP_CLOSED|RIGHT_CLOSED|LEFT_ENEMY
    .byte #ENEMY_TYPE_GHOST_BOSS
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_4
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D5
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED|TOP_KEY
    .byte #ENEMY_TYPE_CROCKY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|2

    
RoomDef_1_5
    .byte #ROOM_OUTDOOR_CLIFF3
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #0
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_6
    .byte #ROOM_HOURGLASS
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #0
    .byte #ENEMY_TYPE_BOULDER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_VERY_NONE<<4

    
RoomDef_1_7
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_WASTELAND
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_8
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_WASTELAND
    .byte #BOTTOM_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_9
    .byte #ROOM_MAZE
    .byte #COLOR_THEME_D3
    .byte #TOP_CLOSED|BOTTOM_ENEMY|RIGHT_FAKE
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_10
    .byte #ROOM_UFO
    .byte #COLOR_THEME_D3
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_11
    .byte #ROOM_LAKE
    .byte #COLOR_THEME_LAVA
    .byte #0
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_12
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_D3
    .byte #LEFT_CLOSED|BOTTOM_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_GHOST
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_1_13
    .byte #ROOM_BACK_FORTH
    .byte #COLOR_THEME_DARK_CAVE
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_0
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_D4
    .byte #LEFT_CLOSED|TOP_CLOSED|BOTTOM_KEY
    .byte #ENEMY_TYPE_BOULDER
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE|6

    
RoomDef_2_1
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D4
    .byte #BOTTOM_CLOSED|LEFT_CLOSED|RIGHT_ENEMY|TOP_KEY
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|6

    
RoomDef_2_2
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|LEFT_CLOSED|BOTTOM_KEY
    .byte #ENEMY_TYPE_CROCKY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|7

    
RoomDef_2_3
    .byte #ROOM_FUNNEL_TOP
    .byte #COLOR_THEME_D4
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_JUMPER_BOSS
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_KEY<<4|4

    
RoomDef_2_4
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_D4
    .byte #LEFT_CLOSED|TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SWITCH
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_5
    .byte #ROOM_OUTDOOR_CLIFF2
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #0
    .byte #ENEMY_TYPE_SNAKE
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_6
    .byte #ROOM_PINES
    .byte #COLOR_THEME_TREES_2
    .byte #0
    .byte #ENEMY_TYPE_REDBUG
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_7
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_WASTELAND
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_8
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_WASTELAND
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_9
    .byte #ROOM_RIVER
    .byte #COLOR_THEME_LAVA
    .byte #TOP_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_10
    .byte #ROOM_RIVER
    .byte #COLOR_THEME_LAVA
    .byte #LEFT_CLOSED|BOTTOM_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_11
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_D3
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_GHOST
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_2_12
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D3
    .byte #LEFT_CLOSED|RIGHT_ENEMY|TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_GREENTOADY
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ITEM_DYNAMITE<<4

    
RoomDef_2_13
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D3
    .byte #BOTTOM_CLOSED|LEFT_ENEMY|TOP_CLOSED|RIGHT_ENEMY
    .byte #ENEMY_TYPE_BOSS_3
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_0
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED
    .byte #ENEMY_TYPE_SWITCH
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_1
    .byte #ROOM_MAZE
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_2
    .byte #ROOM_BIGV
    .byte #COLOR_THEME_D4
    .byte #TOP_KEY
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE|4

    
RoomDef_3_3
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D4
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_4
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D4
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED|LEFT_ENEMY
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_5
    .byte #ROOM_OUTDOOR_CLIFF2
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #0
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_6
    .byte #ROOM_BIG_TREE
    .byte #COLOR_THEME_BIG_TREE
    .byte #0
    .byte #ENEMY_TYPE_REDBUG
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_7
    .byte #ROOM_HOURGLASS
    .byte #COLOR_THEME_WASTELAND
    .byte #0
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_8
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_WASTELAND
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_9
    .byte #ROOM_BACK_FORTH
    .byte #COLOR_THEME_D3
    .byte #RIGHT_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_10
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D3
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SWITCH
    ;.byte #ENEMY_TYPE_SWITCH
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_11
    .byte #ROOM_PILLARS
    .byte #COLOR_THEME_D3
    .byte #TOP_CLOSED|RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_BOOTS<<4

    
RoomDef_3_12
    .byte #ROOM_BACK_FORTH
    .byte #COLOR_THEME_DARK_CAVE
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_3_13
    .byte #ROOM_LAKE
    .byte #COLOR_THEME_LAVA
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_KEY<<4|3

    
RoomDef_4_0
    .byte #ROOM_STARTING
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ITEM_RING<<4

    
RoomDef_4_1
    .byte #ROOM_OVAL
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_2
    .byte #ROOM_GOOFY
    .byte #COLOR_THEME_D4
    .byte #RIGHT_CLOSED|TOP_CLOSED|BOTTOM_KEY
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|6

    
RoomDef_4_3
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D4
    .byte #TOP_KEY|LEFT_CLOSED|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_GHOST
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|6

    
RoomDef_4_4
    .byte #ROOM_CHAMBERS
    .byte #COLOR_THEME_D4
    .byte #LEFT_CLOSED|BOTTOM_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_SWITCH
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_5
    .byte #ROOM_RIVER_START
    .byte #COLOR_THEME_RIVER_START
    .byte #0
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_6
    .byte #ROOM_RIVER_CURVE
    .byte #COLOR_THEME_RIVER
    .byte #0
    .byte #ENEMY_TYPE_OVERLAY
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_7
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_TREES_1
    .byte #0
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE
    ;.byte #ITEM_BOOTS<<4

    
RoomDef_4_8
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_2
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_BUG
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_9
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_1
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_10
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_TREES_1
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_REDBUG
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_11
    .byte #ROOM_BIG_TREE
    .byte #COLOR_THEME_BIG_TREE
    .byte #LEFT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_BUG
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_12
    .byte #ROOM_WINGS
    .byte #COLOR_THEME_DARK_CAVE
    .byte #TOP_CLOSED|RIGHT_CLOSED|LEFT_ENEMY
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_4_13
    .byte #ROOM_OUTDOOR_CLIFFTOP_CAVE
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #LEFT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_CAVEENTRANCE
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_0
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_BOSS_4
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_1
    .byte #ROOM_CHAMBERS
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_KEY<<4|5

    
RoomDef_5_2
    .byte #ROOM_WINGS
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|LEFT_CLOSED|RIGHT_ENEMY|BOTTOM_KEY
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|4

    
RoomDef_5_3
    .byte #ROOM_UFO
    .byte #COLOR_THEME_D4
    .byte #RIGHT_CLOSED|TOP_KEY
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|4

    
RoomDef_5_4
    .byte #ROOM_OVAL
    .byte #COLOR_THEME_D4
    .byte #0
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_5
    .byte #ROOM_CASTLE
    .byte #COLOR_THEME_CASTLE
    .byte #0
    .byte #ENEMY_TYPE_CAVEENTRANCE
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_6
    .byte #ROOM_RIVER_MERGE
    .byte #COLOR_THEME_RIVER
    .byte #0
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

RoomDef_5_7
    .byte #ROOM_RIVER
    .byte #COLOR_THEME_RIVER
    .byte #0
    .byte #ENEMY_TYPE_SPIDER
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_8
    .byte #ROOM_RIVER_WIDE
    .byte #COLOR_THEME_RIVER
    .byte #0
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_9
    .byte #ROOM_RIVER
    .byte #COLOR_THEME_RIVER
    .byte #0
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_10
    ;.byte #ROOM_RIVER
    .byte #ROOM_RIVER_BRIDGE
    .byte #COLOR_THEME_RIVER
    .byte #0
    .byte #ENEMY_TYPE_SNAKE
    .byte #2
    .byte #ROOM_ITEM_NONE
    ;.byte #ITEM_BOOTS << 4


    
RoomDef_5_11
    .byte #ROOM_RIVER
    .byte #COLOR_THEME_RIVER
    .byte #0
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_12
    .byte #ROOM_RIVER_WIDE
    .byte #COLOR_THEME_RIVER
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_5_13
    .byte #ROOM_RIVER
    .byte #COLOR_THEME_RIVER
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_0
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED
    .byte #ENEMY_TYPE_SLIME
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_1
    .byte #ROOM_PILLARS
    .byte #COLOR_THEME_D4
    .byte #LEFT_CLOSED|BOTTOM_CLOSED|TOP_ENEMY
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_2
    .byte #ROOM_STARTING
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_POWER_UP<<4|6

    
RoomDef_6_3
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D4
    .byte #LEFT_CLOSED|BOTTOM_KEY
    .byte #ENEMY_TYPE_GREENSLIME
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE|7

    
RoomDef_6_4
    .byte #ROOM_BACK_FORTH
    .byte #COLOR_THEME_D4
    .byte #TOP_KEY|RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE|7

    
RoomDef_6_5
    .byte #ROOM_RIVER_START
    .byte #COLOR_THEME_RIVER_START
    .byte #0
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_6
    .byte #ROOM_RIVER_CURVE
    .byte #COLOR_THEME_RIVER
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_OVERLAY
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_7
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_TREES_1
    .byte #BOTTOM_CLOSED|RIGHT_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_JUMPER_BOSS
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_POWER_UP<<4|4

    
RoomDef_6_8
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_1
    .byte #TOP_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_REDBUG
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_9
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_TREES_1
    .byte #0
    .byte #ENEMY_TYPE_SNAKE
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_10
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_TREES_1
    .byte #0
    .byte #ENEMY_TYPE_BUG
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_11
    .byte #ROOM_PINES
    .byte #COLOR_THEME_TREES_1
    .byte #0
    .byte #ENEMY_TYPE_BUG
    ;.byte #>EnemyDefBug
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_12
    .byte #ROOM_BIG_TREE
    .byte #COLOR_THEME_BIG_TREE
    .byte #0
    .byte #ENEMY_TYPE_SPIDER
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_6_13
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_2
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SPIDER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_0
    .byte #ROOM_OVAL
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|LEFT_FAKE
    .byte #ENEMY_TYPE_REDTOADY
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_1
    .byte #ROOM_OVAL
    .byte #COLOR_THEME_D4
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED|LEFT_ENEMY
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_ARMOR_UP<<4|7

    
RoomDef_7_2
    .byte #ROOM_OVAL
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|BOTTOM_CLOSED|LEFT_CLOSED
    .byte #ENEMY_TYPE_SWITCH
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_3
    .byte #ROOM_OVAL
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|BOTTOM_KEY|RIGHT_ENEMY
    .byte #ENEMY_TYPE_SKELETON
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE|6

    
RoomDef_7_4
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_D4
    .byte #TOP_KEY|RIGHT_FAKE|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE|6

    
RoomDef_7_5
    .byte #ROOM_OUTDOOR_CLIFF2
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #0
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_6
    .byte #ROOM_OUTDOOR_BOULDER
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #0
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_7
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_2
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SPIDER
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_8
    .byte #ROOM_PINES
    .byte #COLOR_THEME_TREES_2
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SNAKE
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_9
    .byte #ROOM_OUTDOOR_BOULDER
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_SPIDER
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_10
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_TREES_2
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_REDBUG
    ;.byte #>EnemyDefRedBug
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_11
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_1
    .byte #0
    .byte #ENEMY_TYPE_SNAKE
    ;.byte #>EnemyDefSnake
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_12
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_2
    .byte #0
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_7_13
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_TREES_1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SPIDER
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_0
    .byte #ROOM_ROUND
    .byte #COLOR_THEME_D4
    .byte #TOP_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_GREENSLIME
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_1
    .byte #ROOM_FUNNEL_TOP
    .byte #COLOR_THEME_D4
    .byte #LEFT_CLOSED|RIGHT_CLOSED|TOP_KEY
    .byte #ENEMY_TYPE_GHOST
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE|7

    
RoomDef_8_2
    .byte #ROOM_GOOFY
    .byte #COLOR_THEME_D4
    .byte #RIGHT_CLOSED|LEFT_ENEMY
    .byte #ENEMY_TYPE_ORANGESLIME
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_3
    .byte #ROOM_HOURGLASS
    .byte #COLOR_THEME_D4
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_4
    .byte #ROOM_CHAMBERS
    .byte #COLOR_THEME_D4
    .byte #BOTTOM_CLOSED|RIGHT_CLOSED|TOP_ENEMY
    .byte #ENEMY_TYPE_REDTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_5
    .byte #ROOM_OUTDOOR_CLIFFTOP
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #0
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_6
    .byte #ROOM_PINES
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_7
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE
RoomDef_8_8
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|TOP_ENEMY
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE
RoomDef_8_9
    .byte #ROOM_BIGV
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_GREENSLIME
    ;.byte #>EnemyDefGreenSlime
    .byte #2
    .byte #ROOM_ITEM_NONE
RoomDef_8_10
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_CROCKY
    ;.byte #>EnemyDefCrocky
    .byte #1
    .byte #ROOM_ITEM_NONE

    
;StartRoom
RoomDef_8_11
    .byte #ROOM_OUTDOOR_CLIFFTOP_CAVE
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #0
    .byte #ENEMY_TYPE_CAVEENTRANCE
    ;.byte #>EnemyDefCaveEntrance
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_12
    .byte #ROOM_PINES
    .byte #COLOR_THEME_TREES_1
    .byte #BOTTOM_CLOSED | RIGHT_CLOSED
    .byte #ENEMY_TYPE_SPIDER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_8_13
    .byte #ROOM_OUTDOOR_BOULDER
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_9_0
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D2
    .byte #LEFT_CLOSED|TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_GHOST_BOSS
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_KEY<<4|2

    
RoomDef_9_1
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D2
    .byte #BOTTOM_CLOSED|LEFT_CLOSED|TOP_CLOSED|RIGHT_ENEMY
    .byte #ENEMY_TYPE_GHOST
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_9_2
    .byte #ROOM_UFO
    .byte #COLOR_THEME_D2
    .byte #LEFT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_9_3
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D2
    .byte #TOP_CLOSED|LEFT_CLOSED|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_GREENTOADY
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_9_4
    .byte #ROOM_BIGV
    .byte #COLOR_THEME_D2
    .byte #LEFT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_LANTERN<<4

    
RoomDef_9_5
    .byte #ROOM_OUTDOOR_CLIFF3
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_SNAKE
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_9_6
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

RoomDef_9_7
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_D1
    .byte #TOP_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_POWER_UP << 4|1
RoomDef_9_8
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D1
    .byte #LEFT_ENEMY|TOP_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_ORANGESLIME
    ;.byte #>EnemyDefOrangeSlime
    .byte #2
    .byte #ROOM_ITEM_NONE;
RoomDef_9_9
    .byte #ROOM_FUNNEL_TOP
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE;|2
RoomDef_9_10
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_BAT
    ;.byte #>EnemyDefBat
    .byte #2
    .byte #ROOM_ITEM_NONE;

    
RoomDef_9_11
    .byte #ROOM_OUTDOOR_CLIFFTOP
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #0
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_9_12
    .byte #ROOM_PINES
    .byte #COLOR_THEME_TREES_2
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_9_13
    .byte #ROOM_PINES
    .byte #COLOR_THEME_TREES_1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_10_0
    .byte #ROOM_PILLARS
    .byte #COLOR_THEME_D2
    .byte #TOP_CLOSED|BOTTOM_KEY|LEFT_ENEMY
    .byte #ENEMY_TYPE_SLIME
    .byte #1
    .byte #ROOM_ITEM_NONE|2

    
RoomDef_10_1
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_D2
    .byte #TOP_KEY|BOTTOM_KEY
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE|2

    
RoomDef_10_2
    .byte #ROOM_PILLARS
    .byte #COLOR_THEME_D2
    .byte #TOP_CLOSED|BOTTOM_KEY
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE|1

    
RoomDef_10_3
    .byte #ROOM_MAZE
    .byte #COLOR_THEME_D2
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_GHOST
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_10_4
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D2
    .byte #LEFT_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE
    ;.byte ITEM_LANTERN<<4

    
RoomDef_10_5
    .byte #ROOM_OUTDOOR_CLIFFTOP_CAVE
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_CAVEENTRANCE
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_10_6
    .byte #ROOM_BIG_TREE
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_REDBUG
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
;StartRoom
RoomDef_10_7
    .byte #ROOM_STARTING
    .byte #COLOR_THEME_D1
    .byte #BOTTOM_ENEMY|LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    .byte #2
    .byte #ROOM_ITEM_NONE
    ;.byte #ITEM_BOW_ARROWS<<4|2

RoomDef_10_8
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|BOTTOM_ENEMY|RIGHT_ENEMY
    .byte #ENEMY_TYPE_SLIME
    .byte #3
    .byte #ROOM_ITEM_NONE

RoomDef_10_9
    .byte #ROOM_UFO
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_KEY
    .byte #ENEMY_TYPE_ORANGESLIME
    ;.byte #>EnemyDefOrangeSlime
    .byte #2
    .byte #ROOM_ITEM_NONE|0

RoomDef_10_10
    .byte #ROOM_PILLARS
    .byte #COLOR_THEME_D1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_GREENTOADY
    ;.byte #>EnemyDefGreenToady
    .byte #1
    ;.byte #ITEM_HP_UP<<4|0 ;progress flag 0
    .byte #ROOM_ITEM_NONE

    
RoomDef_10_11
    .byte #ROOM_OUTDOOR_CLIFF2
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #0
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_10_12
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_1
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_10_13
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_TREES_1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SNAKE
    ;.byte #>EnemyDefSnake
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_0
    .byte #ROOM_BACK_FORTH
    .byte #COLOR_THEME_DARK_CAVE
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_1
    .byte #ROOM_PILLARS
    .byte #COLOR_THEME_D2
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_GREENSLIME
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_2
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D2
    .byte #TOP_CLOSED|BOTTOM_CLOSED|LEFT_FAKE
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_3
    .byte #ROOM_STARTING
    .byte #COLOR_THEME_DARK_CAVE
    .byte #LEFT_CLOSED|TOP_CLOSED|RIGHT_ENEMY
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_4
    .byte #ROOM_FUNNEL_TOP
    .byte #COLOR_THEME_DARK_CAVE
    ;.byte #COLOR_THEME_D2
    .byte #BOTTOM_CLOSED|RIGHT_ENEMY
    .byte #ENEMY_TYPE_GHOST
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_5
    .byte #ROOM_OUTDOOR_CLIFF3
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #LEFT_CLOSED
    .byte #ENEMY_TYPE_SNAKE
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_6
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_BUG
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_7
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_D1
    .byte #BOTTOM_CLOSED|TOP_CLOSED|LEFT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_BOW_ARROWS<<4

RoomDef_11_8
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D1
    .byte #RIGHT_CLOSED|TOP_CLOSED|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_ORANGESLIME
    ;.byte #>EnemyDefOrangeSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

RoomDef_11_9
    .byte #ROOM_PILLARS
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_GREENSLIME
    ;.byte #>EnemyDefGreenSlime
    .byte #2
    .byte #ITEM_KEY<<4|0
RoomDef_11_10
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_D1
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_BAT
    ;.byte #>EnemyDefBat
    .byte #2
    .byte #ROOM_ITEM_NONE
    
RoomDef_11_11
    .byte #ROOM_OUTDOOR_CLIFFTOP
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_SNAKE
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_12
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_DARK_CAVE
    .byte #LEFT_CLOSED|TOP_CLOSED|RIGHT_ENEMY
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_11_13
    .byte #ROOM_OUTDOOR_CLIFFTOP_CAVE
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_CAVEENTRANCE
    ;.byte #>EnemyDefSnake
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_0
    .byte #ROOM_MAZE
    .byte #COLOR_THEME_DARK_CAVE
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_1
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D2
    .byte #TOP_CLOSED|BOTTOM_ENEMY
    .byte #ENEMY_TYPE_CROCKY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_ARMOR_UP << 4|3

    
RoomDef_12_2
    .byte #ROOM_BACK_FORTH
    .byte #COLOR_THEME_D2
    .byte #TOP_CLOSED|RIGHT_FAKE|LEFT_ENEMY
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_3
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D2
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_ORANGESLIME
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_4
    .byte #ROOM_BACK_FORTH
    .byte #COLOR_THEME_D2
    .byte #TOP_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_5
    .byte #ROOM_OUTDOOR_CLIFFTOP
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_6
    .byte #ROOM_TREES
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #BOTTOM_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_7
    .byte #ROOM_PILLARS
    .byte #COLOR_THEME_D1
    .byte #TOP_CLOSED|LEFT_ENEMY|RIGHT_CLOSED
    .byte #ENEMY_TYPE_GREENTOADY
    ;.byte #>EnemyDefGreenToady
    .byte #1
    .byte #ROOM_ITEM_NONE
RoomDef_12_8
    .byte #ROOM_FUNNEL_TOP
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_BAT
    ;.byte #>EnemyDefBat
    .byte #3
    .byte #ROOM_ITEM_NONE
RoomDef_12_9
    .byte #ROOM_BIGV
    .byte #COLOR_THEME_D1
    .byte #LEFT_CLOSED|RIGHT_CLOSED|TOP_ENEMY
    .byte #ENEMY_TYPE_BAT
    ;.byte #>EnemyDefBat
    .byte #3
    .byte #ROOM_ITEM_NONE
RoomDef_12_10
    .byte #ROOM_WINGS
    .byte #COLOR_THEME_D1
    .byte #RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_GREENSLIME
    ;.byte #>EnemyDefGreenSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_11
    .byte #ROOM_OUTDOOR_CLIFF3
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_BUG
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_12_12
    .byte #ROOM_FOUR_DOORS
    .byte #COLOR_THEME_D1
    .byte #TOP_CLOSED|RIGHT_CLOSED|BOTTOM_CLOSED
    .byte #ENEMY_TYPE_REDBUG
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_KEY<<4|1

    
RoomDef_12_13
    .byte #ROOM_OUTDOOR_CLIFFTOP
    .byte #COLOR_THEME_OUTDOOR_ROCKS_1
    .byte #BOTTOM_CLOSED
    .byte #ENEMY_TYPE_BUG
    ;.byte #>EnemyDefBug
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_0
    .byte #ROOM_STARTING
    .byte #COLOR_THEME_D2
    .byte #RIGHT_CLOSED|TOP_CLOSED
    .byte #ENEMY_TYPE_GHOST
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_1
    .byte #ROOM_HOURGLASS
    .byte #COLOR_THEME_D2
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_2
    .byte #ROOM_WINGS
    .byte #COLOR_THEME_D2
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_GHOST
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_3
    .byte #ROOM_PLUS
    .byte #COLOR_THEME_DARK_CAVE
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_TRAP
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_4
    .byte #ROOM_UFO
    .byte #COLOR_THEME_D2
    .byte #BOTTOM_CLOSED|RIGHT_CLOSED|TOP_ENEMY
    .byte #ENEMY_TYPE_GREENTOADY
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_5
    .byte #ROOM_OUTDOOR_CLIFF2
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_JUMPER
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_6
    .byte #ROOM_PINES
    .byte #COLOR_THEME_TREES_1
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_SNAKE
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_7
    .byte #ROOM_HOURGLASS
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_BOULDER
    .byte #1
    .byte #ITEM_VERY_NONE<<4

    
RoomDef_13_8
    .byte #ROOM_TREES
    .byte #COLOR_THEME_TREES_2
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    .byte #0
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_9
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    .byte #0
    .byte #ITEM_ARMOR_UP<<4|5

    
RoomDef_13_10
    .byte #ROOM_HOURGLASS
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_BOULDER
    ;.byte #>EnemyDefSlime
    .byte #1
    .byte #ITEM_VERY_NONE<<4

    
RoomDef_13_11
    .byte #ROOM_LAKE
    .byte #COLOR_THEME_RIVER
    .byte #RIGHT_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #3
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_12
    .byte #ROOM_BLANK
    .byte #COLOR_THEME_OUTDOOR_ROCKS_2
    .byte #LEFT_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_FLYING_BAT
    ;.byte #>EnemyDefSlime
    .byte #2
    .byte #ROOM_ITEM_NONE

    
RoomDef_13_13
    .byte #ROOM_LAKE
    .byte #COLOR_THEME_RIVER
    .byte #BOTTOM_CLOSED|RIGHT_CLOSED
    .byte #ENEMY_TYPE_SLIME
    ;.byte #>EnemyDefSlime
    .byte #0
    .byte #ITEM_ARMOR_UP << 4|2

    
