    MAC KernelFirstLine
    SUBROUTINE
        ;LDA Enemy0SpriteBuffer
        STA GRP1
        LDA PlayerColorBuffer
        STA COLUP0
        ldy TempPF1
        lda (RoomColorPtr),Y
        sta COLUPF
        
        KernelDrawMissiles

        KernelDrawPlayer 

    ENDM

    MAC KernelFirstLineNom
    SUBROUTINE
        STA GRP1
        LDA PlayerColorBuffer
        STA COLUP0

        KernelDrawOnlyPlayerMissile
        KernelDrawPlayer 
    ENDM


    MAC KernelSecondLine
    SUBROUTINE
        KernelDrawBackgroundIndexed
        KernelDrawEnemy {1}
    ENDM


    MAC BurnScanlineAndHMove
    SUBROUTINE
        ;STA WSYNC   
        ;NLT -- REFP1 enemy facing
        lda Enemy0Dir+{1}+1
        sta REFP1
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        sta WSYNC
        LDA #$80			        ;2	prevent other things from moving on early hmove
        STA HMP0			        ;3
        STA HMM0			        ;3
        STA HMM1			        ;3
        STA HMBL			        ;3 

        StoreAllCollisions {1}      ;52

        LDA #0                      ;2   (prepare the next line to display a blank enemy)
        DEX                         ;2
        STA HMOVE                   ;3
    ENDM


    MAC KernelSection
    SUBROUTINE
.KernelTopBeginning
        STA WSYNC
        KernelFirstLine
        ;STA WSYNC
        KernelSecondLine {1}

        DEX 
        CPX KernelSwitch1+{1}
        bne .KernelTopBeginning
        ;BEQ .EndKernelTop
        ;JMP .KernelTopBeginning
.EndKernelTop
        CPX #KERNEL_BOTTOM_DOOR_CUTOFF
        BNE .EndSection
        ;beq .ReadyForKernelBottom
        ;jmp .EndSection
.ReadyForKernelBottom
        StoreEnemyCollisions {1}
        jmp KernelBottom
        ;jsr KernelBottom
        ;StoreAllCollisions {1}
        ;JMP EndOfKernelSplit
.EndSection
    ENDM


KernelBottom
    lda #0
    STA WSYNC
    KernelFirstLineNom

    txa             ;2
    lsr             ;2
    lsr             ;2
    tay             ;2  (8)

    LDA BottomDoorColor
    sta WSYNC
    STA COLUBK

    lda (LineDefA),Y    ;6
    ora SideDoorOrVal
    sta PF0             ;3
    lda (LineDefB),Y    ;6
    sta PF1             ;3

    lda (LineDefC),Y    ;6
    sta PF2             ;3  (27 + 8 = 35)
    sty TempPF1

    dex
    bne KernelBottom


    sta WSYNC
    StorePlayerCollisions

    sta CXCLR
    jmp EndOfKernelSplit

    MAC KernelReposition
    SUBROUTINE
        ;STA WSYNC
        ;KernelFirstLine
        KernelFirstLineNom
        STA HMCLR
        LDA Enemy0X+{1}
        ADC #7
        LDX #1
        PosObject2
        LDA #0
        ;PositionObjectWithoutJSR 1
        STA WSYNC

        LDX KernelSwitch1+{1}-1
        DEX
        KernelFirstLineNom
        BurnScanlineAndHMove {1}-1
    ENDM


    MAC KernelBatch
        KernelSection {1}
        KernelReposition {2}   ;then the repositioning logic
        ;now we do 1 more "section", but
        ;not as the macro, because we don't want 
        ;to WSYNC at the beginning
        KernelFirstLine     
        ;STA WSYNC
        KernelSecondLine {2}
        DEX 
        BNE .doneWithBatch
        JMP EndOfKernelSplit
.doneWithBatch
    ENDM


    MAC KernelDrawTopDoorLines
    SUBROUTINE
        ;This mess renders the top few lines
        ; differently to handle closed/locked
        ; doors at the top of the screen
        lda #3
        sta TempPF2
        lda #0
.topLoop
        STA WSYNC
        KernelFirstLine
        LDA #0
        STA GRP1
        DEX
        dec TempPF2
        bne .topLoop
        STA WSYNC


        KernelFirstLineNom
        ;KernelDrawBackgroundIndexed
        ;this is the same as the normal
        ;KenrelDrawBackgroundIndexed,
        ;except that it shoves the background-color
        ;update right into the middle of it
        ;to end the key display
        ;LDA RoomBGColor 
        ;STA COLUBK

        txa             ;2
        lsr             ;2
        lsr             ;2
        tay             ;2  (8)

        LDA RoomBGColor 
        STA COLUBK
        sta WSYNC

        lda (LineDefA),Y    ;6
        ora SideDoorOrVal
        sta PF0             ;3
        lda (LineDefB),Y    ;6
        sta PF1             ;3


        lda (LineDefC),Y    ;6
        sta PF2             ;3  (27 + 8 = 35)
        sty TempPF1

        ;SLEEP 34
        ldy #6 
.wasteLoop
        dey
        bne .wasteLoop
        lda RoomBGColor ;waste 3

  
        LDA RoomBGColor 
        STA COLUBK
   
        DEX
        nop

        LDA #0

    ENDM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SingleKernel
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Do HMOVE's after everything is all ready
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    sta WSYNC
    sta HMOVE

	sta WSYNC	
    sta HMCLR

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;Possibly an extra HMOVE for missiles
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    lda ExtraMissileMovement
    beq SkipExtraHMove
    sta HMM0
    lda #0
    sta ExtraMissileMovement

	sta WSYNC	
    sta HMOVE
	
SkipExtraHMove


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Clear everything out so we're ready to start drawing
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #0
    sta GRP0
    sta GRP1
    sta PF0
    sta PF1
    sta PF2
    sta COLUBK

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Wait for Vblank to be done....
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    sta WSYNC
    ;sta VBLANK
WaitForVblankEnd
	lda INTIM	
	bne WaitForVblankEnd	

	ldx #HALF_SCANLINES
	sta WSYNC	
	sta VBLANK  	
    sta CXCLR       ;clear collisions just before we draw

    JSR DrawHeader
    KernelPrepareMissiles
    LDX #MAIN_SCREEN_START
    ;NOP
    ;NOP
    ;NOP
    ;NOP
    ;NOP
    ;NOP
    ;NOP
    ;NOP
    ;KernelDrawBackgroundIndexed

    txa             ;2
    lsr             ;2
    lsr             ;2
    tay             ;2  (8)
    LDA TopDoorColor

    sta WSYNC


    STA COLUBK

    lda (LineDefA),Y    ;6
    ;ora #%00010000
    ora SideDoorOrVal
    sta PF0             ;3
    lda (LineDefB),Y    ;6
    sta PF1             ;3
    lda (LineDefC),Y    ;6
    sta PF2             ;3  (27 + 8 = 35)
    sty TempPF1



    LDA #0

    ;STA WSYNC


    ;Important! Accumulator needs to be 0
    ; going into main kernel loop
    LDX #MAIN_SCREEN_START ;first do the kernel top half

    KernelDrawTopDoorLines

    

    KernelBatch 0,1

    KernelBatch 1,2

    KernelSection 2
foobar
    StoreAllCollisions 2


EndOfKernelSplit
    STA CXCLR
    FixMissileKernelStackWeirdness
    ClearMissiles
    PlayerAfterKernel
    BankJump AfterKernel,BANK_1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    MAC SelectAndRunKernel
        JMP SingleKernel


    ENDM
