
InitRoom    SUBROUTINE
        ClearEnemies 
        SpawnRoomEnemies

        ;set the one flag2, clear all the others
        lda #MISC_FLAGS2_JUST_SWITCHED_ROOM
        sta MiscFlags2

        jsr BurnFrame
        jsr TrampolineFor_BankB_RoomPrep
        ;ClearPlayerLastPositions
        jsr BurnFrame
        ;SetFlag2 #MISC_FLAGS2_JUST_SWITCHED_ROOM
        ;ClearFlag2 #MISC_FLAGS2_HANDLED_ENEMIES_DEAD
        PrepareBackgroundIndexed
        jsr BurnFrame
        jsr SortEnemies
        jsr BurnFrame
        lda #30
        sta RoomTransitionTimer

    rts

LdaRoomDefYBank1Jump
    BankJump LdaRoomDefYBankBToBank1,BANK_B
Bank1Rts
    rts


SetEnemyPositionsFromSafeSpawns
    BankJump LoadPositionsFromSafeSpawnsImpl,BANK_B
    rts

;using a trampoline here just to make
;returning easier
TrampolineFor_BankB_RoomPrep
    BankJump BankB_RoomPrep,BANK_B
    rts

CheckAllDoorsSub
    CheckAllDoors
    rts
