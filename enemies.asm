
LdaEnemyDefYBank1Jump
    BankJump LdaEnemyDefYBankAToBank1,BANK_A


; This saves off the enemy's last
; position. Which we might not use
; because we really only want to hold
; on to their last position when visible.
; but we don't know at this point whether or
; not they are visible.
CopyEnemyPosToLastPos

    lda Enemy0X,X
    sta Enemy0LastX,X
    lda Enemy0Y,X
    sta Enemy0LastY,X

    rts

CopyEnemyPosToLastShown
    lda Enemy0LastX,X
    sta Enemy0LastShownX,X
    lda Enemy0LastY,X
    sta Enemy0LastShownY,X

CopyEnemyRts
    rts

SortEnemies
        ;a bubble sort with only 3 items only requires 3 steps?
    lda INTIM
    sbc #10
    bmi CopyEnemyRts
        CompareSwap 0,1
        CompareSwap 1,2
        CompareSwap 0,1
        rts

KernelFlagsForEnemyHidden
    .byte #%11100000
    .byte #%01100000
    .byte #%00100000

UpdateSingleEnemy    SUBROUTINE 
    lda INTIM
    sbc #15
    bmi CopyEnemyRts

    IfWallCollision SingleEnemyAfterCopy
    lda KernelMode
    and KernelFlagsForEnemyHidden,X
    beq SingleEnemyAfterCopy

    jsr CopyEnemyPosToLastPos

SingleEnemyAfterCopy
    lda Enemy0HP,X
    beq CopyEnemyRts ;.ContinueUpdatingEnemyJump
.ContinueUpdatingEnemyJump
    ;if offscreen, kill the enemy and go on
    lda Enemy0Y,X
    beq .ResetEnemy
    bpl .ContinueUpdatingEnemyJump2
    lda #MAIN_SCREEN_START
    sbc Enemy0Y,X
    bmi .ContinueUpdatingEnemyJump2
.ResetEnemy
    lda #0
    sta Enemy0HP,X
    rts

.ContinueUpdatingEnemyJump2
    ;if flying, clear the wall flags
    lda EnemyColor
    eor #ENEMY_COLOR_FLYING
    bne .ContinueUpdatingEnemyJump3
    lda EnemyCollisionFlags0,X
    and #~COL_FLAG_WALL
    sta EnemyCollisionFlags0,X

.ContinueUpdatingEnemyJump3


    BankJump RunEnemyUpdateFunc,BANK_A



;Incoming Registers:
;X - first enemy index
;Y - second enemy index
SwapEnemies
    SwapEnemyValues Enemy0X
    SwapEnemyValues Enemy0Y
    SwapEnemyValues Enemy0State0
    SwapEnemyValues Enemy0Dir
    SwapEnemyValues Enemy0HP
    SwapEnemyValues Enemy0LastX
    SwapEnemyValues Enemy0LastY
    SwapEnemyValues Enemy0LastShownX
    SwapEnemyValues Enemy0LastShownY
    rts
        
;Incoming registers:
;A - Y value of enemy to switch for
;X - Which KernelSwitch to set
SetKernelSwitch

    clc         ;we do the switcheroo 12 pixels above
    adc #4     ;this enemy's Y value

    ror         ;I'm not sure this is very efficient...
    ror         ;it's supposed to set the last 2 bits
    clc         ;to 01 to get the reposition out of sync
    rol         ;with changing the background?
    sec         ; (Wouldn't it be faster to OR #%00000001
    rol         ; then AND #%11111101 ?)

    sta KernelSwitch1,X

    rts


