    
    echo "------", [bitmap_00 - *]d, " bytes free before TitleGraphics"

    
    align 256
bitmap_00
StartTextA
PasswordTextA
StartTextF
	BYTE %00000000
	BYTE %00000000
	BYTE %00000000
	BYTE %00000000
	BYTE %00000000
	BYTE %00000000
	BYTE %00000000
	BYTE %00000000
	BYTE %00000000
TitleA
	BYTE %11000110
	BYTE %11000110
	BYTE %11100110
	BYTE %01100110
	BYTE %01100110
	BYTE %01100110
	BYTE %00110110
	BYTE %00110110
	BYTE %00110110
	BYTE %00111110
	BYTE %00111110
	BYTE %00110110
	BYTE %00110110
	BYTE %00110110
	BYTE %00110010
	BYTE %00011010
	BYTE %00011110
	BYTE %00001110
	BYTE %00001110
	BYTE %00001110
	BYTE %00001110
	BYTE %00000000

PasswordTextB
	BYTE %10000010
	BYTE %10000010
	BYTE %10000010
	BYTE %10000011
	BYTE %11110010
	BYTE %10010001
	BYTE %11110000
	BYTE %00000000
	BYTE %00000000
StartTextB
	BYTE %11100000
	BYTE %00010000
	BYTE %00010000
	BYTE %11110000
	BYTE %10000000
	BYTE %10000000
	BYTE %01110011
	BYTE %00000000
TitleB
	BYTE %10011101
	BYTE %10011101
	BYTE %10011101
	BYTE %10011101
	BYTE %10011100
	BYTE %10011100
	BYTE %10011100
	BYTE %10011100
	BYTE %10011100
	BYTE %10011100
	BYTE %10111100
	BYTE %10111100
	BYTE %11111100
	BYTE %11101100
	BYTE %11101100
	BYTE %11001100
	BYTE %11001100
	BYTE %11001101
	BYTE %11001101
	BYTE %10001101
	BYTE %10001101
	BYTE %00000000

PasswordTextC
	BYTE %01001110
	BYTE %01000001
	BYTE %01000001
	BYTE %11001111
	BYTE %01001000
	BYTE %01001000
	BYTE %11000111
	BYTE %00000000
StartTextC
	BYTE %10000100
	BYTE %10000100
	BYTE %10000100
	BYTE %10000111
	BYTE %10000100
	BYTE %10000010
	BYTE %11100001
	BYTE %00000000
TitleC
	BYTE %11110111
	BYTE %11111001
	BYTE %11111101
	BYTE %00011101
	BYTE %00001101
	BYTE %00001101
	BYTE %00001101
	BYTE %11111101
	BYTE %01111101
	BYTE %00000001
	BYTE %00000111
	BYTE %00000111
	BYTE %00000111
	BYTE %00000111
	BYTE %00000111
	BYTE %00000111
	BYTE %00000011
	BYTE %00001011
	BYTE %11111011
	BYTE %11111011
	BYTE %11110111
	BYTE %00000000

PasswordTextD
	BYTE %00111000
	BYTE %00000100
	BYTE %00000100
	BYTE %00111100
	BYTE %00100000
	BYTE %00100000
	BYTE %00011100
	BYTE %00000000
StartTextD
	BYTE %10010010
	BYTE %10010010
	BYTE %10011100
	BYTE %10010100
	BYTE %10010010
	BYTE %10010010
	BYTE %10011100
	BYTE %00000000
TitleD
	BYTE %11100110
	BYTE %11110010
	BYTE %11111010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011010
	BYTE %00011011
	BYTE %00011011
	BYTE %00011011
	BYTE %00011011
	BYTE %00011011
	BYTE %00111011
	BYTE %00000000

PasswordTextE
	BYTE %10001001
	BYTE %11011001
	BYTE %11111001
	BYTE %10101001
	BYTE %10001001
	BYTE %10001001
	BYTE %10001001
	BYTE %00000000
StartTextE
	BYTE %00010000
	BYTE %00010000
	BYTE %00010000
	BYTE %00010000
	BYTE %00010000
	BYTE %00010000
	BYTE %01111100
	BYTE %00000000
TitleE
	BYTE %00011101
	BYTE %00011101
	BYTE %00011101
	BYTE %00011101
	BYTE %00011101
	BYTE %00011101
	BYTE %00011101
	BYTE %00011101
	BYTE %00011101
	BYTE %00011101
	BYTE %00111101
	BYTE %00111100
	BYTE %01111100
	BYTE %11111100
	BYTE %11101100
	BYTE %11001100
	BYTE %10001100
	BYTE %10001100
	BYTE %10001110
	BYTE %00001110
	BYTE %00001110
	BYTE %00000000

PasswordTextF
	BYTE %11100000
	BYTE %00010000
	BYTE %00010000
	BYTE %00010000
	BYTE %00010000
	BYTE %00010000
	BYTE %11100000
	BYTE %00000000
TitleF
	BYTE %00011100
	BYTE %00011100
	BYTE %00011100
	BYTE %00011100
	BYTE %00011100
	BYTE %00011100
	BYTE %00011100
	BYTE %00011100
	BYTE %11111100
	BYTE %11111100
	BYTE %10001100
	BYTE %11001100
	BYTE %11001100
	BYTE %11001100
	BYTE %11001100
	BYTE %11001100
	BYTE %11001100
	BYTE %01101100
	BYTE %01101100
	BYTE %00111100
	BYTE %00111100
	BYTE %00000000


    ;.align 256
    .byte 0
    .byte 0
      ;

PDigit0
        .byte #%00000000
        .byte #%00111000
        .byte #%01000100
        .byte #%01000100
        .byte #%01000100
        .byte #%01000100
        .byte #%00111000
        .byte #%00000000

PDigit1

        .byte #%00000000
        .byte #%01111110
        .byte #%00011000
        .byte #%00011000
        .byte #%00011000
        .byte #%00111000
        .byte #%00011000
        .byte #%00000000
PDigit2
        .byte #%00000000
        .byte #%01111100
        .byte #%01100000
        .byte #%00111000
        .byte #%00000100
        .byte #%01100100
        .byte #%00111000
        .byte #%00000000

PDigit3
        .byte #%00000000
        .byte #%00111000
        .byte #%01100100
        .byte #%00001100
        .byte #%00001000
        .byte #%01100100
        .byte #%00111000
        .byte #%00000000
PDigit4
        .byte #%00000000
        .byte #%00000100
        .byte #%00000100
        .byte #%00111110
        .byte #%00010100
        .byte #%00001100
        .byte #%00000100
        .byte #%00000000
PDigit5
        .byte #%00000000
        .byte #%01111100
        .byte #%00000010
        .byte #%01111100
        .byte #%01000000
        .byte #%01000000
        .byte #%01111100
        .byte #%00000000

PDigit6
        .byte #%00000000
        .byte #%00111100
        .byte #%01000010
        .byte #%01111100
        .byte #%01000000
        .byte #%01000000
        .byte #%00111100
        .byte #%00000000
        
PDigit7
        .byte #%00000000
        .byte #%00010000
        .byte #%00010000
        .byte #%00010000
        .byte #%00001000
        .byte #%00000100
        .byte #%01111100
        .byte #%00000000
PDigit8
        .byte #%00000000
        .byte #%00111100
        .byte #%01000010
        .byte #%01000010
        .byte #%00111100
        .byte #%01000010
        .byte #%00111100
        .byte #%00000000
PDigit9
        .byte #%00000000
        .byte #%00111000
        .byte #%00000100
        .byte #%00000100
        .byte #%00111100
        .byte #%01000100
        .byte #%00111000
        .byte #%00000000
PDigitA 
        .byte #%00000000
        .byte #%01000100
        .byte #%01000100
        .byte #%01000100
        .byte #%01111100
        .byte #%01000100
        .byte #%00111000
        .byte #%00000000
PDigitB 
        .byte #%00000000
        .byte #%01111000
        .byte #%01000100
        .byte #%01000100
        .byte #%01111000
        .byte #%01000100
        .byte #%01111000
        .byte #%00000000
PDigitC 
        .byte #%00000000
        .byte #%00111000
        .byte #%01000100
        .byte #%01000000
        .byte #%01000000
        .byte #%01000100
        .byte #%00111000
        .byte #%00000000
PDigitD 
        .byte #%00000000
        .byte #%01111000
        .byte #%01000100
        .byte #%01000100
        .byte #%01000100
        .byte #%01000100
        .byte #%01111000
        .byte #%00000000
PDigitE 
        .byte #%00000000
        .byte #%01111100
        .byte #%01000000
        .byte #%01000000
        .byte #%01110000
        .byte #%01000000
        .byte #%01111100
        .byte #%00000000
PDigitF 
        .byte #%00000000
        .byte #%01000000
        .byte #%01000000
        .byte #%01000000
        .byte #%01110000
        .byte #%01000000
        .byte #%01111100
        .byte #%00000000
        .byte #%00000000

