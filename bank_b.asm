
    ORG $B000
    RORG $F000

CURRENT_BANK eqm BANK_B

    include allBanks.asm
    include roomDefs.asm
    include allRooms.asm
    include password.asm


LdaRoomDefYBankBToBank1
    lda (RoomDef),Y
    BankJumpSaveA Bank1Rts,BANK_1




    include title.asm


BurnFrameB SUBROUTINE

.WaitVblankEnd
        lda INTIM	
        bne .WaitVblankEnd
        ldx #HALF_SCANLINES*2+33
        sta WSYNC
        lda #$0F
        sta VBLANK
.Repeat
        sta WSYNC
        nop
        dex
        bne .Repeat

        lda #2 
        sta WSYNC	
        sta VSYNC	
        sta WSYNC 	
        sta WSYNC	
        lda #43	
        sta TIM64T	
        lda #0
        sta WSYNC
        sta VSYNC 	
        ;sta VBLANK
        rts

FirstHalfOfRoomPrep
        ldy #ROOMDEF_COLOR
        lda (RoomDef),Y
        clc
        rol
        rol
        tax
    ClearFlag #MISC_FLAGS_BALL_WALL_FAKE
    ;rts
    jmp SecondHalfOfRoomPrep

    include titleGraphics.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Bank B is room definitions, and 
; code that operates directly with
; roomdefs (at this point, just safe spawn loading)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PasswordAfterTitleUpdate SUBROUTINE

   lda     #JOY_LEFT
    bit     SWCHA
    bne     .notLeft
    dec     PasswordChar
    dec     PasswordX
    bmi     .wrapLeft
    jmp     .notLeft
.wrapLeft
    lda     #5
    sta     PasswordX
    dec     PasswordY
    bmi     .wrapUp
    jmp     .notLeft
.wrapUp
    lda     #2
    sta     PasswordY
    lda     #17
    sta     PasswordChar

.notLeft

    lda     #JOY_RIGHT
    bit     SWCHA
    bne     .notRight
    inc     PasswordChar
    inc     PasswordX
    lda     PasswordX
    cmp     #6
    beq     .wrapRight
    jmp     .notRight
.wrapRight
    lda     #0
    sta     PasswordX
    inc     PasswordY
    lda     #3
    cmp     PasswordY
    beq     .wrapDown
    jmp     .notRight
.wrapDown
    lda     #0
    sta     PasswordY
    sta     PasswordChar

.notRight


    lda     PasswordChar
    clc
    lsr
    tax

    lda     #JOY_UP
    bit     SWCHA
    bne     .notUp
    lda     #1
    and     PasswordChar
    beq     .incByOne
.upBy16
    lda     PlayerMaxHP,X
    clc
    adc     #16
    sta     PlayerMaxHP,X
    jmp     .notUp
.incByOne
    inc     PlayerMaxHP,X
    lda     PlayerMaxHP,X
    and     #$0F
    bne     .notUp
    jmp     .downBy16
.notUp


    lda     #JOY_DOWN
    bit     SWCHA
    bne     .notDown
    lda     #1
    and     PasswordChar
    beq     .decByOne
.downBy16
    lda     PlayerMaxHP,X
    sec
    sbc     #16
    sta     PlayerMaxHP,X
    jmp     .notDown
.decByOne
    dec     PlayerMaxHP,X
    lda     PlayerMaxHP,X
    and     #$0F
    cmp     #$0F
    bne     .notDown
    ;up by 16
    lda     PlayerMaxHP,X
    clc
    adc     #16
    sta     PlayerMaxHP,X
.notDown



AfterPasswordControls
    lda     SWCHA
    eor     #$FF
    bne     .setScrollTimer
    sta     PasswordScrollTimer
    jmp     PasswordBackToMain

.setScrollTimer
    lda     PasswordScrollTimer
    bne     .done
    lda     #15
    sta     PasswordScrollTimer
.done
    jmp     PasswordBackToMain




LoadPositionsFromSafeSpawnsImpl
    SUBROUTINE
    ;TempPF2 should be the number of enemies we want to spawn
    ;dex
    ;stx TempPF2 ;TempPF2 holds the number of enemies we want to spawn - 1 (index of the enemy spawning now)
    dec TempPF2

    ;load the room index
    ldy #0
    lda (RoomDef),Y
    tax
    ;x now has room index

    ;Store #1 in TempPF1 to bit against..
    lda #1
    sta TempPF1

    ;get random number between 0 and 7
    jsr GetRandomNumber
    ;lda Rand
    and #%00000111
    tay
    iny

    ;a has safe spawns
    lda SafeSpawns,X

    ;rotate the safe spawn based on the random number
    sty Temp
    ldx Temp

.keepRotating
    cmp #$80
    rol
    dex
    bne .keepRotating

    ldx Temp
    ;now work our way through seeing if it's safe
.checkSpawnLoop

    ;if it's safe, go to useThisValue (of x)
    bit TempPF1
    bne .useThisValue

.beforeRotation
    ;if not, rotate again, increase X
    cmp #$80
    rol

    inx

    ; if X equals start, fail. What then?
    ;cpx Temp
    ;beq .useThisValue

    ; if X equals 8, set to 0
    cpx #8
    bne .checkSpawnLoop
    ldx #0
    jmp .checkSpawnLoop


.useThisValue
    pha
    ldy TempPF2

    ; otherwise, look up X and Y in table
    lda SpawnsX,X
    sta Enemy0X,Y
    sta Enemy0LastX,Y
    sta Enemy0LastShownX,Y

    lda SpawnsY,X
    sta Enemy0Y,Y
    sta Enemy0LastY,Y
    sta Enemy0LastShownY,Y
    pla

    dec TempPF2
    bmi .allDone

    jmp .beforeRotation
.allDone

    BankJump Bank1Rts,BANK_1


BankB_PickUpItem SUBROUTINE
        ;don't pick up if within timer time.....
        lda ITEM_PICKUP_TIMER
        beq .AfterDeadTimerCheck
        jmp EndOfPickUpItem
.AfterDeadTimerCheck


        PlaySound #SOUND_GET_ITEM
        ClearFlag #MISC_FLAGS_ITEM_SHOWING
        SetFlag #MISC_FLAGS_ITEM_GOTTEN
        lda #0
        sta Enemy0Y
        sta Enemy0Y
        sta Enemy0HP

        AddItemToInventory

EndOfPickUpItem
        BankJump AfterPickupItem,BANK_1



BankB_RoomPrep SUBROUTINE
        jmp FirstHalfOfRoomPrep
SecondHalfOfRoomPrep
        ;ClearFlag #MISC_FLAGS_BALL_WALL_FAKE

        ;first get the room colors

        lda ColorDefStart,X
        sta RoomBGColor
        inx
        lda ColorDefStart,X
        sta RoomFGColor
        inx
        lda ColorDefStart,X
        sta RoomColorPtr
        inx
        lda ColorDefStart,X
        sta RoomColorPtr+1

        ;then set bg to reflect
        lda #BG_REFLECT
        sta CTRLPF ; reflect background

        ;then see if we need to update respawns
        ldx #5
.loop
        lda MapPosition
        eor Respawns,X
        beq .foundSpawn
        dex
        bne .loop
        jmp .skipUpdateRespawn
.foundSpawn
        lda MapPosition
        sta Respawn
.skipUpdateRespawn

        lda Inventory
        and #(1<<ITEM_LANTERN)
        bne .notDark

        lda RoomFGColor
        sec
        cmp #0
        bne .notDark
        sta RoomBGColor
        SetFlag2 #MISC_FLAGS2_DARK
        jmp .done

.notDark
        ClearFlag2 #MISC_FLAGS2_DARK

.done
        ;BankJump MidPrepBurn,BANK_1
;ReadyForDoubleIndex

        PrepareBackgroundDoubleIndexed


        BankJump Bank1Rts,BANK_1
 

    echo "------", [$FFFC - *]d, " bytes free in bank b"

	ORG $BFFC
	RORG $FFFC
	.word Start
	.word Start



