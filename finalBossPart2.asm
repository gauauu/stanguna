
.upDownInvincible

    BankJump UpDownInvincible,BANK_A



FinalBossPart2 

.LeftFrame



    ;first up or down movement
    lda Enemy0State0
    and #DIR_Y_UP
    beq .notUp
.Up
    inc Enemy0Y
    lda Enemy0Y
    ;adc #1
    ;sta Enemy0Y
    cmp #83
    bcc .upDownInvincible
    lda Enemy0State0
    and #DIR_X_RIGHT|DIR_X_LEFT
    sta Enemy0State0
    lda Enemy0Dir
    and #~FLAG_ALREADY_HIT
    sta Enemy0Dir
    ;jmp .upDownInvincible
    dec Enemy2State0       ;when back to top, mark it for delay
    bne BossCompletelyDone ;optimized jmp

.notUp
    lda Enemy0State0
    and #DIR_Y_DOWN
    beq .notDown
.Down
    lda Enemy0Y
    sbc #2
    sta Enemy0Y
    sbc #30
    bpl .upDownInvincible
    lda Enemy0State0
    and #DIR_X_RIGHT|DIR_X_LEFT
    ora #DIR_Y_UP
    sta Enemy0State0
    bne .upDownInvincible;optimized jmp

.notDown


.Move

    ;now horizontal movment
    lda #87
    sta Enemy0Y

    lda Enemy0State0
    beq .left
    and #DIR_X_LEFT
    beq .notLeft
.left
    lda Enemy0X
    sbc #2
    sta Enemy0X
    sbc #5
    bpl .bossMoveDone
    lda #DIR_X_RIGHT
    sta Enemy0State0
    bne .bossMoveDone;optimized jmp

.notLeft
    lda Enemy0State0
    and #DIR_X_RIGHT
    beq .notRight
.right
    lda Enemy0X
    adc #2
    sta Enemy0X
    cmp #90
    bcc .bossMoveDone
    lda #DIR_X_LEFT
    sta Enemy0State0
    ;jmp .bossMoveDone

.notRight
.bossMoveDone
    ;lda Enemy0X
    ;sbc #10
    ;bpl .normalCheck
    ;lda PlayerX
    ;sbc #10
    ;bpl .normalCheck
    ;jmp .dive

.normalCheck
    lda Enemy0X
    adc #30
    sbc PlayerX
    AbsoluteValue
    sbc #30
    bpl .notDive

.dive
    lda Enemy0State0
    ora #DIR_Y_DOWN
    sta Enemy0State0

.notDive
BossCompletelyDone

    BankJump GhostDone,BANK_A

