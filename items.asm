ItemColors 
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$14
        .byte #$24  ;7 - hp up, reddish brown
        .byte #$44  ;8 - armor up
        .byte #$06  ;9 - power up - gray
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$44
        .byte #$44

SpawnItem   SUBROUTINE
        lda #ENEMY_SIZE_NORMAL
        sta EnemyScaleNuSiz
        lda #0
        sta REFP1
        sta Enemy0Dir

        ;Spawn the item in the safe spawn
        ldx #1
        stx TempPF2
        jsr SetEnemyPositionsFromSafeSpawns


        ;not 100% if we'll need to do this 
        ;but playing safe for now
        ;so nothing breaks
        lda #<ItemEnemyDef
        sta EnemyDef
        lda #>ItemEnemyDef
        sta EnemyDef+1

        
        SetColorForItemDisplay
        
        SetFlag #MISC_FLAGS_ITEM_SHOWING

        lda #ITEM_SPAWN_DEAD_TIME
        sta ITEM_PICKUP_TIMER
                    
        rts

UpdateEnemies_Item SUBROUTINE
        lda #8
        sta EnemyHeight

        lda Enemy0State0

        LoadGraphicByOffset EnemySpriteTable,ItemGfx

        ;decrement "dont pick up" counter
        lda ITEM_PICKUP_TIMER
        beq .SkipDecrement
        dec ITEM_PICKUP_TIMER

       
.SkipDecrement       
        ; update kernel mode
        SetKernelMode #KERNEL_MODE_1
ItemUpdate ;this just points to any random rts, not related to UpdateEnemies
        rts

;ItemUpdate     ;moved above to optimize space
        ;rts




ItemEnemyDef
    .byte #<Digit0 ;we won't actually use this
    .byte #>Digit0
    .byte #<Digit0 
    .byte #>Digit0
    .byte #8
    .byte #$86
    .byte #<ItemUpdate
    .byte #>ItemUpdate
    .byte #ENEMY_HP_INVINCIBLE
    .byte #ENEMY_DMG_NONE
    .byte 0
    
