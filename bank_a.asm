    ORG $C000
    RORG $F000

CURRENT_BANK eqm BANK_A

    include allBanks.asm
    include enemyDefs.asm
    include enemies_a.asm
    include saveKey.asm


EnemySwitchUpdate
    lda KeysOwned
    and #%10000000
    beq .nextColor
    lda #$1E
    Skip2Bytes
.nextColor
    lda #$0B
    sta EnemyColor
    jmp EnemySwitchUpdate2 

    include subscreen.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;
; more squeezing code into random places
;;;;;;;;;;;;;;;;;;;;;;;;;;


EnemySwitchUpdate2 SUBROUTINE
    
    inc EnemyAnimCounter ;don't animate

    ;check if hit
    lda Enemy0HP,X 
    cmp #50
    beq .done ;if not, do nothing

    ;if so, reset health
    lda #50
    sta Enemy0HP,X
    ;change animation
    lda #1
    sta EnemyAnimCounter
    ;toggle key state
    lda KeysOwned
    eor #%11000000
    sta KeysOwned

.done
    EndEnemyFunc


       
EnemyCaveUpdate SUBROUTINE

        lda PlayerY
        sbc #78
        bmi .done
        ;either reposition or 
        ;change rooms
        lda Enemy0State0,X
        bne .warp
        lda #62
        sta PlayerY
        sta PlayerLastY
        bne .done ;optimized jmp
.warp
        SetFlag2 #MISC_FLAGS2_WARP_UP
.done
        sta Enemy0State0,X

        ldy #88
        ;sta Enemy0Y,X
        lda #71
        ;sta Enemy0X,X
        jmp SetEnemyPosition


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    include subscreenGfx.asm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


PrepLoad    
    jsr LoadGame
    lda #0
    sta RoomDef
    BankJump AfterLoad,BANK_1


    MAC DeactivateMissileIfOutOfBounds
    SUBROUTINE
        lda PlayerMissileX
        ;;;new
        sbc #148
        bcs .deactivate
        ;;;new(end)
        ;bmi .deactivate
        lda PlayerMissileY
        bmi .deactivate
        sbc #MAX_Y
        bpl .deactivate
        jmp .done
.deactivate
        lda MiscFlags2
        and #~MISC_FLAGS2_ARROW_ACTIVE
        sta MiscFlags2
        ClearEnemyAlreadyHitFlags
        lda #%00000010
        sta RESMP0
.done
    ENDM

    MAC MoveMissileX
    SUBROUTINE
        lda PlayerMissileDir
        and #~DIR_X_MASK
        beq .done
        and #DIR_X_RIGHT
        beq .moveArrowLeft
.moveArrowRight
        lda #HMOVE_RIGHT_2
        sta ExtraMissileMovement
        inc PlayerMissileX
        inc PlayerMissileX
        inc PlayerMissileX
        inc PlayerMissileX
        jmp .done
.moveArrowLeft
        lda #HMOVE_LEFT_2
        dec PlayerMissileX
        dec PlayerMissileX
        dec PlayerMissileX
        dec PlayerMissileX
        sta ExtraMissileMovement
        jmp .done
.done
    ENDM

    MAC MoveMissileY
    SUBROUTINE
        lda PlayerMissileDir
        and #~DIR_Y_MASK
        beq .done
        and #DIR_Y_UP
        beq .moveArrowUp
.moveArrowDown
        inc PlayerMissileY
        inc PlayerMissileY
        inc PlayerMissileEndY
        inc PlayerMissileEndY
        jmp .done
.moveArrowUp
        dec PlayerMissileY
        dec PlayerMissileY
        dec PlayerMissileEndY
        dec PlayerMissileEndY
        jmp .done
.done

    ENDM

    MAC SwapEnemyVal
        lda Enemy0{3}+{1}
        ldy Enemy0{3}+{2}
        sta Enemy0{3}+{2}
        sty Enemy0{3}+{1}
    ENDM

    MAC SwapEnemyVals
        SwapEnemyVal {1},{2},X
        SwapEnemyVal {1},{2},Y
        SwapEnemyVal {1},{2},State0
        SwapEnemyVal {1},{2},Dir
        SwapEnemyVal {1},{2},HP
        SwapEnemyVal {1},{2},LastX
        SwapEnemyVal {1},{2},LastY
        SwapEnemyVal {1},{2},LastShownX
        SwapEnemyVal {1},{2},LastShownY
    ENDM

SwapEnemy01
    SwapEnemyVals 0,1
    BankRts
SwapEnemy02
    SwapEnemyVals 0,2
    BankRts
SwapEnemy0112
    SwapEnemyVals 0,1
SwapEnemy12
    SwapEnemyVals 1,2
    BankRts


    

UpdateArrowImpl
SUBROUTINE
    lda #MISC_FLAGS2_ARROW_ACTIVE
    bit MiscFlags2
    bne .continue
    BankJump ReturnFromArrowImpl,BANK_1
.continue

    DeactivateMissileIfOutOfBounds
    MoveMissileX
    MoveMissileY
    BankJump ReturnFromArrowImpl,BANK_1
;;;;




PrepareHeader
    ;SUBROUTINE

    lda #NUSIZ_COPIES_3_CLOSE    ; arranging inventory like
    sta NUSIZ1                  ; 1 10

    ;lda NUSIZ_COPIES_1         (nusiz0 should already be set to 
    ;sta NUSIZ0                 this, but with missle size
                                ;already set up for the sword/etc)
    
                    ; use TempPF1 and TempPF2 to store PF1 and PF2
    lda #0          ; clear them in case we end up skipping everything
    sta TempPF0
    sta TempPF1
    sta TempPF2
    sta ENABL
    sta REFP1

    lda PlayerHP    ;first load the HP and divide by 2
    clc             ; (1st atTempPF1t: 2 hp per background pixel)
    ror
    beq .PrepareHPDone  ;if 0, skip to the end

    cmp #5
    bpl .DoMoreThanFourHP
    ldy #0
    ;sty TempPF0
    ;sty TempPF1
    ;sty TempPF2
    tax
    lda #0
.BitSetterLoop0
    sec
    rol
    dex
    bne .BitSetterLoop0
    clc
    rol
    clc
    rol
    clc
    rol
    clc
    rol
    sta TempPF0
    jmp .PrepareHPDone

.DoMoreThanFourHP
    ldy #$FF
    sty TempPF0

    sbc #4

    cmp #9          ; compare to 8 (8 pixels for each register)
    bpl .DoMoreThanEightHP

    tax             ; if less than 8....
    lda #0          ; then PF2 will be blank
    sta TempPF2
.BitSetterLoop1
    sec             ; then rotate bits into place for PF1
    ror
    dex
    bne .BitSetterLoop1
    sta TempPF1
    jmp .PrepareHPDone


.DoMoreThanEightHP
    sbc #8
    tax
    lda #0
.BitSetterLoop2
    sec
    rol
    dex
    bne .BitSetterLoop2
    sta TempPF2

    lda #%11111111
    sta TempPF1


.PrepareHPDone

    ;now prepare digits for inventory display, y
    ; has the number we're trying to display
    ;ldx #ITEM_ARROWS
    ;lda Inventory,x
    lda NumArrows
    beq .HideArrowQty
    jmp .PrepArrowQty
.HideArrowQty
    lda #<None
    sta DisplayDigit2
    sta DisplayDigit1
    sta DisplayDigit0
    lda #>None
    sta DisplayDigit2+1
    sta DisplayDigit1+1
    sta DisplayDigit0+1
    jmp .DonePrepHeader

.PrepArrowQty
    tay

    
    ;isolate first digit and multiply by 8
    and #%00001111
    clc
    rol
    rol
    rol

    
    ;add the result # of bytes to the address of gfx 0
    ;and save it to the temp var for displaying digit 1
    clc
    adc #<Digit0
    sta DisplayDigit2
    lda #>Digit0
    adc #0
    sta DisplayDigit2+1

    ;now work on the 2nd digit
    tya
    and #%11110000
    clc
    ror

    ;add the result # of bytes to the address of gfx 0
    ;and save it to the temp var for displaying digit 2 
    clc
    adc #<Digit0
    sta DisplayDigit1
    lda #>Digit0
    adc #0
    sta DisplayDigit1+1

    ;x still has the current inventory item,
    ;so load the gfx for it into "digit 0"
    ;txa
    ;clc
    ;rol
    ;rol
    ;rol
    lda #<GfxArrow
    sta DisplayDigit0
    lda #>GfxArrow
    ;adc #0
    sta DisplayDigit0+1
    
.DonePrepHeader
    BankJump AfterPrepareHeader,BANK_1
    ;ENDM

;;;;;;;;;;;;;;;;;;;
;;ENEMY_DEFS
;logically these should be in 
;enemyDefs.asm, but I ran out
;of space there, now shoving them
;in any free space I can find
;;;;;;;;;;;;;;;;;;;;;;;


EnemyBoss3Update SUBROUTINE

    lda Enemy0Dir,X
    bne .noInit
    lda #DIR_X_RIGHT
    sta Enemy0Dir,X
.noInit
    lda #80
    sta Enemy0Y,X
    jsr MoveEnemyXByDirection
    jsr MoveEnemyXByDirection
    jsr ShootAtPlayer
    ;lda EnemyColor
    ;cmp #ENEMY_COLOR_MAINBOSS
    ;bne .done
    ;jsr FinalBossUpdate
.done
    jmp GeneralEnemyDone

EnemyBoulderUpdate SUBROUTINE
        ldy #50
        lda #71
        jmp SetEnemyPosition


ShootAtPlayer SUBROUTINE
    lda Enemy0Y,X
    sbc PlayerY
    tay

    lda Enemy0X,X
    sbc PlayerX
    adc #10
    bmi .playerRight
    sbc #20
    bpl .playerLeft

    tya
    bmi .straightUp

    lda #DIR_Y_DOWN
    Skip2Bytes
.straightUp
    lda #DIR_Y_UP
    jmp .okShoot

.playerRight
    tya
    adc #10
    bmi .playerUpRight
    sbc #20
    bpl .playerDownRight
    lda #DIR_X_RIGHT
    Skip2Bytes
.playerDownRight
    lda #DIR_X_RIGHT|DIR_Y_DOWN
    Skip2Bytes
.playerUpRight
    lda #DIR_X_RIGHT|DIR_Y_UP
    jmp .okShoot

.playerLeft
    tya
    adc #10
    bmi .playerUpLeft
    sbc #20
    bpl .playerDownLeft
    lda #DIR_X_LEFT
    Skip2Bytes
.playerDownLeft
    lda #DIR_X_LEFT|DIR_Y_DOWN
    Skip2Bytes
.playerUpLeft
    lda #DIR_X_LEFT|DIR_Y_UP
.okShoot
    jmp EnemyUpdateMaybeSpawnBulletWithDir


MainBossDelay SUBROUTINE
    dec Enemy2State0
    lda Enemy2State0
    cmp #235
    bne .finished
    lda #0
    sta Enemy2State0
.finished
    jmp JumpBank_1


    

    echo "------", [$FFFC - *]d, " bytes free in bank a"
	ORG $CFFC
	RORG $FFFC
	.word Start
	.word Start

