
HurtSelectedEnemy SUBROUTINE
    LDA Enemy0Dir,X
    AND #FLAG_ALREADY_HIT
    BNE DoneHurtingSelectedEnemy

    LDA Enemy0Dir,X
    ORA #FLAG_ALREADY_HIT
    STA Enemy0Dir,X

    LDA Enemy0HP,X
    cmp #ENEMY_HP_INVINCIBLE
    beq DoneHurtingSelectedEnemy

    cmp #ENEMY_HP_DYNAMITE
    bne AfterDynamiteChecks
    lda #%0001000 ;#ITEM_DYNAMITE
    and Inventory
    beq DoneHurtingSelectedEnemy
    PlaySound #SOUND_ENEMY_HURT
    jmp KillEnemy


AfterDynamiteChecks
    PlaySound #SOUND_ENEMY_HURT

    ;don't bounce flying enemies
    ;lda EnemyColor
    ;cmp #ENEMY_COLOR_FLYING
    ;beq .afterBounce

    lda PlayerFacing
    cmp #JOY_LEFT
    bne .notLeft
    dec Enemy0X,X
    dec Enemy0X,X
    ;jmp .afterBounce
    bne .afterBounce ;optimized jmp
.notLeft
    cmp #JOY_RIGHT
    bne .notRight
    inc Enemy0X,X
    inc Enemy0X,X
    ;jmp .afterBounce
    bne .afterBounce ;optimized jmp
.notRight
    cmp #JOY_UP
    bne .notUp
    inc Enemy0Y,X
    inc Enemy0Y,X
    ;jmp .afterBounce
    bne .afterBounce ;optimized jmp
.notUp
    dec Enemy0Y,X
    dec Enemy0Y,X
    

.afterBounce

    lda Enemy0HP,X
    sec
    sbc PlayerPower
    sta Enemy0HP,X
    
    ;DEC Enemy0HP,X
    BEQ KillEnemyWithExp
    BMI KillEnemyWithExp
    ;JMP DoneHurtingSelectedEnemy
    bpl DoneHurtingSelectedEnemy
KillEnemyWithExp
    ldy #ENEMY_DEF_DMG
    LdaEnemyDefY
    sta Temp
    ldy #ENEMY_DEF_HP
    LdaEnemyDefY
    clc
    adc Temp
    ror
    adc Experience
    sta Experience
    jsr CheckForLevelUp
    
KillEnemy
    LDA #0        ;move them offscreen
    STA Enemy0Y,X
    STA Enemy0HP,X
DoneHurtingSelectedEnemy
    RTS


    
