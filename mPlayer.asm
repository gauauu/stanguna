;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Player-related constants
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PLAYER_HEIGHT equ 13
PLAYER_COLOR_CHANGE equ 9
PLAYER_MAIN_COLOR equ $08
PLAYER_COLOR_2 equ $46

PLAYER_ATTACK_TIMER_AMT equ 20
PLAYER_ATTACK_TIMER_AMT_OFF equ 10

PLAYER_FADE_TIME equ 60

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; Initialize the player (will have to redo this once we start switching rooms
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MAC NewGame
        SUBROUTINE
            lda #$A7 ;normal start
            ;lda #$2D ;boss of d3
            ;lda #$AA ;first crocky in d1
            ;lda #$8B ;outside of d1
            ;lda #$55 ; outside d4, castle
            ;lda #$8A ; d1 crocky
            ;lda #$75
            ;lda #$DB
            ;lda #$47 ; just beside wasteland
            ;lda #$29 ; lava in d3
            ;lda #$46
            ;lda #$c7 ;arrows room
            ;lda #$54 ; d4 entrance
            ;lda #$73 ;mid-d4
            ;lda #$10 ; right before ending
            ;lda #$00 ;main boss
            ;lda #$C3 ; d2 orange slimes
            ;lda #$78 ; below glitched outdoor room w/ monkeys
            ;lda #$1A ; skeleon room in d3
            ;lda #$3A ; switch in d3
            ;lda #$40 ; ring room
            ;lda #$16 ; boulder in top leftish
            ;lda #$DC ; bottom right for testing all room data
            sta Respawn

            lda #8
            sta PlayerMaxHP

            lda #1
            ;lda #2

            sta PlayerPower
            sta PlayerArmor
            lda #%01000000
            sta KeysOwned
        ENDM

        MAC InitPlayer
        SUBROUTINE


            lda #80
            sta PlayerY	;Initial Y Position

            lda #60 ; intitial X position
            sta PlayerX

            lda PlayerMaxHP
            sta PlayerHP

            lda #0
            sta REFP0
            sta NUSIZ0
            sta VDELP1

            ;sta Experience
            

            ;initialize animation
            sta PlayerSpriteAnimFrame
            lda #ANIMATION_TIME
            sta PlayerSpriteAnimCtr

            PrepareSpriteLookupTable PlayerFrame0,PlayerAnimTable ; in generalMACros.h

            sta GRP0
        ENDM

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; Prepare the player for the kernel
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MAC PreparePlayer
        SUBROUTINE
            lda #%00000001
            sta VDELP0
            lda #PLAYER_MAIN_COLOR
            sta PlayerColorBuffer

            CopyPointer PlayerColor,PlayerColorDef

            lda PlayerFaded             ; if player is faded, every other frame,
            and #1                      ; hide the player by moving 100 pixels Y
            beq .DonePreparePlayer
            CopyPointer BunchOfGray,PlayerColorDef
.DonePreparePlayer
        ENDM

        MAC PlayerAfterKernel
        SUBROUTINE
            lda PlayerFaded             ;decrement the faded counter and reset hidden player
            beq .DonePlayerAfterKernel
            dec PlayerFaded
.DonePlayerAfterKernel
        ENDM

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; Player Drawing routine in the kernel
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MAC KernelDrawPlayer
        SUBROUTINE

            ;SkipDraw PlayerY,PLAYER_HEIGHT,PlayerAnimTable,GRP0
            ;based on skipdraw, but with colors as well
            txa
            sec
            sbc PlayerY
            adc #PLAYER_HEIGHT
            bcc .skipPlayer
            tay
            lda (PlayerAnimTable),Y
            sta GRP0
            lda (PlayerColorDef),Y
            sta PlayerColorBuffer
            jmp .FinishPlayer

            ;CPY #PLAYER_COLOR_CHANGE ;check whether we need to change color here
            ;BNE .FinishPlayer
            ;LDA #PLAYER_COLOR_2     ;if so, change color
            ;STA PlayerColorBuffer
.skipPlayer
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop

.FinishPlayer


        ENDM


        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; Process controls, move based on controls, mirror player
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MAC PlayerControls
        SUBROUTINE

            ;LDA FrameCtr
            ;AND #1
            ;BNE .SkipUpdatingLastPlayerPosition
            
            IfFlag2Set #MISC_FLAGS2_JUST_SWITCHED_ROOM,SkipUpdatingLastPlayerPosition
            lda PlayerX             ;save our last values for collision detection
            sta PlayerLastX
            lda PlayerY
            sta PlayerLastY

SkipUpdatingLastPlayerPosition
            ClearFlag2 #MISC_FLAGS2_JUST_SWITCHED_ROOM


            lda PlayerAttackTimer       ;if we're already attacking, can't do anything
            beq HandleControls          
            dec PlayerAttackTimer
            cmp #PLAYER_ATTACK_TIMER_AMT_OFF
            beq JumpToHideMissile
            jmp PlayerControlsDone
JumpToHideMissile
            jmp CheckForShootBow
 

HandleControls

            ;LDA #0                     ;if A wasn't 0, then we fell through. 
            sta PlayerIsMoving
        
            ;ldx #01
            ldx #0	        ;assume 0 for horizontal speed

            lda PlayerCollisionFlags
            and #COL_FLAG_WALL
            beq SkipFrameCtrFirstCheck
            lda FrameCtr
            and #1
            beq SkipMoveDown
SkipFrameCtrFirstCheck

            lda #JOY_UP	
            bit SWCHA 
            bne SkipMoveUp
            sta PlayerIsMoving
            sta PlayerFacing
            inc PlayerY
SkipMoveUp

            lda #JOY_DOWN	
            bit SWCHA 
            bne SkipMoveDown
            sta PlayerIsMoving
            sta PlayerFacing
            dec PlayerY
SkipMoveDown

            ; for left and right, we're gonna 
            ; set the horizontal speed, and then do
            ; a single HMOVE.  We'll use X to hold the
            ; horizontal speed, then store it in the 
            ; appropriate register


            lda PlayerCollisionFlags
            and #COL_FLAG_WALL
            beq SkipFrameCtrSecondCheck
            lda FrameCtr
            and #1
            bne SkipMoveRight

SkipFrameCtrSecondCheck
            

            lda #JOY_LEFT   ;Left?
            bit SWCHA 
            bne SkipMoveLeft
            sta PlayerIsMoving
            sta PlayerFacing
            lda #MIRROR_SET ;a 1 in D3 of REFP0 says make it mirror
            sta REFP0
            ldx #$10	      ;a 1 in the left nibble means go left
            dec PlayerX

SkipMoveLeft
            lda #JOY_RIGHT  ;Right?
            bit SWCHA 
            bne SkipMoveRight
            sta PlayerIsMoving
            sta PlayerFacing
            ldx #$F0	    ;a -1 in the left nibble means go right...
            lda #MIRROR_CLEAR ; remove mirror image
            sta REFP0
            inc PlayerX

SkipMoveRight
            stx HMP0	    ;set the move for player 0


            lda #%10000000  ;Button press?
            bit INPT4
            beq HandleButtonPress
            jmp SkipButtonPress

HandleButtonPress

            lda MiscFlags               ;if the button hasn't been released, don't allow a 2nd attack
            and #MISC_FLAGS_BUTTON_RELEASE_CHECK
            beq DontHideMissile 
            jmp SwordFinished

DontHideMissile

            lda MiscFlags               ;save the button as pressed
            ora #MISC_FLAGS_BUTTON_RELEASE_CHECK
            sta MiscFlags

            PlaySound #SOUND_SWORD

            lda SWCHB
            and #%01000000
            clc
            rol
            rol
            rol
            ;beq .centeredOn
            ;lda #0
            ;Skip2Bytes
;.centeredOn
            ;lda #1
            sta Temp

            ;lda #1
            ;sta PlayerMissileEnabled
            lda #0
            sta RESMP0                  ; un-hide and unlock missile
            lda PlayerY                 ; set the missile y to player y(ish)
            sbc #3
            sta PlayerMissileY
            sec
            sbc #1
            sta PlayerMissileEndY

            lda #PLAYER_ATTACK_TIMER_AMT    ;start the attack timer for
            sta PlayerAttackTimer           ;a nice little delay between attacks

            PrepareSpriteLookupTable PlayerFrame2,PlayerAnimTable ;attack animation frame


            

            ;;;;;;;;;;;;;;;
            ;Attacking 
            ;;;;;;;;;;;;;;;;
            lda #MISC_FLAGS2_ARROW_ACTIVE
            bit MiscFlags2
            beq .goOnWithAttack
            jmp ButtonDone

.goOnWithAttack

            lda PlayerFacing            ;if we're facing left,

            cmp #JOY_LEFT               ;we need to move the missile to the left a bit
            bne SkipFacingLeftAttack
            lda #HMOVE_LEFT_4
            sta ExtraMissileMovement     ;+7 on the next frame

            lda #HMOVE_LEFT_7            ;nudge 7 pixels left (is that enough?)
            ;sta HMM0 ;do this in the sub

            ldx #DIR_X_LEFT ;save to PlayerMissileDir in sub

            jsr AdjustMissileVertOffset


            ;track the direction in case it becomes an arrow
            ;(move this to sub)
            ;lda DIR_X_LEFT
            ;sta PlayerMissileDir

            ;save the missile X so we can see if it's gone offscreen
            lda PlayerX
            sbc #8
            sta PlayerMissileX

            jmp ButtonDone
SkipFacingLeftAttack


            cmp #JOY_RIGHT               ;we need to move the missile to the left a bit
            bne SkipFacingRightAttack

            lda #HMOVE_RIGHT_4            ;nudge 7 pixels left (is that enough?)
            ;sta HMM0 ;do this in the sub

            ldx #DIR_X_RIGHT

            jsr AdjustMissileVertOffset

            ;track the direction in case it becomes an arrow
            ;(move part of this to sub)
            ;lda DIR_X_RIGHT
            ;sta PlayerMissileDir

            ;save the missile X so we can see if it's gone offscreen
            lda PlayerX
            sta PlayerMissileX

            jmp ButtonDone
SkipFacingRightAttack



            ;Up
            cmp #JOY_UP
            bne SkipFacingUpAttack
            ldx Temp
            ;lda #HMOVE_RIGHT_3
            lda MissileHorizOffset,X
            sta HMM0
            lda #NUSIZ_MISSILE_1        ; 1-width missle, leave player alone
            sta NUSIZ0
            lda PlayerMissileEndY
            adc #8
            sta PlayerMissileY
            lda PlayerX
            sta PlayerMissileX

            ;track the direction in case it becomes an arrow
            lda #DIR_Y_UP
            sta PlayerMissileDir

            bne ButtonDone
            ;jmp ButtonDone

SkipFacingUpAttack


            cmp #JOY_DOWN
            bne SkipFacingDownAttack
            ldx Temp
            lda MissileHorizOffset,X
            ;lda #HMOVE_RIGHT_3
            sta HMM0
            lda #NUSIZ_MISSILE_1        ; 1-width missle, leave player alone
            sta NUSIZ0
            lda PlayerMissileEndY
            clc
            sbc #8
            sta PlayerMissileY
            clc
            sbc #8
            sta PlayerMissileEndY
            lda PlayerX
            sta PlayerMissileX

            ;track the direction in case it becomes an arrow
            lda #DIR_Y_DOWN
            sta PlayerMissileDir

            bne ButtonDone
            ;jmp ButtonDone

SkipFacingDownAttack



SkipButtonPress
            ClearFlag #MISC_FLAGS_BUTTON_RELEASE_CHECK
            ;LDA MiscFlags               ; Clear the flag for button release
            ;AND #MISC_FLAGS_BUTTON_RELEASE
            ;STA MiscFlags
            jmp SwordFinished

CheckForShootBow
            lda #%10000000  ;Button press?
            bit INPT4
            bne .noArrows
            lda NumArrows
            beq .noArrows
            DecrementArrows
            lda MiscFlags2
            ora #MISC_FLAGS2_ARROW_ACTIVE
            sta MiscFlags2

SwordFinished
            
.noArrows
            lda #MISC_FLAGS2_ARROW_ACTIVE
            bit MiscFlags2
            beq ClearPlayerMissile
            bne ButtonDone
            ;jmp ButtonDone

ClearPlayerMissile
            ClearEnemyAlreadyHitFlags

            lda #%00000010
            sta RESMP0                  ; hide and lock missile
ButtonDone
PlayerControlsDone
        ENDM

        MAC ClearEnemyAlreadyHitFlags
        SUBROUTINE
            ldx #3
.LoopTop
            lda Enemy0Dir-1,X
            and #FLAG_ALREADY_HIT_CLR
            sta Enemy0Dir-1,X
            dex
            bne .LoopTop
        ENDM

   
        MAC UpdateArrow
            BankJump UpdateArrowImpl,BANK_A
ReturnFromArrowImpl
        ENDM


        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; set Player animation frame, based on movement and 
        ; frame counter
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MAC PlayerAnimation

            lda PlayerAttackTimer
            cmp #PLAYER_ATTACK_TIMER_AMT_OFF
            bmi RegularPlayerAnimation
            PrepareSpriteLookupTable PlayerFrame2,PlayerAnimTable
            jmp SkipChangePlayerFrame

RegularPlayerAnimation
            lda PlayerIsMoving
            beq SkipChangePlayerFrame   ;don't advance frame timing if player not moving

            dec PlayerSpriteAnimCtr     ;advance the counter
            bne SkipChangePlayerFrame   ;but don't do anything else unless timer is run out

            lda #ANIMATION_TIME          ;reset the frame counter
            sta PlayerSpriteAnimCtr

            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            PrepareSpriteLookupTable PlayerFrame0,PlayerAnimTable ; in generalMACros.h
            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

            lda #1                      ;toggle the anim frame (between 1 and 0)
            sec                         ;(we do 1 - PlayerSpriteAnimFrame. To do the subtraction
            sbc PlayerSpriteAnimFrame   ; you first have to set the Carry Flag or else it tries to carry :-/ )
            sta PlayerSpriteAnimFrame
            beq SkipChangePlayerFrame   ;If we're on frame 1, skip the next part where we set frame 2


            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            PrepareSpriteLookupTable PlayerFrame1,PlayerAnimTable ; in generalMACros.h
            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SkipChangePlayerFrame

        ENDM


        MAC ClearPlayerLastPositions
        SUBROUTINE
            lda #0
            sta PlayerLastX
            sta PlayerLastY
        ENDM

        MAC IfPlayerLastPositionUnset
        SUBROUTINE
            lda PlayerLastX
            bne .Nope
            lda PlayerLastY
            bne .Nope
            jmp {1}
.Nope
        ENDM
