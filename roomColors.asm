ColorDungeon1
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
    .byte #ROOM_COLOR_GRAY
ColorOutdoorRocks1
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN


ColorTrees1
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK

    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN

    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK

    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN
    .byte #ROOM_COLOR_DARK_GREEN

    

ColorRiver
    .byte #$80
    .byte #$82
    .byte #$84
    .byte #$86
    .byte #$88
    .byte #$84
    .byte #$82
    .byte #$80
    .byte #$82
    .byte #$84
    .byte #$86
    .byte #$88
ColorRiverStart
    .byte #$84
    .byte #$82
    .byte #$80
    .byte #$82
    .byte #$84
    .byte #$86
    .byte #$88
    .byte #$84
    .byte #$82
    .byte #$80
    .byte #$82
    .byte #$84
    .byte #$86
    .byte #$88
    .byte #$84
    .byte #$82
    .byte #$84

ColorOutdoorRocks2
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    .byte #ROOM_COLOR_TAN
    

    
ColorTrees2
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B

    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B

    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK
    .byte #ROOM_COLOR_TRUNK

    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B


ColorBigTree
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_TRUNK_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B
    .byte #ROOM_COLOR_DARK_GREEN_B

    

ColorLava
    .byte #$40
    .byte #$42
    .byte #$44
    .byte #$46
    .byte #$48
    .byte #$44
    .byte #$42
    .byte #$40
    .byte #$42
    .byte #$44
    .byte #$46
    .byte #$48
ColorLavaStart
    .byte #$44
    .byte #$42
    .byte #$40
    .byte #$42
    .byte #$44
    .byte #$46
    .byte #$48
    .byte #$44
    .byte #$42
    .byte #$40
    .byte #$42
    .byte #$44
    .byte #$46
    .byte #$48
    .byte #$44
    .byte #$42
    .byte #$44

ColorWasteland
    .byte #$F4
    .byte #$F6
    .byte #$F4
    .byte #$F2
    .byte #$F4
    .byte #$F6
    .byte #$F4
    .byte #$F2
    .byte #$F4
    .byte #$F6
    .byte #$F4
    .byte #$F2
    .byte #$F4
    .byte #$F6
    .byte #$F4
    .byte #$F2
    .byte #$F4
    .byte #$F6
    .byte #$F4
    .byte #$F2
    .byte #$F4
    .byte #$F6
    .byte #$F4
    .byte #$F2
    .byte #$F4
    .byte #$F6
    .byte #$F4
    .byte #$F2
    .byte #$F2



