MISSILE_RIGHT_THRESHOLD equ 145    
MISSILE_LEFT_THRESHOLD equ 1
MISSILE_TOP_THRESHOLD equ DOOR_TOP_Y_COLLIDE
MISSILE_BOTTOM_THRESHOLD equ 1


MissileVertOffset
        .byte -5
        .byte 0
MissileHorizOffset
        .byte #HMOVE_NONE
        .byte #HMOVE_RIGHT_3


AdjustMissileVertOffset
        sta HMM0
        stx PlayerMissileDir

        ;set width
        lda #NUSIZ_MISSILE_8        ; 8-width missle, leave player alone
        sta NUSIZ0

        ldx Temp
        lda MissileVertOffset,X
        tay
        adc PlayerMissileY
        sta PlayerMissileY
        sta PlayerMissileEndY
        dec PlayerMissileEndY
        rts



KillEnemyMissile
        lda #0
        sta EnemyMissileDir
        sta EnemyMissileX
        sta EnemyMissileY
        ;calling functions DEPEND on zero flag being set!
        rts



    MAC MoveEnemyMissileX
    SUBROUTINE
        lda EnemyMissileDir
        and #~DIR_X_MASK
        beq .done
        and #DIR_X_RIGHT
        beq .moveXLeft
        inc EnemyMissileX   ; move right
        lda EnemyMissileX
        cmp #MISSILE_RIGHT_THRESHOLD
        bne .done
        jsr KillEnemyMissile
        ;jmp .done
        beq .done ;optimized jmp
.moveXLeft
        dec EnemyMissileX
        lda EnemyMissileX
        cmp #MISSILE_LEFT_THRESHOLD
        bne .done
        jsr KillEnemyMissile
.done
    ENDM

    MAC MoveEnemyMissileY
    SUBROUTINE
        lda EnemyMissileDir
        and #~DIR_Y_MASK
        beq .done
        and #DIR_Y_UP
        beq .moveYDown

        inc EnemyMissileY  ; move up
        lda EnemyMissileY
        cmp #MISSILE_TOP_THRESHOLD
        bne .done
        jsr KillEnemyMissile
        ;jmp .done
        beq .done ;optimized jmp
.moveYDown
        dec EnemyMissileY
        lda EnemyMissileY
        cmp #MISSILE_BOTTOM_THRESHOLD
        bne .done
        jsr KillEnemyMissile
.done
    ENDM

    
    MAC UpdateMissiles
    SUBROUTINE
        ; check if the enemy missile is enabled
        lda EnemyMissileDir
        and #%10000000
        beq .done

        MoveEnemyMissileX
        MoveEnemyMissileY

.done
    ENDM
    
    
    
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Before the kernel, prepare the missiles to be 
    ; displayed. This mucks with the stack, so the stack can't be used
    ; without undo-ing this
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC PrepareMissiles
    SUBROUTINE

        ; check if the enemy missile is enabled
        lda EnemyMissileDir
        and #%10000000
        beq .skipPosition


        ;if so, position the missile horizontally
        ldx #OBJ_M2
        lda EnemyMissileX
        ;might need to play with the timing here?
        PosObject2

.skipPosition
    ENDM

    MAC KernelPrepareMissiles
        ; this method for efficient show/hiding of missiles and the ball taken from 
        ; http://www.qotile.net/minidig/docs/2600_advanced_prog_guide.txt
        ;TSX             ; Transfer stack pointer to X
        ;STX Temp2       ; Store it in ram
        ; -- actually I don't need to store stack pointer at this
        ; point becuase it will always be #FF
        ldx #ENAM1
        txs             ; Set the top of the stack to ENAM1+1
    ENDM

    MAC ClearMissiles
        ;turn off missiles at top of screen
        lda #0               
        sta ENAM0             
        sta ENAM1             
    ENDM


    MAC KernelDrawMissiles
        KernelDrawMissilesBranchVersion
    ENDM

    ; my sad attempt at a faster version of the missile check.
    MAC KernelDrawMissilesDecVersion
    SUBROUTINE
        cpx EnemyMissileY       ;3
        php                     ;3
        cpx PlayerMissileY      ;3
        php                     ;3
        pla                     ;4
        pla                     ;4
        beq .done               ;2 (3)
        cpx PlayerMissileEndY   ;3
        beq .done               ;2 (3)
        dec PlayerMissileY      ;5  - 32 worst case
.done

    ENDM

    MAC KernelDrawMissilesBranchVersion
    SUBROUTINE
        ; this is voodoo. see that link above.
        ; Basically, php pushes flags register to stack. We've moved the stack
        ; to the ENAM1 register, so it pushes the zero flag to the appropriate bit of
        ; the ENAM1 register (it was how they did it in combat!)

        cpx PlayerMissileY      ;3
        bne .NextMissilePart    ;2/3
        lda #%11111111          ;2
        sta ENAM0               ;3
        ;jmp .MissileDone        
        bne .afterEnemyMissile  ;3 (13 this branch)
.NextMissilePart
        cpx PlayerMissileEndY   ;3
        bne .MissileDone        ;2/3
        lda #0                  ;2
        sta ENAM0               ;3 
        beq .afterEnemyMissile  ;3 (19 this branch)
.MissileDone                

        cpx EnemyMissileY       ;3
        php                     ;3
        pla                     ;4 (21 this branch)
.afterEnemyMissile
    ENDM

    MAC KernelDrawOnlyPlayerMissile
    SUBROUTINE
        cpx PlayerMissileY      ;3
        bne .NextMissilePart    ;2/3
        lda #%11111111          ;2
        sta ENAM0               ;3
        jmp .MissileDone        ;3  -- 23
.NextMissilePart
        cpx PlayerMissileEndY   ;3
        bne .MissileDone        ;2/3
        lda #0                  ;2
        sta ENAM0               ;3 -- this branch 26, worst case
.MissileDone                ;(all branches -- 18)
    ENDM


    MAC FixMissileKernelStackWeirdness
        ldx #$FF
        txs
    ENDM



