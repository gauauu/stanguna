    ORG $D000
    RORG $F000

CURRENT_BANK eqm BANK_1


    include allBanks.asm


    ;stuff in bank 1
    ;include roomDefs.asm
    ;include sound.asm
    ;include player.asm
    include enemies.asm
    include collisions.asm
    include background.asm
    include items.asm
    include missiles.asm
    include sound.asm
    include experience.asm

    ;nop
    ;nop

BeginGame
PlayerDied

    ;BankJsr DoNothingAndReturn,BANK_A

    ;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Do basic one-time initialization:
    ;;;;;;;;;;;;;;;;;;;;;;;;;

    BankJump PrepLoad,BANK_A
AfterLoad
    ;moved these to bank_A
    ;lda #0
    ;sta RoomDef
GotoTitle
    BankJump Title,BANK_B

StartNewGame
    StartupZeroMem
    jsr BurnFrame
    NewGame
ContinueGame
    InitPlayer
    ;jsr PrepareBackgroundFlags
    ;InitEnemies 
    InitBackgroundIndexed
    jsr InitRoom
    jmp MainLoopEntry


MainLoop

    ;PrepareBackgroundFlags
    
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; set up the frame. I need to see if we can actually do work here
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




	lda #2 
    sta WSYNC
	sta VSYNC	
	sta WSYNC	
	sta WSYNC 	
	lda #43	
	sta TIM64T	
	lda #0
	sta WSYNC	
	sta VSYNC 	

MainLoopEntry
    
    inc FrameCtr

    lda RoomTransitionTimer
    cmp #30
    beq MainSkipSubscreen


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Update the game state:
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;check if subscreen switch
    lda #SWCHB_COLOR
    and SWCHB
    bne MainSkipSubscreen
    BankJump ShowSubscreen,BANK_A


MainSkipSubscreen
    ;check for reset button
    lda #SWCHB_RESET
    and SWCHB
    bne MainSkipReset
    BankJump ResetAfterChecksum,BANK_B


MainSkipReset
    CheckForNewRoom
    ;CheckAndProcessCollisions

    lda RoomTransitionTimer
    beq NormalFrame
    dec RoomTransitionTimer
    eor #30
    bne NormalFrame
    jsr BurnFrame

NormalFrame

    lda PlayerHP
    bne PlayerStillAlive
    jmp AfterUpdates

PlayerStillAlive


SkipToUpdateEnemies
    UpdateEnemies
    UpdateMissiles

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Preparation for the kernel
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

AfterUpdates

    PreparePlayer
    PrepareEnemies
    PrepareMissiles

    lda PlayerHP
    bne .playerAlive2

    ;;handle if the player is dead
    lda #<ColorDungeon1
    sta RoomColorPtr
    lda #>ColorDungeon1
    sta RoomColorPtr+1
    lda #ROOM_COLOR_GRAY-2
    sta RoomBGColor
    ClearFlag #MISC_FLAGS_ENABLE_BALL 
    lda FrameCtr
    bne .dontChangeColor
    BankJump ResetAfterChecksum,BANK_B
    ;jmp PlayerDied

    

.playerAlive2
    lda RoomFGColor
    cmp #ROOM_COLOR_RIVER
    beq .waterChangeColor

    cmp #ROOM_COLOR_LAVA
    beq .lavaChangeColor
    bne .dontChangeColor

.waterChangeColor
    lda #<ColorRiver
    Skip2Bytes
.lavaChangeColor
    lda #<ColorLava
    sta Temp

    lda FrameCtr
    and #%00011000
    clc
    ror
    ror
    ror
    adc Temp
    sta RoomColorPtr

.dontChangeColor
        ;lda #<ColorDungeon1
        ;sta RoomColorPtr
        ;lda #>ColorDungeon1
        ;sta RoomColorPtr+1

    BankJump PrepareHeader,BANK_A
    ;nop ;something here is wonky -- without this,
        ;it doesn't do the right # of scanlines


AfterPrepareHeader



    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; and ready for kernel...
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    
    BankJump SingleKernel,BANK_2

AfterKernel
    

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Do any post-kernel processing
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


    ; Wait for Overscan...
	lda #$0F
	sta WSYNC  	
	sta VBLANK 	
	sta WSYNC  	
    MaybeFixDarkness
    ;PlaySounds 0
    sta WSYNC
    ;PlaySounds 1
    sta WSYNC

    lda #22
    sta TIM64T
    ldx #0
    jsr PlayAndUpdateSounds
    ;ldx #1
    inx
    jsr PlayAndUpdateSounds

    ;dont do more collisions if the player is dead
    lda PlayerHP
    bne .goToCollisions
    jmp .overscanTimerLoop

.goToCollisions
    CheckAndProcessCollisions

    lda #0
    sta HMCLR

    lda PlayerHP
    bne PlayerStillAliveB
    jmp OverscanWait

PlayerStillAliveB
    
    PlayerControls 
    UpdateArrow
    ClearPlayerCollisionFlags
    PlayerAnimation 




OverscanWait

.overscanTimerLoop

    lda INTIM
    bne .overscanTimerLoop
    sta WSYNC


	jmp  MainLoop      

MainKernelPrep
    jsr BurnFrame
    jmp MainLoop


BurnFrame SUBROUTINE

.WaitVblankEnd
        lda INTIM	
        bne .WaitVblankEnd
        ldx #HALF_SCANLINES*2+33
        sta WSYNC
        lda #$0F
        sta VBLANK
.Repeat
        sta WSYNC
        nop
        dex
        bne .Repeat


        lda #2 
        sta WSYNC	
        sta VSYNC	
        sta WSYNC 	
        sta WSYNC	
        lda #43	
        sta TIM64T	
        lda #0
        sta WSYNC
        sta VSYNC 	
        ;sta VBLANK
        rts

    echo "------", [$FFF6 - *]d, " bytes free in main bank"
	ORG $DFFC
	RORG $FFFC
	.word Start
	.word Start

