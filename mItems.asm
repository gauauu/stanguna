ITEM_NONE           equ 0

ITEM_KEY            equ 1

;tracked via flags in the inventory variable
ITEM_BOW_ARROWS     equ 2
ITEM_DYNAMITE       equ 3
ITEM_LANTERN        equ 4
ITEM_RING           equ 5
ITEM_BOOTS          equ 6

;these are tracked via the progress flags
ITEM_HP_UP          equ 7
ITEM_ARMOR_UP       equ 8
ITEM_POWER_UP       equ 9

;things you grab over and over again
ITEM_MEAT           equ 10
ITEM_MONEY          equ 11
ITEM_ARROWS         equ 12

ITEM_VERY_NONE      equ 13

;ROOM_FLAG_RESPAWN   equ 14
ROOM_FLAG_DARK      equ 15

ITEM_UNIQUE_CUTOFF  equ 6 ;;??
ITEM_MULTIPLE_CUTOFF  equ 10 ;;??
ITEM_REG_INV_START  equ 2

ITEM_SPAWN_DEAD_TIME equ 40

;ITEM_CURRENT_ITEM   equ Enemy0State0
ITEM_PICKUP_TIMER   equ Enemy1State0
;ITEM_KEY_COLOR      equ Enemy2State0


    MAC IfHasItem
    SUBROUTINE
        ; if it's a key, we have to do magic
        cmp #ITEM_KEY
        bne .NotKey
        IfPlayerMissingKey
        bne {1}
        jmp .Continue
.NotKey

        ; if it's a regular inventory item,
        ; see if we have more than 1
        cmp #ITEM_UNIQUE_CUTOFF+1
        bmi .RegularInventoryItem
        jmp .NonUniquePowerup
.RegularInventoryItem
        ;sec
        ;sbc #ITEM_REG_INV_START
        tax
        lda #1
        BitShiftAByX
        and Inventory
        beq .Continue
        jmp {1}
.NonUniquePowerup
        ; if it's a non-unique item,
        ; check the flags in the lower 4
        ; bits against the ProgressFlags
        ldy #ROOMDEF_ITEM_AND_LOCK
        LdaRoomDefY
        and #LOCK_COLOR_MASK
        tax
        lda #1
        BitShiftAByX
        and ProgressFlags
        bne {1}
.Continue
    ENDM

    
    MAC CheckIfSpawnItem
    SUBROUTINE
TopOfCheckIfSpawnItem
        IfFlagSet #MISC_FLAGS_ENEMIES_ALIVE,JmpDoneCheckIfSpawnItem
        IfFlagSet #MISC_FLAGS_ITEM_GOTTEN,JmpDoneCheckIfSpawnItem
        ;see if there's no item
        ldy #ROOMDEF_ITEM_AND_LOCK
        LdaRoomDefY
        tax
        cmp #(ITEM_VERY_NONE<<4)
        beq JmpDoneCheckIfSpawnItem
        ;cmp #ROOM_FLAG_RESPAWN
        ;beq JmpDoneCheckIfSpawnItem
        ;cmp #ROOM_FLAG_DARK
        ;beq JmpDoneCheckIfSpawnItem
        jmp .keepGoing

JmpDoneCheckIfSpawnItem
        jmp DoneCheckIfSpawnItem

.keepGoing
        txa
        ;if it's a key or progress item, save the
        ;key color (or progress flag)
        and #LOCK_COLOR_MASK
        sta Enemy2State0

        txa

        ;shift it right to get the 
        ;actual item id
        ;(dont need to keep CLC'ing
        ; because we know the lower bits
        ; are 0 based on the AND
        ; with the item mask above)
        and #ITEM_MASK
        clc
        ror
        ror
        ror
        ror

        ;store a copy in Enemy0State0
        ;so we can futz with it
        ;in A without losing it
        sta Enemy0State0

        bne .notRandom
        ldy #ROOMDEF_NUM_ENEMIES
        LdaRoomDefY
        beq .SpawnNothing
        lda FrameCtr

        ;TODO
        ;TODO: change this ;;;;
        ;TODO
        ;TODO
        ;TODO
        ;TODO
        and #$01
        ;and #$00
        beq .spawnRandomItem
        ; no random
.SpawnNothing
        SetFlag #MISC_FLAGS_ITEM_GOTTEN
        jmp DoneCheckIfSpawnItem

.spawnRandomItem
        ldx #ITEM_MEAT
        lda FrameCtr
        and #%00000100
        bne .jumpOverArrows
        lda Inventory
        and #(1 << ITEM_BOW_ARROWS)
        beq .SpawnNothing

        ldx #ITEM_ARROWS
.jumpOverArrows
        stx Enemy0State0
        jmp AfterAlreadyOwnedCheck

.notRandom
        cmp #ITEM_KEY
        beq .doItemOwnedCheck

        ;if not unique, we don't care
        ;if it's already owned...
        sec
        sbc #ITEM_MULTIPLE_CUTOFF
        bpl AfterAlreadyOwnedCheck

.doItemOwnedCheck
        lda Enemy0State0

        IfHasItem DoneCheckIfSpawnItem

AfterAlreadyOwnedCheck
        jsr SpawnItem
DoneCheckIfSpawnItem
    ENDM


        MAC SetColorForItemDisplay
        SUBROUTINE
            lda Enemy0State0
            cmp #ITEM_KEY
            bne .NotKey
            lda Enemy2State0
            AccDoorToColor
            sta EnemyColor
            jmp .Done
.NotKey
            ldx Enemy0State0
            lda ItemColors,X
            sta EnemyColor
.Done
        ENDM

        MAC AddItemToInventory
        SUBROUTINE
            lda Enemy0State0
            cmp #ITEM_KEY
            bne .NotKey
            AddKeyToInventory Enemy2State0
            jmp EndOfPickUpItem
.NotKey
            cmp #ITEM_MEAT
            bne .notMeat
            lda PlayerHP
            clc
            adc #4
            sta PlayerHP
            sec
            sbc PlayerMaxHP
            bpl .capHp
            jmp EndOfPickUpItem
.capHp
            lda PlayerMaxHP
            sta PlayerHP
            jmp EndOfPickUpItem

.notMeat
            cmp #ITEM_ARROWS
            bne .notArrows
            ldx #5
.arrowLoop
            AddArrows
            dex
            bne .arrowLoop
            jmp EndOfPickUpItem

.notArrows
            cmp #ITEM_RING
            bne .notRing
            lda PlayerPower
            clc
            adc #4
            sta PlayerPower
            lda #ITEM_RING
            bne .RegularInventoryItem ;optimized jmp
.notRing
            cmp #ITEM_UNIQUE_CUTOFF+1
            bmi .RegularInventoryItem
            jmp .NonUniquePowerup
.RegularInventoryItem
            tax
            lda #1
            BitShiftAByX
            ora Inventory
            sta Inventory

            ;if we found the bow and arrow,
            ;add 10 arrows
            lda Enemy0State0
            cmp #ITEM_BOW_ARROWS
            bne .done
            lda NumArrows
            adc #8
            sta NumArrows

            jmp EndOfPickUpItem

.NonUniquePowerup
            AddProgressItemToInventory Enemy2State0

            cmp #ITEM_HP_UP
            bne .SkipHpUp
            lda PlayerMaxHP
            clc
            adc #5
            sta PlayerMaxHP
            lda PlayerHP
            clc
            adc #5
            sta PlayerHP
            jmp EndOfPickUpItem

.SkipHpUp
            cmp #ITEM_POWER_UP
            bne .SkipPowerUp
            inc PlayerPower
.readyForEnd
            jmp EndOfPickUpItem
.SkipPowerUp
            cmp #ITEM_ARMOR_UP
            bne .readyForEnd
            inc PlayerArmor
            bne .readyForEnd
            ;TODO: add other items to inventory (inventory or powerups both)
.done
        ENDM

