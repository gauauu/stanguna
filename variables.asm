
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Misc Constants
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ANIMATION_TIME equ 10
TOTAL_SCANLINES equ 191
HALF_SCANLINES equ 95
MAIN_SCREEN_START equ 90
MIDLINE equ 47

MAX_X equ 159
MAX_PLAYER_X equ 151
CENTER_X equ 80

CENTER_Y equ MIDLINE

MAX_Y equ MAIN_SCREEN_START

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Variables
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    SEG.U variables
    ORG $80

PlayerY ds 1
PlayerX ds 1
PlayerLastX ds 1
PlayerLastY ds 1

PlayerSpriteAnimFrame ds 1
PlayerSpriteAnimCtr ds 1
PlayerIsMoving ds 1

PlayerAnimTable ds 2


PlayerColorDef ds 2

EnemyHeight ds 1

Enemy0X ds 1
Enemy1X ds 1
Enemy2X ds 1

Enemy0Y ds 1
Enemy1Y ds 1
Enemy2Y ds 1

Enemy0LastShownX ds 1
Enemy1LastShownX ds 1
Enemy2LastShownX ds 1

Enemy0LastShownY ds 1
Enemy1LastShownY ds 1
Enemy2LastShownY ds 1

Enemy0LastX ds 1
Enemy1LastX ds 1
Enemy2LastX ds 1

Enemy0LastY ds 1
Enemy1LastY ds 1
Enemy2LastY ds 1


Enemy0State0 ds 1
Enemy1State0 ds 1
Enemy2State0 ds 1

Enemy0Dir ds 1
Enemy1Dir ds 1
Enemy2Dir ds 1

Enemy0HP ds 1
Enemy1HP ds 1
Enemy2HP ds 1

;Enemy0SpriteBuffer ds 1
;Enemy1SpriteBuffer ds 1
;Enemy2SpriteBuffer ds 1

EnemySpriteTable ds 2

;EnemyUpdateFunc ds 2
EnemyDef ds 2

EnemyColor       ds 1

EnemyAnimCounter ds 1
EnemyAnimFrame ds 1
EnemyStateCounter ds 1

EnemyMissileY ds 1
EnemyMissileX ds 1
;highest bit is an "active" bit?
EnemyMissileDir ds 1

EnemyCollisionFlags0 ds 1
EnemyCollisionFlags1 ds 1
EnemyCollisionFlags2 ds 1

;PlayerMissileEnabled ds 1
PlayerMissileY ds 1
PlayerMissileEndY ds 1
PlayerMissileX ds 1
PlayerMissileDir ds 1


PlayerAttackTimer ds 1

PlayerFaded ds 1

PlayerFacing ds 1

PlayerHP ds 1

PlayerCollisionFlags ds 1

COL_FLAG_HURT    = %10000000
COL_FLAG_WALL    = %01100000
COL_FLAG_BALL    = %00100000


RoomDef ds 2
LineDefA ds 2
LineDefB ds 2
LineDefC ds 2

RoomBGColor ds 1
RoomFGColor ds 1
RoomColorPtr ds 2

;CurrentItem ds 1
;TODO: rework inventory to only be a byte

EnemyScaleNuSiz ds 1

FrameCtr ds 1

TopDoorColor ds 1          
BottomDoorColor ds 1          

SideDoorOrVal ds 1

MiscFlags ds 1      
MiscFlags2 ds 1

MapPosition ds 1
Experience ds 1

;Progress:
PlayerMaxHP ds 1
Inventory ds 1
NumArrows ds 1
PlayerPower ds 1
PlayerArmor ds 1
KeysOwned       ds 1
Respawn         ds 1
ProgressFlags   ds 1
LockStatus      ds 1

Sound0 ds 1
Sound1 ds 1

Rand   ds 1

RoomTransitionTimer ds 1

;;;; everything below here is temporary, and can be reused in other
; custom kernel routines (subscreen/title/etc?)

DisplayDigit0 ds 2
DisplayDigit1 ds 2
DisplayDigit2 ds 2

KernelSwitch1 ds 1
KernelSwitch2 ds 1
KernelSwitch3 ds 1

KernelMode ds 1

ExtraMissileMovement ds 1 

PlayerColorBuffer ds 1


Temp ds 1

;CAUTION -- also using
;this for EnemyUpdateFunc jump
;address
TempPF0 ds 1
TempPF1 ds 1
TempPF2 ds 1

;Flags:
MISC_FLAGS_BUTTON_RELEASE       = %11111110
MISC_FLAGS_BUTTON_RELEASE_CHECK = %00000001
MISC_FLAGS_ENEMIES_ALIVE        = %00000010
MISC_FLAGS_ENABLE_BALL          = %00000100
MISC_FLAGS_BALL_WALL_FAKE       = %00001000
MISC_FLAGS_TOP_CLOSED           = %00010000
MISC_FLAGS_BOTTOM_CLOSED        = %00100000
MISC_FLAGS_ITEM_SHOWING         = %01000000
MISC_FLAGS_ITEM_GOTTEN          = %10000000
;;;;

MISC_FLAGS2_ARROW_ACTIVE        = %00000001
MISC_FLAGS2_WARP_UP             = %00000010
MISC_FLAGS2_JUST_SWITCHED_ROOM  = %00000100
MISC_FLAGS2_PARTIAL_WATER       = %00001000
MISC_FLAGS2_DARK                = %00010000
MISC_FLAGS2_HANDLED_ENEMIES_DEAD = %00100000



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; synonyms for title screens:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

tmpVar          equ DisplayDigit0
tmpVar2         equ DisplayDigit0+1

IsStart         equ Sound0
CtrlReleased    equ Sound1
ptr             equ DisplayDigit1
ptr1            equ DisplayDigit1
ptr2            equ (ptr+2)
ptr3            equ (ptr+4)
ptr4            equ (ptr+6)
ptr5            equ (ptr+8)
ptr6            equ (ptr+10)

PasswordChar    equ    TopDoorColor
PasswordX       equ    RoomFGColor
PasswordY       equ    RoomBGColor
PasswordScrollTimer equ EnemyScaleNuSiz



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


FLAG_KERNEL_0       = %00000000
FLAG_KERNEL_1       = %10000000
FLAG_KERNEL_2       = %01000000
FLAG_KERNEL_3       = %00100000

KERNEL_MODE_0       = %00000000

KERNEL_MODE_1       = %10000000 ;works
KERNEL_MODE_12_A    = %10000001 ;works
KERNEL_MODE_12_B    = %10000010 ;works

KERNEL_MODE_1_2     = %01000000 ;works
KERNEL_MODE_12_3_A  = %01000001 ;tries to do 1_23 instead
KERNEL_MODE_12_3_B  = %01000010 ;tries to do 1_23 instead
KERNEL_MODE_1_23_A  = %01000011 ;works 
KERNEL_MODE_1_23_B  = %01000100 ;works 

KERNEL_MODE_123_A   = %10000011 ;no
KERNEL_MODE_123_B   = %10000100 ;no
KERNEL_MODE_123_C   = %10000101 ;no

KERNEL_MODE_1_2_3   = %00100001 ;?

KERNEL_BOTTOM_DOOR_CUTOFF = 3

;DIR_Y_MASK          = %11110011
;DIR_Y_UP            = %00000100
;DIR_Y_DOWN          = %00001000
;
;DIR_X_MASK          = %11111100
;DIR_X_RIGHT         = %00000001
;DIR_X_LEFT          = %00000010

DIR_X_MASK           = %11110011
DIR_X_LEFT           = %00000100
DIR_X_RIGHT          = %00001000

DIR_Y_MASK          = %11111100
DIR_Y_UP            = %00000001
DIR_Y_DOWN          = %00000010

DIR_MASK            = %11110000

FLAG_ALREADY_HIT    = %00010000
FLAG_ALREADY_HIT_CLR = ~FLAG_ALREADY_HIT

FLAG_ACTIVE         = %10000000

    echo "------", [$F8 - *]d, " bytes RAM remaining"

