DrawHeader SUBROUTINE

        sta WSYNC
        ;Line 1 - preparation
        lda #$07
        sta COLUPF
        ;STA RESP0       ; position the first sprite

        ;nop     ;2
        lda #$0e ;2
        sta COLUP0 ;3
        sta COLUP1 ;3
        ;nop     ;2
        ;nop     ;2
        nop     ;2
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop

        nop
        nop
        nop
        nop

        lda #0
        sta PF0
        sta PF1
        sta PF2
        sta RESP1

        ldx #7      ;Really we want it to be 6, but we're decrementing
                    ;once immediately to make timing work


        lda #$4F ; color for the heart
        sta COLUP0
        sta COLUPF

        lda TempPF2
        sta PF2

        sta WSYNC
        dex                 ;this is to mimic the exact timing
        bne .HeaderMainLoop ;from below
        ldy HpMeterIcon,X

.HeaderMainLoop

        ;Body Left
        ;LDA HpMeterIcon,X
        ;STA GRP0
        ;STA RESP0
        lda TempPF0
        sta PF0
        lda TempPF1
        sta PF1

        stx Temp
        ldy Temp
        lda (DisplayDigit0),y
        sta GRP1
        lda (DisplayDigit1),y
        tax
        lda (DisplayDigit2),y

        ;STY GRP1

        ldy #0
        sty PF2
        sty PF1
        sty PF0
        sty PF0 ;purposeful repeat to burn 3 cycles

        ;ldy (DisplayDigit0),X
        ;sta RESP1

        stx GRP1
        nop
        ;sta RESP1
        sta GRP1

        ;Body Right
        ;sta GRP1

        ;ldy HpMeterIcon,X
        ldx Temp

        nop

        lda TempPF2
        sta PF2
        ;sta WSYNC
        dex
        bne .HeaderMainLoop
        lda #0
        sta PF2
        sta GRP0
        sta GRP1
        sta WSYNC
        sta PF1
        sta PF0


        sta WSYNC
        lda #0
        sta GRP0
        sta GRP1
        sta PF0
        sta PF1
        sta PF2
        lda EnemyScaleNuSiz
        sta NUSIZ1

        sta WSYNC
        lda PlayerX
        ldx #0
        PosObject2
        ;PositionObjectWithoutJSR 0 ; macro in PosObject.asm. 0 refers to Player 0
        sta WSYNC
        lda Enemy0X
        ldx #1
        PosObject2
        sta WSYNC
        ;PositionObjectWithoutJSR 1 ; macro in PosObject.asm. 0 refers to Player 0
        sta HMOVE
        ;reset the REFP1 based on enemy direction
        lda Enemy0Dir
        sta REFP1
        lda EnemyColor
        sta COLUP1
        sta WSYNC
        ;PrepareBackgroundFlags 
        SetBackgroundFlags
        lda #0      ;accumulator needs to be 0 going into the main kernel
        
        rts
        

