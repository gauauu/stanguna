PlayAndUpdateSounds SUBROUTINE
        LDA Sound0,X
        AND #SOUND_TIMER_MASK  
        BNE .YesPlaySound       
        beq .WaitThenTurnOffSound 
.YesPlaySound                   
        TAY                     
        DEY                     
        STY Temp                
        LDA Sound0,X

                                
        AND #SOUND_EFFECT_MASK  
        TAY       
        ORA Temp  
        STA Sound0,X 

        LDA #$FF  
        STA AUDV0,X


        TYA     
        CMP #SOUND_PLAYER_HURT 
        BNE .TrySound2        

        ; player hurt sound:
        ; channel 6, module from around f30 to f5
                             
        LDY #6              
        STY AUDC0,X
        LDA #10    
        SBC Temp   
        STA AUDF0,X
        JMP .DonePlayingSound 

.TrySound2                   

        CMP #SOUND_SWORD    
        BNE .TrySound3     

        ;sword sound:
        ;c8/f4, but cut off after only 4 or so frames
        LDA Temp          
        CMP #%00001100   
        BNE .PlaySound2 

        LDA #0         
        STA Sound0,X
        ;JMP .TurnOffSound 
        beq .TurnOffSound ;optimized jmp

.PlaySound2              
        LDY #8          
        STY AUDC0,X
        LDY #%00000100   
        STY AUDF0,X
        ;JMP .DonePlayingSound  
        beq .DonePlayingSound ;optimized jmp


.TrySound3                    

        CMP #SOUND_ENEMY_HURT
        BNE .TrySound4      

        ;enemy hurt sound:
        ;c14, f30 to f5
        LDY #14   
        STY AUDC0,X
        LDA #10  
        SBC Temp 
        STA AUDF0,X
        JMP .DonePlayingSound 


.TrySound4                  


        LDY #6    
        STY AUDC0,X
        LDA Temp 
        AND #8  
        ADC #1 
        STA AUDF0,X
        ;JMP .DonePlayingSound 
        bne .DonePlayingSound ;optimized jmp

.TrySound5
        nop
        
.WaitThenTurnOffSound 
        ldy #5
.waitLoop
        dey
        bne .waitLoop

.TurnOffSound   
        LDA #0
        STA AUDV0,X
        STA AUDC0,X
        STA AUDF0,X

.DonePlayingSound
        rts
