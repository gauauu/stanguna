    ;Whether a door is locked or unlocked 
    ;Params:
    ;Door #
    ;Place to jump to if locked
    MAC IfIsLocked
        ;first transform a number into a bit flag shifted by 
        ;that amount (ie 3 = 00000100)
        lda #1
        BitShiftBy {1}

        ;Then check agains the first progress byte
        and LockStatus
        beq {2}

    ENDM


    MAC UnlockDoor
    SUBROUTINE
.UnlockDoor
        ;lda #1
        ;BitShiftBy {1}
        ;ora LockStatus
        ;sta LockStatus
    ENDM

    MAC AccDoorToColor
.accdd
        clc
        rol
        clc
        rol
        rol
        rol
        rol
        and #$F0
        ora #8
    ENDM

    MAC AddArrows
    SUBROUTINE
        inc NumArrows
        lda #$0f
        and NumArrows
        sec
        cmp #10
        bne .done
        lda NumArrows
        and #%11110000 ;0 out the last digit
        clc
        adc #%00010000
        sta NumArrows
        cmp #$A0
        bne .done
        lda #$99
        sta NumArrows
.done
    ENDM

    MAC Add10Arrows
    SUBROUTINE
        lda NumArrows
        clc
        adc #$10
        sta NumArrows
        sec
        sbc #$99
        bpl .skipResetTo99
        lda #$99
        sta NumArrows
.skipResetTo99
    ENDM

    MAC DecrementArrows
    SUBROUTINE
        lda #$0f
        and NumArrows
        beq .decNextDigit
        dec NumArrows
        jmp .done
.decNextDigit
        lda NumArrows
        sec
        sbc #%00010000
        and #%11110000
        ora #$09
        sta NumArrows
.done
    ENDM




    MAC IfPlayerMissingKey

        ldy #ROOMDEF_ITEM_AND_LOCK
        LdaRoomDefY
        and #LOCK_COLOR_MASK

        tax ;now A has lock color
            ;transfer that to X
        lda #1
        BitShiftAByX 
        and KeysOwned

    ENDM

    ;params: 
    ; 1 - location where key
    ;     color/id is held
    MAC AddKeyToInventory
    SUBROUTINE
        lda {1}
        tax
        lda #1
        BitShiftAByX
        ora KeysOwned
        sta KeysOwned
    ENDM

    MAC AddProgressItemToInventory
    SUBROUTINE
        pha
        lda {1}
        tax
        lda #1
        BitShiftAByX
        ora ProgressFlags
        sta ProgressFlags
        pla
    ENDM

