
	processor 6502

    ;;variables or macros, so they don't need to be in the main segment
	include vcs.h
    include variables.asm
    include mProgress.asm
    include mPosObject.asm
    include mBackground.asm
    include mPlayer.asm
    include mEnemies.asm
    include mSound.asm
    include mCollisions.asm
    include mItems.asm
    include generalMacros.asm

    SEG

    ;;;;;;;;;;;;;;;;;;
    ; Banks
    ;;;;;;;;;;;;;;;;;;;

    include bank_b.asm
    include bank_a.asm
    include bank_1.asm
    include bank_2.asm
  



